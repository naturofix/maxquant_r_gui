setwd("/home/sgarnett/Documents/RData/Neural_Stem_Cells/Differentiation_Experiment/140416_NSC_Differentiation_S1_S2_H9_H10")
print(getwd())
print("Loading : /home/sgarnett/Documents/RData/Neural_Stem_Cells/Differentiation_Experiment/140416_NSC_Differentiation_S1_S2_H9_H10/workspace/Test.RData")
load("/home/sgarnett/Documents/RData/Neural_Stem_Cells/Differentiation_Experiment/140416_NSC_Differentiation_S1_S2_H9_H10/workspace/Test.RData")
setwd(working_directory)
table_name = "df.proteinGroups.txt.edited.iBAQ.S2."



































##TOP42###







####50######



sample_number = dim(data)[2]

diff_data = apply(data,1,diff_function)
print(dim(t(diff_data)))
df.diff_data = data.frame(t(diff_data))
colnames(df.diff_data) = colnames(data)[2:dim(data)[2]]
rownames(df.diff_data) = rownames(data)
cmd = paste(table_name,'.diff = df.diff_data',sep='')
print(cmd)
eval(parse(text=cmd))

print(workspace_filename)
save.image(workspace_filename)