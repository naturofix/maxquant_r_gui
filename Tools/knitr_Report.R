setwd("/Users/sgarnett/Documents/RData/141212_SH7/txt")
print(getwd())
print("Loading : /Users/sgarnett/Documents/RData/141212_SH7/txt/workspace/Documents_RData_141212_SH7.RData")
load("/Users/sgarnett/Documents/RData/141212_SH7/txt/workspace/Documents_RData_141212_SH7.RData")
setwd('/Users/sgarnett/Documents/RData/141212_SH7/txt')
table_name = df.proteinGroups.txt.edited.LFQ.intensity.z2na
data = ""
table_name = "df.proteinGroups.txt.edited.LFQ.intensity.z2na"
experiment_name = "141212_SH7"
par(oma=c(3,0,0,0),cex.lab=0.6,cex.axis = 0.6,cex.main=0.8) #(bottom,left,top,right )

table_title = paste(experiment_name,'\n',table_name,sep='')
file_title = paste(experiment_name,'_',table_name,sep='')
data = ''

cmd = paste('data = ',table_name,sep='')
print(cmd)
try(eval(parse(text=cmd)))
try(print(dim(data)))
try(print(colnames(data)))
wd = "/Users/sgarnett/Documents/RData/141212_SH7/txt"





































































###42###







###50###



#require(xtable)
#xtab<-xtable(df.con.rev)
#print(xtab,include.rowname=FALSE)
xtable_function(wd,'df.con.rev',',include.rowname=FALSE')
#library(knitr)
#kable(df.con.rev)


summary_data = df.summary.txt
column_name = "Peptide.Sequences.Identified"
column_data = summary_data[,column_name]
data_rownames = summary_data$Raw.file
horizontal_barplot_function(column_data,data_rownames,paste(table_title,'\n',column_name,sep=''))

# save_plot_function = function(wd,table_title,plot_name){
#   full_plot_name = paste(wd,'/images/',gsub('\n','_',table_title),'_',plot_name,'.png',sep='')
#   print(full_plot_name)
#   dev.copy(png,full_plot_name)
#   dev.off()
# }

save_plot_function(wd,table_title,paste('all',column_name,sep='_'))

experiment_data = summary_data[summary_data$Experiment != '',]
column_data = experiment_data[,column_name]
data_rownames = experiment_data$Experiment
horizontal_barplot_function(column_data,data_rownames,paste(table_title,'\n',column_name,sep=''))
save_plot_function(wd,table_title,paste('Experiment',column_name,sep='_'))

df.generated_summary = data.frame(files = rep('',length(column_data)),stringsAsFactors=FALSE)
df.generated_summary$files = data_rownames
df.generated_summary$Peptides = column_data

#kable(df.generated_summary)
#xtable(df.generated_summary)
xtable_function(wd,'df.generated_summary')


cmd = paste('data = ',table_name,sep='')
print(cmd)
eval(parse(text=cmd))

column_names = colnames(data)
data_col_max = apply(data,2,function(x) max(x,na.rm=T))
horizontal_barplot_function(data_col_max,column_names,paste(table_title,'\nMax TIC',sep=''))

column_names = colnames(data)
data_col_max = signif(apply(data,2,function(x) mean(x,na.rm=T)),digits=3)
horizontal_barplot_function(data_col_max,column_names,paste(table_title,'\nmean TIC',sep=''))

column_names = colnames(data)
data_col_max = signif(apply(data,2,function(x) median(x,na.rm=T)),digits=3)
horizontal_barplot_function(data_col_max,column_names,paste(table_title,'\nmedian TIC',sep=''))



data_col_sum = colSums (data, na.rm = TRUE, dims = 1)
#plot(data_col_sum)
horizontal_barplot_function(data_col_sum,column_names,paste(table_title,'\nTotal Protein Intensities',sep=''))

#print(n_prot)
n_prot = data > 0
number_list = c()
for(i in c(1:dim(n_prot)[2])){
  number_list = c(number_list,sum(as.numeric(table(n_prot[,i]))))
}
column_names = colnames(data)
horizontal_barplot_function(number_list,column_names,paste(table_title,"\nNumber of Quantifiable Proteins",sep=''))
save_plot_function(wd,table_title,"Number_of_Quantified_Proteins")


df.generated_protein_summary = data.frame(sample = rep('',length(column_names)),stringsAsFactors=FALSE)
df.generated_protein_summary$sample = column_names
df.generated_protein_summary$Proteins = number_list
df.generated_protein_summary$Max.TIC = signif(data_col_max,digits=3)
df.generated_protein_summary$Total.TIC = signif(data_col_sum,digits=3)
#kable(df.generated_protein_summary,digit=3)
#print(xtable(df.generated_protein_summary,digits =3,display=c('d','s','d','e','e')))

xtable_name = 'df.generated_protein_summary'
xtable_function(wd,xtable_name,",digits =3,display=c('d','s','d','e','e')")


##### INSERTED SUMMARY ####
print_table = 'not_full'
text_cex = 0.8
summary = df.summary.txt
experiment_data = summary[summary$Experiment != '',]
file_data = summary[summary$Experiment == '',]



label_names = experiment_data$Experiment
plot_label_names = label_names
print(label_names)

df.data.overview = data.frame(files = rep('',length(label_names)),stringsAsFactors=FALSE)
df.data.overview$files = label_names
ms_data = experiment_data$MS
ms_title = 'Summary\nMS'
if(print_table == 'full'){barplot_vector_function(ms_data,plot_label_names,ms_title)}
df.data.overview$MS = ms_data

msms_data = experiment_data$MS.MS
msms_title = 'Summary\nMS.MS'
if(print_table == 'full'){barplot_vector_function(msms_data,plot_label_names,msms_title)}
df.data.overview$MS.MS = msms_data

msms_id_data = experiment_data$MS.MS.Identified
df.data.overview$MS.MS.Id = msms_id_data
label_names = experiment_data$Experiment
msms_id_title = 'Summary\nMS.MS.Identified'
if(print_table == 'full'){barplot_vector_function(msms_id_data,plot_label_names,msms_id_title)}

pep_data = experiment_data$Peptide.Sequences.Identified
label_names = experiment_data$Experiment
pep_title = 'Summary-Experiment\nPeptide.Sequences.Identified'
if(print_table == 'full'){barplot_vector_function(pep_data,plot_label_names,pep_title)}
df.data.overview$Peptided.Id = pep_data
data = file_data$Peptide.Sequences.Identified
label_names = file_data[,1]
title = 'Summar-File\nPeptide.Sequences.Identified'
if(print_table == 'full'){barplot_vector_function(data,label_names,title)}
#df.data.overview$Peptide.Sequences.Identified = data




per_id_sample_name_list = c()
per_identified_list = c()
for(i in c(1:dim(experiment_data)[1])){
  if(!is.na(experiment_data[i,'MS'])){
    if(print_table == 'full'){
      data = experiment_data[i,c('MS','MS.MS','MS.MS.Submitted','MS.MS.Identified','Peptide.Sequences.Identified')]
      label_names = gsub('\\.', " ", colnames(data))
      label_names = wrap.labels(label_names,15)
      data_max = max(data)+(max(data)*30/100)
      label_line = max(data)+(max(data)*15/100)
      axis_labels = try(barplot(as.matrix(data),main=experiment_data[i,1],xaxt='n',ylim=c(0,data_max)))
      axis(1,at = axis_labels,labels = label_names,las=3,cex.axis=0.8)
      text(x=axis_labels,y=c(data[1:5]),labels=c(data[1:5]),cex=text_cex,pos=3)
      ms_msms = paste('MS\nMS.MS\n',round(experiment_data[i,'MS']/experiment_data[i,'MS.MS']*100,digits=2),'%')
      #print(ms_msms)
      text(x=axis_labels[1],y=label_line,ms_msms,cex=text_cex,pos=3,col='blue')
      ms_msms = paste('MS.MS\nMS\n',round(experiment_data[i,'MS.MS']/experiment_data[i,'MS']*100,digits=2),'%')
      #print(ms_msms)
      text(x=axis_labels[2],y=label_line,ms_msms,cex=text_cex,pos=3,col='blue')
      #print(identified)
      msms_submitted = paste('MS.MS\nMS.MS.Submitted\n',round(experiment_data[i,'MS.MS']/experiment_data[i,'MS.MS.Submitted']*100,digits=2),'%')
      optimum_msms_submitted = 'optimum\n75-80%'
      text(x=axis_labels[3],y=label_line,msms_submitted,cex=text_cex,pos=3,col='blue')
      text(x=axis_labels[3],y=label_line,optimum_msms_submitted,cex=text_cex,pos=1,col='red')
      #print(identified)
      identified = paste('MS.MS.Identified\nMS.MS\n',round(experiment_data[i,'MS.MS.Identified']/experiment_data[i,'MS.MS']*100,digits=2),'%')
      text(x=axis_labels[4],y=label_line,identified,cex=text_cex,pos=3,col='blue')
      unique = paste('Peptides.Identified\nMS.MS.Identified\n',round(experiment_data[i,'Peptide.Sequences.Identified']/experiment_data[i,'MS.MS.Identified']*100,digits=2),'%')
      #print(unique)
      text(x=axis_labels[5],y=label_line,unique,cex=text_cex,pos=3,col='blue')
    }
    per_identified_list = c(per_identified_list,round(experiment_data[i,'MS.MS.Identified']/experiment_data[i,'MS.MS']*100,digits=2))
    per_id_sample_name_list = c(per_id_sample_name_list,paste(experiment_data[i,"Experiment"]))
  }
}






rep_sample_name_list = c()
repeat_per_list = c()
for(i in c(1:dim(experiment_data)[1])){
  if(!is.na(experiment_data[i,'MS'])){
    if(print_table == 'full'){
      data = experiment_data[i,c('Isotope.Patterns','Isotope.Patterns.Sequenced','Isotope.Patterns.Repeatedly.Sequenced')]
      label_names = gsub('\\.', " ", colnames(data))
      label_names = wrap.labels(label_names,15)
      data_max = max(data)+(max(data)*30/100)
      label_line = max(data)+(max(data)*15/100)
      axis_labels = try(barplot(as.matrix(data),main=experiment_data[i,1],xaxt='n',ylim=c(0,data_max)))
      axis(1,at = axis_labels,labels = label_names,las=3,cex.axis=0.8)
      
      text(x=axis_labels,y=c(data[1:3]),labels=c(data[1:3]),cex=text_cex,pos=3)
      ms_msms = paste('Isotope Patterns\nIsotope Patterns Sequences\n',round(experiment_data[i,'Isotope.Patterns.Sequenced']/experiment_data[i,'Isotope.Patterns']*100,digits=2),'%')
      #print(ms_msms)
      text(x=axis_labels[2],y=label_line,ms_msms,cex=text_cex,pos=3,col='blue')
      text(x=axis_labels[2],y=label_line,'20-40%',cex=text_cex,pos=1,col='red')
      ms_msms = paste('Isotope.Patterns.Sequenced\nIsotope.Patterns.Repeatedly.Sequenced\n',round(experiment_data[i,'Isotope.Patterns.Repeatedly.Sequenced']/experiment_data[i,'Isotope.Patterns.Sequenced']*100,digits=2),'%')
      #print(ms_msms)
      text(x=axis_labels[3],y=label_line,ms_msms,cex=text_cex,pos=3,col='blue')
      text(x=axis_labels[3],y=label_line,'5-6%',cex=text_cex,pos=1,col='red')
    }
    repeat_per_list = c(repeat_per_list,round(experiment_data[i,'Isotope.Patterns.Sequenced']/experiment_data[i,'Isotope.Patterns']*100,digits=2))
    rep_sample_name_list = c(rep_sample_name_list,paste(experiment_data[i,"Experiment"]))
    
  }
}


per_data = as.numeric(per_identified_list)
#label_names = file_data[,1]
per_title = 'Percentage MS/MS Identified'
if(print_table == 'single'){barplot_vector_function(per_data,per_id_sample_name_list,per_title)}
df.data.overview$Per.MS.MS.Id = per_data

rep_data = as.numeric(repeat_per_list)
#label_names = file_data[,1]
rep_title = 'Percentage Repeated'
if(print_table == 'single'){barplot_vector_function(rep_data,rep_sample_name_list,rep_title)}
df.data.overview$Per.Repeated = rep_data

par(mfrow = c(2,3))
barplot_vector_function(ms_data,plot_label_names,ms_title)
barplot_vector_function(msms_data,plot_label_names,msms_title)
barplot_vector_function(msms_id_data,plot_label_names,msms_id_title)
barplot_vector_function(pep_data,plot_label_names,pep_title)
barplot_vector_function(per_data,per_id_sample_name_list,per_title)
barplot_vector_function(rep_data,rep_sample_name_list,rep_title)

save_plot_function(wd,table_title,'summary')
par(mfrow = c(1,1))


#xtable_function(getwd(),'df.data.overview')
#print(x[1], file = x[2])
xtable_name = 'df.data.overview'
x = xtable_function(wd,xtable_name)
#xtable_file_name = xtable_output_file(xtable_name)
#print(x, file = xtable_file_name)

cmd = paste('data = ',table_name,sep='')
print(cmd)
try(eval(parse(text=cmd)))






plot((data),main=paste(experiment_name,'\n',table_name,sep=''))
plot_name = ('plot')
save_plot_function(wd,table_title,plot_name)


if(grepl('log2',table_name)==FALSE){
  plot(log2(data),main=paste(experiment_name,'\nlog2(',table_name,')',sep=''))
  plot_name = ('log2_plot')
  save_plot_function(wd,table_title,plot_name)
}




boxplot_function(((data)),paste(experiment_name,'\n',table_name,sep=''),'')
plot_name = ('boxplot')
save_plot_function(wd,table_title,plot_name)


if(grepl('log2',table_name)==FALSE){
    boxplot_function(log2((data)),paste(experiment_name,'\nlog2(',table_name,')',sep=''),'')
    plot_name = ('log2_boxplot')
  save_plot_function(wd,table_title,plot_name)
}


############## CORRELATION ##############


library(Hmisc)
data.na = remove_na_function_df(data)
df.rcorr = rcorr(as.matrix(data.na), type="pearson")
boxplot_function(df.rcorr$r,paste(table_title,'\npearson',sep=''),'')
save_plot_function(wd,table_title,'pearson')

df.rcorr = rcorr(as.matrix(data.na), type="spearman")
boxplot_function(df.rcorr$r,paste(table_title,'\nspearman',sep=''),'')
save_plot_function(wd,table_title,'spearman')



heatmap(as.matrix(data.na),main=paste(experiment_name,'\n',table_name,sep=''))
plot_name = ('heatmap')
save_plot_function(wd,table_title,plot_name)


if(grepl('log2',table_name)==FALSE){
    heatmap(as.matrix(log2(data.na)),main=paste(experiment_name,'\nlog2(',table_name,')',sep=''))
    plot_name = ('log2_heatmap')
  save_plot_function(wd,table_title,plot_name)
}



##### MATPLOT ####

matplot(t(data),type='l',main=table_name,xaxt='n')
plot_name = ('matplot')
save_plot_function(wd,table_title,plot_name)


##### STAT DATA #####

cmd = paste('data.stat = ',table_name,'.stat',sep='')
print(cmd)
eval(parse(text=cmd))

plot(sort(as.numeric(data.stat$t_test)))
plot(sort(as.numeric(data.stat$mean)))



### FDR ###

simple_fdr_function(data.stat$t_test,table_title)
plot_name = 'fdr'
save_plot_function(wd,table_title,plot_name)




### VOLCANO ####

simplest_volcano_plot_function(data.stat$mean,data.stat$t_test,table_name)
plot_name = ('volcano')
save_plot_function(wd,table_title,plot_name)


par(mfrow = c(1,2))
simple_fdr_function(data.stat$t_test,table_title)
simplest_volcano_plot_function(data.stat$mean,data.stat$t_test,table_name)
save_plot_function(wd,table_title,'fdr_volcano')
par(mfrow = c(1,1))


### Regenerate the plots as simple summaries

par(mfrow=c(2,2))

summary_data = df.summary.txt
column_name = "Peptide.Sequences.Identified"
column_data = summary_data[,column_name]
data_rownames = summary_data$Raw.file
horizontal_barplot_function(column_data,data_rownames,paste(table_title,'\n',column_name,sep=''))


cmd = paste('data = ',table_name,sep='')
print(cmd)
eval(parse(text=cmd))

n_prot = data > 0
number_list = c()
for(i in c(1:dim(n_prot)[2])){
  number_list = c(number_list,sum(as.numeric(table(n_prot[,i]))))
}
column_names = colnames(data)
horizontal_barplot_function(number_list,column_names,paste(table_title,"\nNumber of Quantifiable Proteins",sep=''))

boxplot_function(((data)),paste(experiment_name,'\n',table_name,sep=''),'')
heatmap(as.matrix(data.na),main=paste(experiment_name,'\n',table_name,sep=''))
save_plot_function(wd,table_title,'summary')
par(mfrow=c(2,2))

if(grepl('log2',table_name)==FALSE){
  
  summary_data = df.summary.txt
  column_name = "Peptide.Sequences.Identified"
  column_data = summary_data[,column_name]
  data_rownames = summary_data$Raw.file
  horizontal_barplot_function(column_data,data_rownames,paste(table_title,'\n',column_name,sep=''))
  
  
  cmd = paste('data = ',table_name,sep='')
  print(cmd)
  eval(parse(text=cmd))
  
  n_prot = data > 0
  number_list = c()
  for(i in c(1:dim(n_prot)[2])){
    number_list = c(number_list,sum(as.numeric(table(n_prot[,i]))))
  }
  column_names = colnames(data)
  horizontal_barplot_function(number_list,column_names,paste(table_title,"\nNumber of Quantifiable Proteins",sep=''))
  
  boxplot_function(log2((data)),paste(experiment_name,'\nlog2(',table_name,')',sep=''),'')
  heatmap(as.matrix(log2(data.na)),main=paste(experiment_name,'\nlog2(',table_name,')',sep=''))
  save_plot_function(wd,table_title,'summary_log2')
}
























































































































































