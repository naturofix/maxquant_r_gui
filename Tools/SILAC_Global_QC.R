setwd("/home/sgarnett/Documents/RData/Neural_Stem_Cells/SILAC/Experiment_2/data/SCX_13_11_All")
print(getwd())
print("Loading : /home/sgarnett/Documents/RData/Neural_Stem_Cells/SILAC/Experiment_2/data/SCX_13_11_All/workspace/Test.RData")
load("/home/sgarnett/Documents/RData/Neural_Stem_Cells/SILAC/Experiment_2/data/SCX_13_11_All/workspace/Test.RData")
setwd(working_directory)
table_name = "df.proteinGroups.txt.edited.Ratio.H.L.rm.log2.rev"
data = ""


































##TOP42###







####50######






file_name_list = c("proteinGroups", "peptides", "evidence")
for (file_name in file_name_list) {
    cmd = paste(file_name, " = df.",file_name,'.txt',sep='')
    print(cmd)
    eval(parse(text = cmd))
}

     
        
hist(evidence$Mass.Error..ppm., breaks = 100, col = rgb(1, 0, 0, alpha = 0.5))
hist(evidence$Mass.Error..ppm., breaks = 100, xlim = c(-5, 5), col = rgb(1, 
    0, 0, alpha = 0.5))

hist(evidence$Uncalibrated.Mass.Error..ppm., breaks = 100, col = rgb(0, 
    0, 1, alpha = 0.5))
hist(evidence$Uncalibrated.Mass.Error..ppm., breaks = 100, xlim = c(-5, 
    5), col = rgb(0, 0, 1, alpha = 0.5))

hist(evidence$Uncalibrated.Mass.Error..ppm., breaks = 100)
hist(evidence$Mass.Error..ppm., breaks = 100, add = TRUE)

hist(evidence$Mass.Error..ppm., breaks = 100, xlim = c(-5, 5), col = rgb(1, 
    0, 0, alpha = 0.5))
hist(evidence$Uncalibrated.Mass.Error..ppm., breaks = 100, xlim = c(-5, 
    5), col = rgb(0, 0, 1, alpha = 0.5), add = TRUE)
plot(evidence$Mass.Error..ppm., evidence$Uncalibrated.Mass.Error..ppm.)

hist(evidence$Uncalibrated...Calibrated.m.z..ppm., breaks = 100)

plot(log2(evidence$Ratio.H.L.normalized), log10(evidence$Intensity), 
    col = "blue", pch = 19, cex = 0.5)
plot(log2(peptides$Ratio.H.L.normalized), log10(peptides$Intensity), 
    col = "red", pch = 19, cex = 0.5)
plot(log2(proteinGroups$Ratio.H.L.normalized), log10(proteinGroups$Intensity), 
    pch = 19, cex = 0.5)

plot(log2(evidence$Ratio.H.L.normalized), log10(evidence$Intensity), 
    col = "blue", pch = 19, cex = 0.5)
points(log2(peptides$Ratio.H.L.normalized), log10(peptides$Intensity), 
    col = "red", pch = 19, cex = 0.5)
points(log2(proteinGroups$Ratio.H.L.normalized), log10(proteinGroups$Intensity), 
    pch = 19, cex = 0.5)
boxplot(log2(proteinGroups$Ratio.H.L.normalized), main = "log2(proteinGroups$Ratio.H.L.normalized)")
boxplot(log10(proteinGroups$Intensity), main = "(log10(proteinGroups$Intensity)")

hist(evidence$PEP, breaks = 100)
boxplot(c(evidence$PEP, peptides$PEP), main = "c(evidence$PEP,peptides$PEP)")
hist(evidence$m.z, breaks = 100)
hist(evidence$Charge)
hist(evidence$MS.MS.Count)
# hist(evidence$Type)

hist(proteinGroups$Unique.peptides, breaks = 100)
boxplot(proteinGroups$Unique.peptides)
hist(proteinGroups$PEP, breaks = 100)
boxplot(proteinGroups$PEP)








