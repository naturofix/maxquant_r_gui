setwd("/Users/sgarnett/Documents/RData/141212_SH7/txt")
print(getwd())
print("Loading : /Users/sgarnett/Documents/RData/141212_SH7/txt/workspace/Documents_RData_141212_SH7.RData")
load("/Users/sgarnett/Documents/RData/141212_SH7/txt/workspace/Documents_RData_141212_SH7.RData")
setwd('/Users/sgarnett/Documents/RData/141212_SH7/txt')
maxquant_file_list = c('.DS_Store','.Rhistory','aifMsms.txt','allPeptides.txt','Deamidation (NQ)Sites.txt','evidence.txt','images','libraryMatch.txt','matchedFeatures.txt','modificationSpecificPeptides.txt','ms3Scans.txt','msms.txt','msmsScans.txt','msScans.txt','mzRange.txt','output','Oxidation (M)Sites.txt','parameters.txt','peptides.txt','proteinGroups.txt','Rplots.pdf','rscripts','summary.txt','tables','tables.pdf','tar','workspace')
table_name = "df.proteinGroups.txt.edited.LFQ.intensity.z2na"
experiment_name = "141212_SH7"
par(oma=c(3,0,0,0),cex.lab=0.6,cex.axis = 0.6,cex.main=0.8) #(bottom,left,top,right )

table_title = paste(experiment_name,'\n',table_name,sep='')
file_title = paste(experiment_name,'_',table_name,sep='')
data = ''

cmd = paste('data = ',table_name,sep='')
print(cmd)
try(eval(parse(text=cmd)))
try(print(dim(data)))
try(print(colnames(data)))
wd = "/Users/sgarnett/Documents/RData/141212_SH7/txt"






















##TOP42###







####50######



print(workspace_filename)
load(workspace_filename) ## this can be used to prevent deletion of dataset
#maxquant_file_list = c('evidence.txt','msms.txt','msmsScans.txt','msScans.txt','allPeptides.txt')
#### LOAD FILES FROM MAXQUANT INTO R DATA FRAMES 
print(getwd())
for(experiment_name in experiment_list){
  location = paste('data/',experiment_name,'/',sep='')
  cat(paste('\nFrom directory : ',working_directory,'\n',sep=''))
  for(file_name in maxquant_file_list){
    cat(paste('\timport file :',file_name,'\n\t\t'))
    cmd = paste("df.",file_name," = read.table(file = '",file_name,"',header=TRUE,sep='\t')",sep='')
    print(cmd)
    try(eval(parse(text = cmd)))
  }
}
working_directory = getwd()
print(getwd())
print(workspace_filename)
save.image(workspace_filename)

































list_all = ls()
df_list = grep('df.',list_all)
file_name = paste(working_directory,'/rscripts/dataframe_list.txt',sep='')
print(file_name)
write.table(list_all[df_list],file=file_name,row.names=FALSE,col.names=FALSE)
















