setwd("/home/sgarnett/Documents/RData/Neural_Stem_Cells/Gene_Experssion")
print(getwd())
print("Loading : /home/sgarnett/Documents/RData/Neural_Stem_Cells/Gene_Experssion/workspace/Test.RData")
load("/home/sgarnett/Documents/RData/Neural_Stem_Cells/Gene_Experssion/workspace/Test.RData")
setwd('/home/sgarnett/Documents/RData/Neural_Stem_Cells/Gene_Experssion')
table_name = "df.NES_vs_NS_jig_probe_expression.txt.edited"
data = ""
sd_cutoff = 0.06
t_cutoff = 0.05
selected_result_data_list_entry = "all"
uniprot_list_entry = "NES_vs_NS_jig_probe2symbol.csv"
table_name = "df.NES_vs_NS_jig_probe_expression.txt.edited"
data=''




























##TOP42###







####50######

cmd = paste('result_data = ',table_name,'.',selected_result_data_list_entry,sep='')
eval(parse(text=cmd))

cmd = paste('data.name = df.',uniprot_list_entry_str,sep='')
eval(parse(text=cmd))

dim(data.name)
dim(result_data)

name_list = c()
for(i in c(1:dim(result_data)[1])){
  name_list = c(name_list,as.character(data.name[data.name[,1] == rownames(result_data)[i],2]))
}
result_data$name = name_list


up_data = result_data[result_data$mean > 0,]
down_data = result_data[result_data$mean < 0,]

file_name = paste(working_directory,'/output/selected/new_',table_name,'_',selected_result_data_list_entry,'.txt',sep='')
print(file_name)
result_file_name = file_name
write.table(result_data,file=file_name,sep='\t',row.names=TRUE,col.names=NA)

file_name = paste(working_directory,'/output/selected/new_',table_name,'_',selected_result_data_list_entry,'_UP.txt',sep='')
print(file_name)
write.table(up_data,file=file_name,sep='\t',row.names=TRUE,col.names=NA)

file_name = paste(working_directory,'/output/selected/new_',table_name,'_',selected_result_data_list_entry,'_DOWN.txt',sep='')
print(file_name)
write.table(down_data,file=file_name,sep='\t',row.names=TRUE,col.names=NA)

up_data = result_data[result_data$mean > 0,]
down_data = result_data[result_data$mean < 0,]
reactome_data = result_data[,c('name','mean')]



uniprot_list = reactome_data[,'name']
uniprot_up = up_data[,'name']
uniprot_down = down_data[,'name']
new_list = c()
for(i in c(1:length(uniprot_list))){
  id = uniprot_list[i]
  #print(id)
  split_id = unlist(strsplit(id,'-'))
  #print(split_id)
  new_id = split_id[1]
  #print(new_id)
  new_list = c(new_list,new_id)
}
new_list
reactome_data$name = new_list
colnames(reactome_data)[1] = paste('#',colnames(reactome_data)[1],sep='')

reactome_data = remove_na_function_df(reactome_data)
reactome_up = reactome_data[reactome_data$mean > 0,]
reactome_down = reactome_data[reactome_data$mean < 0,]

reactome_file_name = paste(working_directory,'/output/selected/reactome_',table_name,'_',selected_result_data_list_entry,'.txt',sep='')
print(reactome_file_name)
write.table(reactome_data,file=reactome_file_name,sep='\t',row.names=FALSE,quote=FALSE)

reactome_file_name = paste(working_directory,'/output/selected/reactome_',table_name,'_',selected_result_data_list_entry,'_UP.txt',sep='')
print(reactome_file_name)
write.table(reactome_up,file=reactome_file_name,sep='\t',row.names=FALSE,quote=FALSE)

reactome_file_name = paste(working_directory,'/output/selected/reactome_',table_name,'_',selected_result_data_list_entry,'_DOWN.txt',sep='')
print(reactome_file_name)
write.table(reactome_down,file=reactome_file_name,sep='\t',row.names=FALSE,quote=FALSE)


cmd = paste('libreoffice --calc ',result_file_name,sep='')
print(cmd)
system(cmd,wait=FALSE)
print(result_file_name)
print(paste(workspace_filename))
save.image(workspace_filename)







print(workspace_filename)
save.image(workspace_filename)



































