setwd("/Users/sgarnett/Documents/Doctorate/Thesis/Thesis_Data/IPA_MQ_Gui")
print(getwd())
print("Loading : /Users/sgarnett/Documents/Doctorate/Thesis/Thesis_Data/IPA_MQ_Gui/workspace/Documents_RData_IPA.RData")
load("/Users/sgarnett/Documents/Doctorate/Thesis/Thesis_Data/IPA_MQ_Gui/workspace/Documents_RData_IPA.RData")
workspace_filename = "/Users/sgarnett/Documents/Doctorate/Thesis/Thesis_Data/IPA_MQ_Gui/workspace/Documents_RData_IPA.RData"
setwd('/Users/sgarnett/Documents/Doctorate/Thesis/Thesis_Data/IPA_MQ_Gui')
table_name = "df.MOL_H9_H10_log_2_ratio.c"
data = ""
sd_cutoff = 1
t_cutoff = 0.05
selected_result_data_list_entry = "_"
original_table = df.MOL_H9_H10_log_2_ratio
table_name = "df.MOL_H9_H10_log_2_ratio.c"
experiment_name = ""
opar = par()
par(oma=c(3,0,0,0),cex.lab=0.6,cex.axis = 0.6,cex.main=0.8) #(bottom,left,top,right )

table_title = paste(experiment_name,'\n',table_name,sep='')
file_title = paste(experiment_name,'_',table_name,sep='')
data = ''

cmd = paste('data = ',table_name,sep='')
print(cmd)
try(eval(parse(text=cmd)))
try(print(dim(data)))
try(print(colnames(data)))
try(print(head(data)))

standard_p_value = 0.05
wd = "/Users/sgarnett/Documents/Doctorate/Thesis/Thesis_Data/IPA_MQ_Gui"

working_directory = "/Users/sgarnett/Documents/Doctorate/Thesis/Thesis_Data/IPA_MQ_Gui"











##TOP42###







####50######



create_dir_function(paste(working_directory,'/output/selected',sep=''))

cmd = paste('data = ',table_name,sep='')
if(selected_result_data_list_entry != '_'){
  cmd = paste('data = ',table_name,'.',selected_result_data_list_entry,sep='') 
}
print(cmd)
eval(parse(text=cmd))
original_table = data
new_data = data
#print(dim(data))
##print(data['Q8TF72',])
#print(colnames(data))
#new_data['ID'] = gsub('"','',original_table[,1])
cmd = paste(table_name,'.',selected_result_data_list_entry,'.final = new_data',sep='')
print(cmd)
eval(parse(text=cmd))
##print(#print_data['Q8TF72',])


up_data = new_data[new_data$mean > 0,]
down_data = new_data[new_data$mean < 0,]

file_name = paste(working_directory,'/output/selected/new_',table_name,'_',selected_result_data_list_entry,'.txt',sep='')
print(file_name)
result_file_name = file_name
write.table(new_data,file=file_name,sep='\t',row.names=TRUE,col.names=NA,quote=FALSE)

file_name = paste(working_directory,'/output/selected/new_',table_name,'_',selected_result_data_list_entry,'_UP.txt',sep='')
print(file_name)
write.table(up_data,file=file_name,sep='\t',row.names=TRUE,col.names=NA,quote=FALSE)

file_name = paste(working_directory,'/output/selected/new_',table_name,'_',selected_result_data_list_entry,'_DOWN.txt',sep='')
print(file_name)
write.table(down_data,file=file_name,sep='\t',row.names=TRUE,col.names=NA,quote=FALSE)
#full_data = df.proteinGroups.txt.edited[df.proteinGroups.txt.edited$Protein.IDs%in%rownames(#print_data),]
#write.table(full_data,file=paste('output/',publish_folder,'/full_data_',selected_table_name,'.txt',sep=''),sep='\t')
#print(dim(new_data))
#print(colnames(new_data))
#write.table(new_data[,c('First.Protein.ID','mean_log2_ratio')],file=paste('output/selected/reactome_',table_name,'.txt',sep=''),sep='\t',row.names=FALSE)


cmd = paste('libreoffice --calc ',result_file_name,sep='')
print(cmd)
#system(cmd,wait=FALSE)
print(result_file_name)
print(paste(workspace_filename))
save.image(workspace_filename)






list_all = ls()
df_list = grep('df.',list_all)
file_name = paste(working_directory,'/rscripts/dataframe_list.txt',sep='')
print(file_name)
write.table(list_all[df_list],file=file_name,row.names=FALSE,col.names=FALSE)













































