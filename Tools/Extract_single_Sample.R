setwd("/home/sgarnett/Documents/RData/Neural_Stem_Cells/Differentiation_Experiment/140516_140515_combined_S2")
print(getwd())
print("Loading : /home/sgarnett/Documents/RData/Neural_Stem_Cells/Differentiation_Experiment/140516_140515_combined_S2/workspace/Test.RData")
load("/home/sgarnett/Documents/RData/Neural_Stem_Cells/Differentiation_Experiment/140516_140515_combined_S2/workspace/Test.RData")
setwd(working_directory)
selected_experiment_list = c('S2_00','S2_01','S2_02','S2_04','S2_08','S2_12')
table_name = "df.proteinGroups.txt.edited.iBAQ"
sample_prefix = "S2_"

































##TOP42###







####50######


quant_column_name_list = c()
for(sample in selected_experiment_list){
	quant_column_name_list = c(quant_column_name_list,colnames(data)[grep(sample,colnames(data))])
}
print(quant_column_name_list)


cat('\nQuant Columns :\n')
cat(paste('\t',quant_column_name_list,'\n'))

print(quant_column_name_list)
for(character in remove_character_list){
	#print(character)
	#print(paste(character))
	quant_column_name_list = gsub(paste(character), ".", quant_column_name_list)
}

print(quant_column_name_list)

quant_table_name = paste(table_name,'.',sample_prefix,sep='')
cat(paste('\nCREATE TABLE :',quant_table_name,'\n'))
cmd = paste(quant_table_name,' = data.frame(',table_name,'[,1])',sep='')
print(cmd)
eval(parse(text=cmd))
for(quant_column in quant_column_name_list){
	cmd = paste(quant_table_name,'$',quant_column,' = ',table_name,'$',quant_column,sep='')
	print(cmd)
	eval(parse(text=cmd))
}



cmd = paste('rownames(',quant_table_name,') = rownames(',table_name,')',sep='')
print(cmd)
eval(parse(text=cmd))
cmd = paste(quant_table_name,' = ',quant_table_name,'[-1]',sep='')
print(cmd)
eval(parse(text=cmd))

cmd = paste('print(dim(',quant_table_name,'))',sep='')
print(cmd)
eval(parse(text=cmd))
print(workspace_filename)
save.image(workspace_filename)
