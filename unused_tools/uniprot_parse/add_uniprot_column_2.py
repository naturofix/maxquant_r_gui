import os
import sys
import csv

sys.path.append('/export/python/DEVELOP/python_modules/')
import mysql_mod
import functions
host1 = 'localhost'
user1 = 'sgarnett'
pwd1 = 'sunshine'
database_name = 'uniprot'
uniprot_table = 'uniprot_sprot_dat'
protein_index_table = 'accession_index'
accession_table_name = 'accession_index'

curs = mysql_mod.MySQL_Connect(host1,user1,pwd1,database_name)

uniprot_table = mysql_mod.Choose_Table(curs,database_name)

com = 'describe %s' %(uniprot_table)
curs.execute(com)
uniprot_heading_list = curs.fetchall()

read_file_name = sys.argv[1]
read_row = csv.reader(open(read_file_name, 'rb'), delimiter='\t')
read_list = []
for row in read_row:
	##print ', '.join(row)
	read_list.append(row)

os.system('clear')
print 'This program will accept a tab delimited file, ask the user to choose a column from the file to use as an identifier, then add a column from the uniprot database to the end fo the file\n\n'

print "First few lines of file\n\n"
for read_line in read_list[:5]:
	print read_line
print '\n\n'
print 'selected the column from your file to use as an identifier'
selection = functions.Select_Entry(read_list[0],'no')[0]

print 'select the type of identifier this column is'
index_list = ['protein_accession','gene name']
index_table_list = ['AC_%s' %(uniprot_table),'GN_%s' %(uniprot_table)]
index_id_list = ['AC','GN']
#where_list = [''," AND WHERE property = 'Name'"]
index_name = functions.Select_Entry(index_list,'no')[0]
index_table_name = index_table_list[index_name]
index_id = index_id_list[index_name]
add_where = ''
#raw_input(index_name)

print 'Select the column from the uniprot database you wish to add to your file'
column_add_index = functions.Select_Entry(uniprot_heading_list,'no')[0]
column_title = uniprot_heading_list[column_add_index][0]
#raw_input(column_title)

if column_title == 'GN':
	uniprot_table = 'GN_%s' %(uniprot_table)
	add_where = ' AND property = "Name"'
if column_title == 'AC':
	uniprot_table = 'AC_%s' %(uniprot_table)

#print read_list[0]
#selection = 0
write_list = ['\t'.join(read_list[0]+[column_title])+'\n']
print '\nplease be patient while the file database is being scanned\n\n'
no_index = 0
no_index2 = 0 
multiples = 0
isoform = 0
for read_line_list in read_list[1:]:
	selected_column= read_line_list[selection]
	#print selected_column
	column_list = selected_column.split(';')
	id_list = []
	for entry in column_list:
		#print entry
		#if index_id == 'AC':
			#print entry.find('-')
		#	if entry.find('-') != -1:
		#		entry = entry[:entry.find('-')]
		#print entry
		com = 'SELECT ID from %s WHERE %s = "%s"' %(index_table_name,index_id,entry)
		#print com
		num = curs.execute(com)
		if num == 0:
			print entry
			if index_id == 'AC':
                       		 #print entry.find('-')
                        	if entry.find('-') != -1:
                                	entry2 = entry[:entry.find('-')]
                #print entry
                			com = 'SELECT ID from %s WHERE %s = "%s"' %(index_table_name,index_id,entry2)
                	#print com
                			num = curs.execute(com)
					if num > 0:
						isoform += 1
						entry = entry2

		if num > 0:
			entry_id = curs.fetchall()
			#print entry_id
			if len(entry_id) > 0:
				id_list.append(entry_id[0][0])
		else:
			no_index += 1
			print entry
			print no_index
	for entry_id in id_list:
		com = 'SELECT %s from %s WHERE ID = "%s"%s' %(column_title,uniprot_table,entry_id,add_where)
		#print com
		num = curs.execute(com)
		#print num
		if num > 0:	
			comment = curs.fetchall()
			#print comment
			if len(comment) > 0:
			#if comment != '()':
				comment = comment[0][0]
			#print comment
			read_line_list.append(comment)
			#print read_line_list
			write_list.append('\t'.join(read_line_list)+'\n')
		if num == 0:
			com = 'SELECT %s from %s WHERE ID = "%s"%s' %(column_title,uniprot_table,entry_id)
			num = curs.execute(com)
			if num > 0:
				comment = curs.fetchall()
                        #print comment
                        	if len(comment) > 0:
                        #if comment != '()':
                                	comment = comment[0][0]
                        #print comment
                        	read_line_list.append(comment)
                        	#print read_line_list
                        	write_list.append('\t'.join(read_line_list)+'\n')
			if num == 0:
				no_index2 += 1
		if num > 1:
			multiples += 1
#print write_list[0]
#print write_list[1]
print "First few lines of file \n\n\'"
for write_line in write_list[:5]:
	print write_line
write_file_name = read_file_name+'_'+column_title
write_file = open(write_file_name,'w')
write_file.writelines(write_list)
write_file.close()
print write_file_name
print len(read_list)
print len(write_list)
print no_index,' : no_index'
print no_index2,'  :no_index2'
print multiples,' : multiples'
print isoform,' : isoform'
