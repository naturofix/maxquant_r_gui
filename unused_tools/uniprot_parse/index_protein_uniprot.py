import os
import sys
import csv

sys.path.append('/export/python/DEVELOP/python_modules')
import mysql_mod

host1 = 'localhost'
user1 = 'sgarnett'
pwd1 = 'sunshine'
database_name = 'uniprot'

curs = mysql_mod.MySQL_Connect(host1,user1,pwd1,database_name)
table_name = mysql_mod.Choose_Table(curs,database_name)
accession_table_name = 'protein_index'

#curs = mysql_mod.MySQL_Connect(host1,user1,pwd1,database_name)



com = 'describe %s' %table_name
curs.execute(com)
heading_list = curs.fetchall()
print heading_list
#raw_input()
identifier_list = ['AC']
table_name_list = ['AC']
print 'please wait while database is indexed'
for index in range(len(identifier_list)):
	identifier = identifier_list[index]
	accession_table_name = '%s_%s' %(table_name_list[index],table_name)

	heading_list = [identifier+'_ID',identifier,'ID']
	attribute_dic = {}
	attribute = mysql_mod.Attribute(accession_table_name, heading_list,'VARCHAR(124)',attribute_dic)
	mysql_mod.Create_Table(accession_table_name,heading_list,attribute,curs,primary_key = 'VARCHAR(700)')

	com = 'SELECT ID,%s from %s' %(identifier,table_name)
	num = curs.execute(com)
	print num
	id_ac_list = curs.fetchall()
	#print id_ac_list
	warning = 0
	entry_number = 0
	id_number = 0
	none_number = 0
	for id_ac in id_ac_list:
		#print id_ac
		id_number += 1
		uniprot_id = id_ac[0]
		uniprot_ac = id_ac[1]
		if uniprot_ac != None:
			ac_list = uniprot_ac.split('; ')
			#print ac_list
			for ac in ac_list:
				#print ac
				if ac != '':
					accession = ac.replace(';','')
					ac_id = accession+'_'+uniprot_id
					
					values_list = [ac_id,accession,uniprot_id]
					#print values_list
					insert_list = mysql_mod.INSERT_INTO_table(accession_table_name,heading_list,values_list,curs)
					entry_number += 1
					if insert_list[2] == True:
						warning += 1
		else:
			none_number += 1
print len(id_ac_list)
print id_number
print entry_number
print none_number
print warning
