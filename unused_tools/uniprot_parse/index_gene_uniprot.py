import os
import sys
import csv

sys.path.append('/export/python/DEVELOP/python_modules/')
import mysql_mod

host1 = 'localhost'
user1 = 'sgarnett'
pwd1 = 'sunshine'
database_name = 'uniprot'

curs = mysql_mod.MySQL_Connect(host1,user1,pwd1,database_name)

table_name = mysql_mod.Choose_Table(curs,database_name)
#table_name = 'uniprot_sprot_dat'
accession_table_name = 'accession_index'


com = 'describe %s' %table_name
curs.execute(com)
heading_list = curs.fetchall()
print heading_list
#raw_input()
identifier_list = ['GN']
table_name_list = ['GN']
os.system('clear')
print 'please wait while database is indexed\n\n'
for index in range(len(identifier_list)):
	identifier = identifier_list[index]
	accession_table_name = '%s_%s' %(table_name_list[index],table_name)

	heading_list = [identifier+'_ID',identifier,'property','ID']
	attribute_dic = {}
	attribute = mysql_mod.Attribute(accession_table_name, heading_list,'VARCHAR(64)',attribute_dic)
	mysql_mod.Create_Table(accession_table_name,heading_list,attribute,curs,primary_key = 'VARCHAR(700)')

	com = 'SELECT ID,%s from %s' %(identifier,table_name)
	curs.execute(com)
	id_ac_list = curs.fetchall()
	warning = 0
	entry_number = 0
	id_number = 0
	none_number = 0
	for id_ac in id_ac_list:
		id_number += 1
		uniprot_id = id_ac[0]
		uniprot_ac = id_ac[1]
		if uniprot_ac != None:
			ac_list = uniprot_ac.split('; ')
			#print ac_list
			for ac in ac_list:
				if ac != '':
					accession = ac.replace(';','')
					ac_id = accession+'_'+uniprot_id
					accession_list = accession.split('=')
					accession_property = accession_list[0]
					accession = accession_list[1]	
					#ac_id = accession+'_'+uniprot_id
					values_list = [ac_id,accession,accession_property,uniprot_id]
					#print values_list
					insert_list = mysql_mod.INSERT_INTO_table(accession_table_name,heading_list,values_list,curs)
					entry_number += 1
					if insert_list[2] == True:
						warning += 1
		else:
			none_number += 1
	print id_number,' : lines in table'
	print entry_number,' : index entries'
	print none_number,' : no index found'
	print warning,' : warning'
