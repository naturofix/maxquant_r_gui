source('/home/sgarnett/Documents/programming/MaxQuant/01_Load_Data/00.config.R')
source('/home/sgarnett/Documents/programming/MaxQuant/00_config/00_03_Select_Data_Quantitation.R')




#cat("\\begin{verbatim}\n") 
suffix = ''
create_dir_function('images/boxplot')
for(file_name in selected_file_list){
	cn = 4
	#par(mfrow=c(2,2))
	for(suffix in selected_suffix_list){
		table_name =  paste('df.',file_name,'.',quant,suffix,sep='')
		
		base_table_name =  paste('df.',file_name,'.',quant,sep='')
		#print(suffix)
		
		cmd = paste('data = ',table_name,sep='')
		#print(table_name)
		#print(cmd)
		eval(parse(text=cmd))
		dim(data)
		#boxplot(log2(data))
		pdf(paste('images/boxplot/',table_name,'.pdf',sep=''))
		try(boxplot_function(log2(data),suffix,''))
		print(table_name)
		title(main=table_name,outer=TRUE,line=-1)
		#dev.copy(png,paste('images/boxplot/',base_table_name,'.png',sep=''))
		dev.off()
		print(table_name)
		print(getwd())
		#cat('\\newpage')
	}
}
#cat("\\end{verbatim}\n") 


