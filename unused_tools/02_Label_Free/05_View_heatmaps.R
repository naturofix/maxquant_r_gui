source('/home/sgarnett/Documents/programming/MaxQuant/01_Load_Data/00.config.R')
source('/home/sgarnett/Documents/programming/MaxQuant/00_config/00_03_Select_Data_Quantitation.R')










create_dir_function(paste(working_directory,'images/heatmap',sep=''))




#suffix = '.na.z'
table_name =  paste('df.',file_name,'.',quant,suffix,sep='')
#png(paste('images/heatmap/',table_name,'.png',sep=''))
cmd = paste('data = ',table_name,sep='')
eval(parse(text=cmd))
dim(data)
#boxplot(log2(data))
#par(bg='white')
try(heatmap(as.matrix((log2(data))),main=table_name,cex.main = 0.5,ylab='log2',na.rm=TRUE,cex.main=0.7))



#boxplot_function(log2(data),suffix,'')
#dev.copy(png,paste('images/heatmap/',table_name,'.png',sep=''))
#dev.off()

