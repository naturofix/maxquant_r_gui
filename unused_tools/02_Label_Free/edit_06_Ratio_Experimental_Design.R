setwd("/home/sgarnett/Documents/RData/SH-SY5Y/tariq_label_free/data/txt_all_30kda")
print(getwd())
print("Loading : /home/sgarnett/Documents/RData/SH-SY5Y/tariq_label_free/data/txt_all_30kda/workspace/Test.RData")
load("/home/sgarnett/Documents/RData/SH-SY5Y/tariq_label_free/data/txt_all_30kda/workspace/Test.RData")
setwd(working_directory)
sample_name_list = c('a','b','c','g','h','i')
selected_quant_list = c('iBAQ')
selected_suffix_list = c('','.na.z')
condition_1 = c('a','b','c')
condition_2 = c('g','h','i')
condition_list = c("condition_1","condition_2")








print(working_directory)
#print(paste(working_directory,'/images/density',sep=''))
create_dir_function(paste(working_directory,'/images/density',sep=''))
create_dir_function(paste(working_directory,'/images/histogram',sep=''))
for(file_name in selected_file_list){
	for(quant in selected_quant_list){ #changed to just intensity
	  condition_quant_list = c()
	  for(condition in condition_list){
	    print(condition)
	    sample_name_list = eval(parse(text=condition))
	    print(sample_name_list)
	    sample_quant = c()
	    for(sample in sample_name_list){
	      sample_quant = c(sample_quant,paste(quant,'.',sample,sep=''))
	    }
	    print(sample_quant)
	    cmd = paste(condition,'.quant = sample_quant',sep='')
	    print(cmd)
	    eval(parse(text=cmd))
	    condition_quant_list = c(condition_quant_list,paste(condition,'.quant',sep=''))
	  }
	  print(condition_quant_list)
		for(suffix in selected_suffix_list){
			table_name =  paste('df.',file_name,'.',quant,suffix,sep='')
			cat(paste('\n\t',table_name,'\n'))
			cmd = paste('data = ',table_name,sep='')
			print(cmd)
			eval(parse(text=cmd))
			dim(data)
			#cmd = paste('pairs_list = pair.',experiment_name,sep='')
			#print(cmd)
	#         try({
			#eval(parse(text=cmd))
			#cat(paste(pairs_list,'\n'))

			
			#Create New table for combining similar pairs
			cmd = paste('df.similar_ratio = data.frame(rownames(',table_name,'))',sep='')
			eval(parse(text=cmd))
			
			#cn = length(unlist(strsplit(pairs_list[1],split_character))) #GENERATES THE NUMBER OF COLUMNS FOR par
			#par(mfrow = c(2,cn),bg='white')
			similar_sd_list = c()
			for(p in c(1:length(condition_quant_list))){#GENERATES HISTOGRAMS FOR SIMILAR SAMPLES
        condition = condition_list[p]
			  condition_quant = condition_quant_list[p] #pairs list contains the comparisons for Experimental Design
			  entries_list = eval(parse(text=condition_quant))
			  cat(paste('\n',condition_quant,' : ',entries_list))
			  single_similar_sd_list = c()
			  cat('\n')
				
				#create another new table for individual similar pairs
				cmd = paste('df.single_similar_ratio = data.frame(rownames(',table_name,'))',sep='')
				eval(parse(text=cmd))
				
				#entries = strsplit(pair,split_character) #splits the pairs_list entry into its components
				#entries_list = unlist(entries)
				cmd = paste('entries_list_',p,' = entries_list',sep='') #ADD ENTRIES TO LIST OF COMPARISONS OF DIFFERENT sd'S
				#print(cmd)
				eval(parse(text=cmd))
				
				for(e in c(1:length(entries_list))){ #runs through each of the entries lists, assigning them there own variable
				entry = entries_list[e]
				cmd = paste(entry,' = ',table_name,'$',entry,sep='') 
				eval(parse(text=cmd))
				}
				cmd = paste('df.similar = data.frame(rownames(',table_name,'))',sep='')
				print(cmd)
				eval(parse(text=cmd))
				for(i in c(1:length(entries_list))){ #generated two seperate lists for each entry, generates ratio
  				entry_name_1 = entries_list[i]
  				df.similar[entry_name_1] = data[entry_name_1]
  				for(j in c(1:length(entries_list))){ #second list
  					if(i < j){ # ensured individual comparisons
  						entry_name_2 = entries_list[j]
  						comparison_name = paste(entries_list[i],'__',entries_list[j],sep='') #generates a name for the comparisons
  						cat(paste('\n\n',comparison_name,'\n'))
  						cmd = paste('ratio = ',entry_name_1,'/',entry_name_2,sep='') #generates a ratio
  						eval(parse(text=cmd))
  						cmd = paste('df.similar_ratio$',comparison_name,' = log2(ratio)',sep='') #adds the ratio to the similar table
  						eval(parse(text=cmd))
  						cmd = paste('df.single_similar_ratio$',comparison_name,' = log2(ratio)',sep='') #adds the ratio to the individual pair table
  						print(cmd)
  						eval(parse(text=cmd))
  						
  						#generates a histogram for each of the newly formed ratios
  						stat_list = simple_histogram_function(ratio,table_name,'shifted_mean',comparison_name)
  						shifted_stat_list = c(stat_list[1],stat_list[2],stat_list[1])
  						hist_lines_function(stat_list)
  						similar_sd_list = c(similar_sd_list,stat_list[2])
  						single_similar_sd_list = c(single_similar_sd_list,stat_list[2])
  						
  
  					}
  				}
				}
				
				rownames(df.similar) = df.similar[,1] #assignes the rownames
				df.similar = df.similar[-1] #removes column 1, which as the rowname
				new_table_name = paste(table_name,'.similar.',condition,sep='') #creates a new table with the pair name 
				cmd = paste(new_table_name,' = df.similar',sep='')
				eval(parse(text=cmd))
        #print(condition)
        #cat('######################################## ERROR HERE ##############################################')
				
				cmd = paste('single_similar_sd_list_',p,' = single_similar_sd_list',sep='')
				print(cmd)
				eval(parse(text=cmd))
				cat(paste('\nsingle similar sd list ',condition,': \n'))
				cat(paste(single_similar_sd_list))
				cmd = paste('sd_list.similar.',table_name,'.',condition,' = single_similar_sd_list',sep='')
				#print(cmd)
				eval(parse(text=cmd))
				rownames(df.single_similar_ratio) = df.single_similar_ratio[,1]
				df.single_similar_ratio = df.single_similar_ratio[-1]
				print('1')
				new_table_name = paste(table_name,'.log2_ratio.similar.',condition,sep='')
				cat(paste('\n',new_table_name,'\n'))
				cmd = paste(new_table_name,' = df.single_similar_ratio',sep='')
				print(cmd)
				eval(parse(text=cmd))
			} #GENERATES HISTOGRAMS FOR SIMILAR SAMPLES
			cat(paste('\nsimilar sd list : \n'))
			cat(paste(similar_sd_list))
			cmd = paste('sd_list.similar.',table_name,' = similar_sd_list',sep='')
			#print(cmd)
			eval(parse(text=cmd))
			
			dev.copy(png,paste(working_directory,'/images/histogram/',table_name,'_similar.png',sep=''))
			dev.off()
			
			
			
			# Generated similar ration dataframe and does a density plot
			#print(dim(df.similar_ratio))
			rownames(df.similar_ratio) = df.similar_ratio[,1]
			df.similar_ratio = df.similar_ratio[-1]
			new_table_name = paste(table_name,'.log2_ratio.similar',sep='')
			cat(paste('\n',new_table_name,'\n'))
			cmd = paste(new_table_name,' = df.similar_ratio',sep='')
			print(cmd)
			eval(parse(text=cmd))
			par(mfrow=c(1,1),bg='white')
			print('density_function')
			try(density_df_function(df.similar_ratio,new_table_name))
			
			cat(paste('\n\n\n DIFFERENT RATIOS \n\n'))
			
			different_sd_list = c()
			df.different_ratio = data.frame(rownames(data))
			#print(pairs_list)
			#print(length(pairs_list))
			par(mfrow = c(3,3),bg='white')
			print("# GENERATED HISTOGRAMS FOR DIFFERENT SAMPLE PAIRS")
			for(i in c(1:length(entries_list_1))){ # GENERATED HISTOGRAMS FOR DIFFERENT SAMPLE PAIRS
				for(j in c(1:length(entries_list_2))){
				cmd = paste('comparison_name = "',entries_list_1[i],'__',entries_list_2[j],'"',sep='')
				cat(paste('\n',comparison_name,'\n'))
				eval(parse(text=cmd))
				cmd = paste('ratio = ',entries_list_1[i],'/',entries_list_2[j],sep='')
				eval(parse(text=cmd))
				length(ratio)
				cmd = paste('df.different_ratio$',comparison_name,' = log2(ratio)',sep= '')
				eval(parse(text=cmd))
				dim(df.different_ratio)  
				try({
					stat_list = simple_histogram_function(ratio,'','shifted_mean',comparison_name)
					shifted_stat_list = c(stat_list[1],stat_list[2],stat_list[1])
					hist_lines_function(stat_list)
					different_sd_list = c(different_sd_list,stat_list[2])
				})
				}
			}# GENERATED HISTOGRAMS FOR DIFFERENT SAMPLE PAIRS
			dev.copy(png,paste(working_directory,'/images/histogram/',table_name,'_different.png',sep=''))
			dev.off()
			cat(paste('\n different sd list : \n'))
			cat(paste(different_sd_list))
			
			cat(paste('\nmean different sd = ',mean(different_sd_list)))
			#cat(paste('\nvar different sd = ',var(different_sd_list)))
			cat(paste('\ncv different sd = ',cv_function(different_sd_list),'%\n'))
			cat(paste('\nmean similar sd = ',mean(similar_sd_list)))
			#cat(paste('\nvar similar sd = ',var(similar_sd_list)))
			cat(paste('\ncv similar sd = ',cv_function(similar_sd_list),'%\n\n'))
			cmd = paste('sd_list.different.',table_name,' = different_sd_list',sep='')
			#print(cmd)
			eval(parse(text=cmd))
			
			#print(dim(df.different_ratio))
			rownames(df.different_ratio) = df.different_ratio[,1]
			df.different_ratio = df.different_ratio[-1]
			#print(dim(df.different_ratio))
			
			new_table_name = paste(table_name,'.log2_ratio.different',sep='')
			cmd = paste(new_table_name,' = df.different_ratio',sep='')
			print(cmd)
			eval(parse(text=cmd))
			
			par(mfrow = c(1,1),bg='white')
			
			try(density_df_function(df.different_ratio,new_table_name))
			par(mfrow = c(1,2),bg='white')
			
			
			try({
				boxplot(single_similar_sd_list_1,single_similar_sd_list_2,similar_sd_list,different_sd_list,main=table_name,sub='sd_lists',col=c('blue','blue','blue','red'),names=c('condition 1','condition 2','similar','difference'),las=3)
				#dev.copy(png,paste(working_directory',/images/boxplot/',table_name,'_sd_lists.png',sep=''))
				#dev.off()
				double_density_plot_function(df.similar_ratio,df.different_ratio,table_name)
				dev.copy(png,paste(working_directory,'/images/density/',table_name,'.png',sep=''))
				dev.off()
			})

	#        })

			#eval_function(cmd)
	#         var_data = data.frame(rownames(data))
	#         var_data[pairs_list[1]] = single_similar_sd_list_1
	#         var_data[pairs_list[2]] = single_similar_sd_list_2
	#         var_data['difference_comparison'] = different_sd_list
			
		}#suffix

	}#file_name
}


print(workspace_filename)
save.image(workspace_filename)
