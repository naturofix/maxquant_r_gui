setwd('/home/sgarnett/Documents/RData/Neural Stem Cells/iTRAQ_Cambridge January_2013')
load('workspace/Cambridge_iTRAQ_January_2013.new.RData')

for(table.name.list.entry in table.name.lists){
  cmd = paste('table.name.list = ',table.name.list.entry,sep='')
  eval(parse(text = cmd))
  print(table.name.list)
  
  
  #REMOVE NA and ZEROs
  #df.nz.tmp = df.intensity.all[df.intensity.all > 0.00000001,]
  #df.intensity.all.nzna = df.nz.tmp[complete.cases(df.nz.tmp),]
  
  for(table.name in table.name.list){
    for(sample in sample.names){
      sample.table.name = paste(table.name,'.',sample,sep='')
      print(sample.table.name)
      
      cmd = paste('df.tmp <- data.frame(',sample.table.name,'[,c(2:9)])',sep='')
      #print(cmd)
      eval(parse(text=cmd))
      print(dim(df.tmp))
      df.tmp.na = df.tmp[complete.cases(df.tmp),]
      #print(dim(df.tmp.na))
      #df.tmp.nz <- df.tmp[is.finite(rowSums(log(df.tmp[-1]))),] 
      df.tmp.nanz_a <- df.tmp[is.finite(rowSums(log(df.tmp[-1]))),] 
      df.tmp.nanz_r = df.tmp.na[df.tmp.na > 0,]
      df.tmp.nanz_r = df.tmp.nanz_r[complete.cases(df.tmp.nanz_r),]
      
      cmd = paste(sample.table.name,'.nanz_r <- df.tmp.nanz_r',sep = '')
      print(cmd)
      print(dim(df.tmp.nanz_r))
      eval(parse(text = cmd))
      cmd = paste(sample.table.name,'.na <- df.tmp.na',sep = '')
      print(dim(df.tmp.na))
      print(cmd)
      eval(parse(text = cmd))
      cmd = paste(sample.table.name,'.nanz_a <- df.tmp.nanz_a',sep = '')
      print(dim(df.tmp.nanz_a))
      print(cmd)
      eval(parse(text = cmd))
    }
  }
  add_suffix_list = c('.nanz_a','.nanz_r')
  for(add_suffix in add_suffix_list){
    #create dataframe divided by column - then log2
    for(timepoint in c('P','C2')){
      print(timepoint)
      for(table.name in table.name.list){
        for(sample in sample.names){
          sample.table.name = paste(table.name,'.',sample,add_suffix,sep='')
          print(sample.table.name)
          cmd = paste(sample.table.name,'.',timepoint,' = ',sample.table.name,'/',sample.table.name,'$',timepoint,sep = '')
          print(cmd)
          eval(parse(text = cmd))
          cmd = paste('df.tmp <- ',sample.table.name,'.',timepoint,sep='')
          print(cmd)
          eval(parse(text=cmd))
          df.tmp.nanz_a <- df.tmp[is.finite(rowSums(log(df.tmp[-1]))),]
          cmd = paste(sample.table.name,'.',timepoint,'.nz_a.log2 <- log2(df.tmp.nanz_a)',sep='')
          print(cmd)
          eval(parse(text=cmd))
        }
        
      }
    }
  }
  
  
  #Create variation column in table by row
  var_list = c('sd','var','mad')
  for(table.name in table.name.list){
    for(sample in sample.names){
      sample.table.name = paste(table.name,'.',sample,'.nanz_a.P.nz_a.log2',sep='')
      cmd = paste('data = ',sample.table.name,'[,c(1:6)]',sep='')
      print(cmd)
      eval(parse(text = cmd))
      for(var_l in var_list){
        cmd = paste('variant = apply(data,1,',var_l,')',sep='')
        print(cmd)
        eval(parse(text = cmd))
        cmd = paste(sample.table.name,'$row_',var_l,' = variant',sep='')
        print(cmd)
        eval(parse(text = cmd))
      }
    }
  }
  
  #Add linear Regression value column
  timepoint_series = c(0,1,2,4,8,12)
  for(table.name in table.name.list){
    for(sample in sample.names){
      sample.table.name = paste(table.name,'.',sample,'.nanz_a.P.nz_a.log2',sep='')
      cmd = paste('data = ',sample.table.name,'[,c(1:6)]',sep='')
      print(cmd)
      eval(parse(text = cmd))
      
      data_slope = c()
      for(i in c(1:dim(data)[1])){
        #print(i)
        n_data = as.numeric((data[i,c(1:6)]))
        #n_data
        #plot(timepoint_series,n_data,type='l')
        #abline(lm1,col=1)
        lm1 = lm(as.numeric(data[i,c(2:6)])~c(1,2,4,8,12))
        #lm1$coefficients[1]
        n_slope <- as.numeric(lm1$coefficients[2])
        #print(n_slope)
        data_slope <- c(data_slope,(n_slope))
      }
      #data_slope
      cmd = paste(sample.table.name,'$row_lm = data_slope',sep='')
      print(cmd)
      eval(parse(text = cmd))
    }
  }
}
### GENERATE ALLL PROTEIN GROUPS DATA FRAMES
# table_suffix = '.nanz_a.'
sample_name_list = c('HB1025','HB901')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.a = df.proteinGroups.nc.HB1025.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.b = df.proteinGroups.nc.HB901.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.NS.proteinGroups.qn.log2 = merge(df.a,df.b, by='row.names')
rownames(df.NS.proteinGroups.qn.log2) <- df.NS.proteinGroups.qn.log2[,1]
head(df.NS.proteinGroups.qn.log2)
df.NS.proteinGroups.qn.log2 = df.NS.proteinGroups.qn.log2[,-1]
colnames(df.NS.proteinGroups.qn.log2) = column_name_list
# 
df.NS.proteinGroups.qn.log2.ALL = merge(df.a,df.b, by='row.names',all=TRUE)
rownames(df.NS.proteinGroups.qn.log2.ALL) <- df.NS.proteinGroups.qn.log2.ALL[,1]
head(df.NS.proteinGroups.qn.log2.ALL)
df.NS.proteinGroups.qn.log2.ALL = df.NS.proteinGroups.qn.log2.ALL[,-1]
colnames(df.NS.proteinGroups.qn.log2.ALL) = column_name_list
write.table(df.NS.proteinGroups.qn.log2, file = 'output/df.NS.proteinGroups.qn.log2', row.name = TRUE, col.names = NA, sep = '\t')

# 
# 

sample_name_list = c('Sai1','Sai2','Sai3')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.Sai1 = df.proteinGroups.nc.Sai1.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.Sai2 = df.proteinGroups.nc.Sai2.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.Sai3 = df.proteinGroups.nc.Sai3.nanz_a.qn.P.nz_a.log2[,c(1:8)]
#only overlap
df.NES.proteinGroups.qn.log2 = merge(df.Sai1,df.Sai2, by='row.names')
rownames(df.NES.proteinGroups.qn.log2) <- df.NES.proteinGroups.qn.log2[,1]
head(df.NES.proteinGroups.qn.log2)
df.NES.proteinGroups.qn.log2 = df.NES.proteinGroups.qn.log2[,-1]
df.NES.proteinGroups.qn.log2 = merge(df.NES.proteinGroups.qn.log2,df.Sai3, by='row.names')
rownames(df.NES.proteinGroups.qn.log2) <- df.NES.proteinGroups.qn.log2[,1]
head(df.NES.proteinGroups.qn.log2)
df.NES.proteinGroups.qn.log2 = df.NES.proteinGroups.qn.log2[,-1]
colnames(df.NES.proteinGroups.qn.log2) = column_name_list
write.table(df.NES.proteinGroups.qn.log2, file = 'output/df.NES.proteinGroups.qn.log2', row.name = TRUE, col.names = NA, sep = '\t')
#ALL
df.NES.proteinGroups.qn.log2.ALL = merge(df.Sai1,df.Sai2, by='row.names',all=TRUE)
rownames(df.NES.proteinGroups.qn.log2.ALL) <- df.NES.proteinGroups.qn.log2.ALL[,1]
head(df.NES.proteinGroups.qn.log2.ALL)
df.NES.proteinGroups.qn.log2.ALL = df.NES.proteinGroups.qn.log2.ALL[,-1]
# 
df.NES.proteinGroups.qn.log2.ALL = merge(df.NES.proteinGroups.qn.log2.ALL,df.Sai3, by='row.names',all=TRUE)
rownames(df.NES.proteinGroups.qn.log2.ALL) <- df.NES.proteinGroups.qn.log2.ALL[,1]
head(df.NES.proteinGroups.qn.log2.ALL)
df.NES.proteinGroups.qn.log2.ALL = df.NES.proteinGroups.qn.log2.ALL[,-1]
colnames(df.NES.proteinGroups.qn.log2.ALL) = column_name_list
write.table(df.NES.proteinGroups.qn.log2.ALL, file = 'output/df.NES.proteinGroups.qn.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')

#combine NS and NES all
df.proteinGroups.qn.log2.ALL = merge(df.NES.proteinGroups.qn.log2.ALL,df.NS.proteinGroups.qn.log2.ALL, by='row.names',all=TRUE)
rownames(df.proteinGroups.qn.log2.ALL) <- df.proteinGroups.qn.log2.ALL[,1]
head(df.proteinGroups.qn.log2.ALL)
df.proteinGroups.qn.log2.ALL = df.proteinGroups.qn.log2.ALL[,-1]
write.table(df.proteinGroups.qn.log2.ALL, file = 'output/df.proteinGroups.qn.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')


### GENERATE ALLL PEPTIDE DATA FRAMES
sample_name_list = c('HB1025','HB901')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.a = df.peptides.nc.HB1025.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.b = df.peptides.nc.HB901.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.NS.peptides.qn.log2 = merge(df.a,df.b, by='row.names')
rownames(df.NS.peptides.qn.log2) <- df.NS.peptides.qn.log2[,1]
head(df.NS.peptides.qn.log2)
df.NS.peptides.qn.log2 = df.NS.peptides.qn.log2[,-1]
colnames(df.NS.peptides.qn.log2) = column_name_list
# 
df.NS.peptides.qn.log2.ALL = merge(df.a,df.b, by='row.names',all=TRUE)
rownames(df.NS.peptides.qn.log2.ALL) <- df.NS.peptides.qn.log2.ALL[,1]
head(df.NS.peptides.qn.log2.ALL)
df.NS.peptides.qn.log2.ALL = df.NS.peptides.qn.log2.ALL[,-1]
colnames(df.NS.peptides.qn.log2.ALL) = column_name_list
write.table(df.NS.peptides.qn.log2, file = 'output/df.NS.peptides.qn.log2', row.name = TRUE, col.names = NA, sep = '\t')


sample_name_list = c('Sai1','Sai2','Sai3')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.Sai1 = df.peptides.nc.Sai1.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.Sai2 = df.peptides.nc.Sai2.nanz_a.qn.P.nz_a.log2[,c(1:8)]
df.Sai3 = df.peptides.nc.Sai3.nanz_a.qn.P.nz_a.log2[,c(1:8)]
#only overlap
df.NES.peptides.qn.log2 = merge(df.Sai1,df.Sai2, by='row.names')
rownames(df.NES.peptides.qn.log2) <- df.NES.peptides.qn.log2[,1]
head(df.NES.peptides.qn.log2)
df.NES.peptides.qn.log2 = df.NES.peptides.qn.log2[,-1]
df.NES.peptides.qn.log2 = merge(df.NES.peptides.qn.log2,df.Sai3, by='row.names')
rownames(df.NES.peptides.qn.log2) <- df.NES.peptides.qn.log2[,1]
head(df.NES.peptides.qn.log2)
df.NES.peptides.qn.log2 = df.NES.peptides.qn.log2[,-1]
colnames(df.NES.peptides.qn.log2) = column_name_list
write.table(df.NES.peptides.qn.log2, file = 'output/df.NES.peptides.qn.log2', row.name = TRUE, col.names = NA, sep = '\t')
#ALL
df.NES.peptides.qn.log2.ALL = merge(df.Sai1,df.Sai2, by='row.names',all=TRUE)
rownames(df.NES.peptides.qn.log2.ALL) <- df.NES.peptides.qn.log2.ALL[,1]
head(df.NES.peptides.qn.log2.ALL)
df.NES.peptides.qn.log2.ALL = df.NES.peptides.qn.log2.ALL[,-1]
# 
df.NES.peptides.qn.log2.ALL = merge(df.NES.peptides.qn.log2.ALL,df.Sai3, by='row.names',all=TRUE)
rownames(df.NES.peptides.qn.log2.ALL) <- df.NES.peptides.qn.log2.ALL[,1]
head(df.NES.peptides.qn.log2.ALL)
df.NES.peptides.qn.log2.ALL = df.NES.peptides.qn.log2.ALL[,-1]
colnames(df.NES.peptides.qn.log2.ALL) = column_name_list
write.table(df.NES.peptides.qn.log2.ALL, file = 'output/df.NES.peptides.qn.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')

#combine NS and NES all
df.peptides.qn.log2.ALL = merge(df.NES.peptides.qn.log2.ALL,df.NS.peptides.qn.log2.ALL, by='row.names',all=TRUE)
rownames(df.peptides.qn.log2.ALL) <- df.peptides.qn.log2.ALL[,1]
head(df.peptides.qn.log2.ALL)
df.peptides.qn.log2.ALL = df.peptides.qn.log2.ALL[,-1]
write.table(df.peptides.qn.log2.ALL, file = 'output/df.peptides.qn.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')




#df.NES.All.proteinGroups = rbind(df.Sai1,df.Sai2,df.Sai3)
#write.table(df.NES.All.proteinGroups, file = 'output/df.NES.All.proteinGroups', row.name = TRUE, col.names = TRUE, sep = '\t')
# 
# dim(df.NS.proteinGroups.qn.log2)
# dim(df.NES.proteinGroups.qn.log2)
# df.ALL.proteinGroups.qn.log2 = merge(df.NS.proteinGroups.qn.log2,df.NES.proteinGroups.qn.log2,by='row.names')
# rownames(df.ALL.proteinGroups.qn.log2) <- df.ALL.proteinGroups.qn.log2[,1]
# head(df.ALL.proteinGroups.qn.log2)
# df.ALL.proteinGroups.qn.log2 = df.ALL.proteinGroups.qn.log2[,-1]
# dim(df.ALL.proteinGroups.qn.log2)
# 
# 
# save.image(workspace_filename_location)
# if(data_location != 'local'){
#   save.image(local_workspace_filename_location)
# }
# 
# 
# df.HB1025 = df.proteinGroups.HB1025
# colnames()
# df.HB901 = df.proteinGroups.HB1025
# df.Sai1 = df.proteinGroups.Sai1
# df.Sai2 = df.proteinGroups.Sai2
# df.Sai3 = df.proteinGroups.Sai3
# 
# df.ALL = merge(df.HB1025,df.HB901,by='row.names',all=TRUE)
# rownames(df.ALL) <- df.ALL[,1]
# df.ALL = df.ALL[,-1]
# 
# df.ALL = merge(df.ALL,df.Sai3,by='row.names',all=TRUE)
# rownames(df.ALL) <- df.ALL[,1]
# df.ALL = df.ALL[,-1]
# 
# save.image('workspace/Cambridge_iTRAQ_January_2013.nc.RData')
# save.image('workspace/Cambridge_iTRAQ_January_2013.nc.RData.backup')
# load('/run/user/sgarnett/gvfs/sftp:host=blackburn2.uct.ac.za,user=sgarnett/blackburn3/sgarnett/NSC_3/MaxQuant/iTRAQ_January_2013/R/workspace/Cambridge_iTRAQ_January_2013.RData')
# load()
print(workspace_filename_location)
save.image(workspace_filename_location)
load(workspace_filename_location)


### GENERATE ALLL PROTEIN GROUPS DATA FRAMES _ Control Mean
# table_suffix = '.nanz_a.'
sample_name_list = c('HB1025','HB901')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.a = df.proteinGroups.nc.HB1025.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.b = df.proteinGroups.nc.HB901.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.NS.proteinGroups.qn.CM.log2 = merge(df.a,df.b, by='row.names')
rownames(df.NS.proteinGroups.qn.CM.log2) <- df.NS.proteinGroups.qn.CM.log2[,1]
head(df.NS.proteinGroups.qn.CM.log2)
df.NS.proteinGroups.qn.CM.log2 = df.NS.proteinGroups.qn.CM.log2[,-1]
colnames(df.NS.proteinGroups.qn.CM.log2) = column_name_list
# 
df.NS.proteinGroups.qn.CM.log2.ALL = merge(df.a,df.b, by='row.names',all=TRUE)
rownames(df.NS.proteinGroups.qn.CM.log2.ALL) <- df.NS.proteinGroups.qn.CM.log2.ALL[,1]
head(df.NS.proteinGroups.qn.CM.log2.ALL)
df.NS.proteinGroups.qn.CM.log2.ALL = df.NS.proteinGroups.qn.CM.log2.ALL[,-1]
colnames(df.NS.proteinGroups.qn.CM.log2.ALL) = column_name_list
write.table(df.NS.proteinGroups.qn.CM.log2, file = 'output/df.NS.proteinGroups.qn.CM.log2', row.name = TRUE, col.names = NA, sep = '\t')

# 
# 

sample_name_list = c('Sai1','Sai2','Sai3')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.Sai1 = df.proteinGroups.nc.Sai1.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.Sai2 = df.proteinGroups.nc.Sai2.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.Sai3 = df.proteinGroups.nc.Sai3.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
#only overlap
df.NES.proteinGroups.qn.CM.log2 = merge(df.Sai1,df.Sai2, by='row.names')
rownames(df.NES.proteinGroups.qn.CM.log2) <- df.NES.proteinGroups.qn.CM.log2[,1]
head(df.NES.proteinGroups.qn.CM.log2)
df.NES.proteinGroups.qn.CM.log2 = df.NES.proteinGroups.qn.CM.log2[,-1]
df.NES.proteinGroups.qn.CM.log2 = merge(df.NES.proteinGroups.qn.CM.log2,df.Sai3, by='row.names')
rownames(df.NES.proteinGroups.qn.CM.log2) <- df.NES.proteinGroups.qn.CM.log2[,1]
head(df.NES.proteinGroups.qn.CM.log2)
df.NES.proteinGroups.qn.CM.log2 = df.NES.proteinGroups.qn.CM.log2[,-1]
colnames(df.NES.proteinGroups.qn.CM.log2) = column_name_list
write.table(df.NES.proteinGroups.qn.CM.log2, file = 'output/df.NES.proteinGroups.qn.CM.log2', row.name = TRUE, col.names = NA, sep = '\t')
#ALL
df.NES.proteinGroups.qn.CM.log2.ALL = merge(df.Sai1,df.Sai2, by='row.names',all=TRUE)
rownames(df.NES.proteinGroups.qn.CM.log2.ALL) <- df.NES.proteinGroups.qn.CM.log2.ALL[,1]
head(df.NES.proteinGroups.qn.CM.log2.ALL)
df.NES.proteinGroups.qn.CM.log2.ALL = df.NES.proteinGroups.qn.CM.log2.ALL[,-1]
# 
df.NES.proteinGroups.qn.CM.log2.ALL = merge(df.NES.proteinGroups.qn.CM.log2.ALL,df.Sai3, by='row.names',all=TRUE)
rownames(df.NES.proteinGroups.qn.CM.log2.ALL) <- df.NES.proteinGroups.qn.CM.log2.ALL[,1]
head(df.NES.proteinGroups.qn.CM.log2.ALL)
df.NES.proteinGroups.qn.CM.log2.ALL = df.NES.proteinGroups.qn.CM.log2.ALL[,-1]
colnames(df.NES.proteinGroups.qn.CM.log2.ALL) = column_name_list
write.table(df.NES.proteinGroups.qn.CM.log2.ALL, file = 'output/df.NES.proteinGroups.qn.CM.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')

#combine NS and NES all
df.proteinGroups.qn.CM.log2.ALL = merge(df.NES.proteinGroups.qn.CM.log2.ALL,df.NS.proteinGroups.qn.CM.log2.ALL, by='row.names',all=TRUE)
rownames(df.proteinGroups.qn.CM.log2.ALL) <- df.proteinGroups.qn.CM.log2.ALL[,1]
head(df.proteinGroups.qn.CM.log2.ALL)
df.proteinGroups.qn.CM.log2.ALL = df.proteinGroups.qn.CM.log2.ALL[,-1]
write.table(df.proteinGroups.qn.CM.log2.ALL, file = 'output/df.proteinGroups.qn.CM.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')


### GENERATE ALLL PEPTIDE DATA FRAMES
sample_name_list = c('HB1025','HB901')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.a = df.peptides.nc.HB1025.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.b = df.peptides.nc.HB901.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.NS.peptides.qn.CM.log2 = merge(df.a,df.b, by='row.names')
rownames(df.NS.peptides.qn.CM.log2) <- df.NS.peptides.qn.CM.log2[,1]
head(df.NS.peptides.qn.CM.log2)
df.NS.peptides.qn.CM.log2 = df.NS.peptides.qn.CM.log2[,-1]
colnames(df.NS.peptides.qn.CM.log2) = column_name_list
# 
df.NS.peptides.qn.CM.log2.ALL = merge(df.a,df.b, by='row.names',all=TRUE)
rownames(df.NS.peptides.qn.CM.log2.ALL) <- df.NS.peptides.qn.CM.log2.ALL[,1]
head(df.NS.peptides.qn.CM.log2.ALL)
df.NS.peptides.qn.CM.log2.ALL = df.NS.peptides.qn.CM.log2.ALL[,-1]
colnames(df.NS.peptides.qn.CM.log2.ALL) = column_name_list
write.table(df.NS.peptides.qn.CM.log2, file = 'output/df.NS.peptides.qn.CM.log2', row.name = TRUE, col.names = NA, sep = '\t')


sample_name_list = c('Sai1','Sai2','Sai3')
column_name_list = ''
for(sample in sample_name_list){
  for(timepoint in timepoint.list){
    column_name_list = c(column_name_list,paste(sample,'_',timepoint,sep=''))
  }
}
column_name_list = column_name_list[2:length(column_name_list)]
column_name_list
df.Sai1 = df.peptides.nc.Sai1.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.Sai2 = df.peptides.nc.Sai2.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
df.Sai3 = df.peptides.nc.Sai3.nanz_a.qn.Control_row_mean.nz_a.log2[,c(1:8)]
#only overlap
df.NES.peptides.qn.CM.log2 = merge(df.Sai1,df.Sai2, by='row.names')
rownames(df.NES.peptides.qn.CM.log2) <- df.NES.peptides.qn.CM.log2[,1]
head(df.NES.peptides.qn.CM.log2)
df.NES.peptides.qn.CM.log2 = df.NES.peptides.qn.CM.log2[,-1]
df.NES.peptides.qn.CM.log2 = merge(df.NES.peptides.qn.CM.log2,df.Sai3, by='row.names')
rownames(df.NES.peptides.qn.CM.log2) <- df.NES.peptides.qn.CM.log2[,1]
head(df.NES.peptides.qn.CM.log2)
df.NES.peptides.qn.CM.log2 = df.NES.peptides.qn.CM.log2[,-1]
colnames(df.NES.peptides.qn.CM.log2) = column_name_list
write.table(df.NES.peptides.qn.CM.log2, file = 'output/df.NES.peptides.qn.CM.log2', row.name = TRUE, col.names = NA, sep = '\t')
#ALL
df.NES.peptides.qn.CM.log2.ALL = merge(df.Sai1,df.Sai2, by='row.names',all=TRUE)
rownames(df.NES.peptides.qn.CM.log2.ALL) <- df.NES.peptides.qn.CM.log2.ALL[,1]
head(df.NES.peptides.qn.CM.log2.ALL)
df.NES.peptides.qn.CM.log2.ALL = df.NES.peptides.qn.CM.log2.ALL[,-1]
# 
df.NES.peptides.qn.CM.log2.ALL = merge(df.NES.peptides.qn.CM.log2.ALL,df.Sai3, by='row.names',all=TRUE)
rownames(df.NES.peptides.qn.CM.log2.ALL) <- df.NES.peptides.qn.CM.log2.ALL[,1]
head(df.NES.peptides.qn.CM.log2.ALL)
df.NES.peptides.qn.CM.log2.ALL = df.NES.peptides.qn.CM.log2.ALL[,-1]
colnames(df.NES.peptides.qn.CM.log2.ALL) = column_name_list
write.table(df.NES.peptides.qn.CM.log2.ALL, file = 'output/df.NES.peptides.qn.CM.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')

#combine NS and NES all
df.peptides.qn.CM.log2.ALL = merge(df.NES.peptides.qn.CM.log2.ALL,df.NS.peptides.qn.CM.log2.ALL, by='row.names',all=TRUE)
rownames(df.peptides.qn.CM.log2.ALL) <- df.peptides.qn.CM.log2.ALL[,1]
head(df.peptides.qn.CM.log2.ALL)
df.peptides.qn.CM.log2.ALL = df.peptides.qn.CM.log2.ALL[,-1]
write.table(df.peptides.qn.CM.log2.ALL, file = 'output/df.peptides.qn.CM.log2.ALL', row.name = TRUE, col.names = NA, sep = '\t')




#df.NES.All.proteinGroups = rbind(df.Sai1,df.Sai2,df.Sai3)
#write.table(df.NES.All.proteinGroups, file = 'output/df.NES.All.proteinGroups', row.name = TRUE, col.names = TRUE, sep = '\t')
# 
# dim(df.NS.proteinGroups.qn.log2)
# dim(df.NES.proteinGroups.qn.log2)
# df.ALL.proteinGroups.qn.log2 = merge(df.NS.proteinGroups.qn.log2,df.NES.proteinGroups.qn.log2,by='row.names')
# rownames(df.ALL.proteinGroups.qn.log2) <- df.ALL.proteinGroups.qn.log2[,1]
# head(df.ALL.proteinGroups.qn.log2)
# df.ALL.proteinGroups.qn.log2 = df.ALL.proteinGroups.qn.log2[,-1]
# dim(df.ALL.proteinGroups.qn.log2)
# 
# 
# save.image(workspace_filename_location)
# if(data_location != 'local'){
#   save.image(local_workspace_filename_location)
# }
# 
# 
# df.HB1025 = df.proteinGroups.HB1025
# colnames()
# df.HB901 = df.proteinGroups.HB1025
# df.Sai1 = df.proteinGroups.Sai1
# df.Sai2 = df.proteinGroups.Sai2
# df.Sai3 = df.proteinGroups.Sai3
# 
# df.ALL = merge(df.HB1025,df.HB901,by='row.names',all=TRUE)
# rownames(df.ALL) <- df.ALL[,1]
# df.ALL = df.ALL[,-1]
# 
# df.ALL = merge(df.ALL,df.Sai3,by='row.names',all=TRUE)
# rownames(df.ALL) <- df.ALL[,1]
# df.ALL = df.ALL[,-1]
# 
# save.image('workspace/Cambridge_iTRAQ_January_2013.nc.RData')
# save.image('workspace/Cambridge_iTRAQ_January_2013.nc.RData.backup')
# load('/run/user/sgarnett/gvfs/sftp:host=blackburn2.uct.ac.za,user=sgarnett/blackburn3/sgarnett/NSC_3/MaxQuant/iTRAQ_January_2013/R/workspace/Cambridge_iTRAQ_January_2013.RData')
# load()
print(workspace_filename_location)
save.image(workspace_filename_location)
load(workspace_filename_location)

