setwd('/home/sgarnett/Documents/RData/Neural Stem Cells/iTRAQ_Cambridge January_2013')
load('workspace/Cambridge_iTRAQ_January_2013.new.RData')


table.name.list = not.corrected.table.name.list
table_suffix = '.nanz_a.qn.P.nz_a.log2'


NES.sample.names = c('Sai1','Sai2','Sai3')
NS.sample.names = c('HB901','HB1025')
cell_type_list = c('NES','NS')
var_list = c('sd','var','mad')

var_list = c('sd')
#generate matplot of timelines  
table_suffix_list = c('.nanz_a','.nanz_a.qn')

#for(table.name.list.entry in table.name.lists){
#  cmd = paste('table.name.list = ',table.name.list.entry,sep='')
#  eval(parse(text = cmd))
#  print(table.name.list)  

#table_suffix = '.nanz_a.qn'
for(table.name in table.name.list){
  rbind_text = ''
  for(cell_type in cell_type_list){
    cmd = paste('sample.names = ',cell_type,'.sample.names',sep='')
    eval(parse(text=cmd))
    for(sample in sample.names){
      sample.table.name = paste(table.name,'.',sample,table_suffix,sep='')
      #all_sample_table.name = paste('df.ALL_de.',sample.table.name)
      
      cmd = paste('data = (',sample.table.name,')',sep='')
      print(cmd)
      eval(parse(text = cmd))
      #cmd = paste(all_sample_table_name = data.frame())
      #print(cmd)
      #eval(parse(text=cmd))
  
      for(var_l in var_list){            
        cmd = paste("matplot(t(data[,c(1:6)]),type='l',main=sample.table.name,sub='",var_l," all')",sep='')
        print(cmd)
        eval(parse(text=cmd))
        cmd = paste("matplot(t(data[data$row_",var_l," > mean(data$row_",var_l,"),c(1:6)]),type='l',main=sample.table.name,sub='",var_l," > mean')",sep='')
        print(cmd)
        eval(parse(text=cmd))
        cmd = paste("matplot(t(data[data$row_",var_l," < mean(data$row_",var_l,"),c(1:6)]),type='l',main=sample.table.name,sub='",var_l," < mean')",sep='')
        print(cmd)
        eval(parse(text=cmd))
        cmd = paste("df_var = data$row_",var_l,sep="")
        print(cmd)
        eval(parse(text = cmd))
        #df_var = log2(df_var)
        #hist(log2(df_var))
        hist_sd = sd(df_var)
        #log_hist_sd = sd(log2(df_var))
        #log_hist_sd
        print(hist_sd)
        hist(df_var,breaks = 100)
        cmd = paste("lines(c(",mean(df_var),",",mean(df_var),"),c(0,500),col='red')",sep="")
        eval(parse(text=cmd))
        cmd = paste("lines(c(",mad(df_var),",",mad(df_var),"),c(0,500),col='green')",sep="")
        eval(parse(text=cmd))
        cmd = paste("lines(c(",median(df_var),",",mad(df_var),"),c(0,500),col='orange')",sep="")
        eval(parse(text=cmd))
        for(j in 1:3){
          cmd = paste("lines(c(",mean(df_var)," + ",hist_sd,"*",j,",",mean(df_var)," + ",hist_sd,"*",j,"),c(0,500),col='blue')",sep="")
          eval(parse(text=cmd))
          cmd = paste("lines(c(",mean(df_var)," - ",hist_sd,"*",j,",",mean(df_var)," - ",hist_sd,"*",j,"),c(0,500),col='blue')",sep="")
          eval(parse(text=cmd))
        }
        cmd = paste("df_var_3 = df_var[df_var < ",hist_sd*3,"]",spe='')
        print(cmd)
        eval(parse(text=cmd))
        hist_sd_2 = sd(df_var_3)
        hist(df_var_3,breaks= 100)
        cmd = paste("lines(c(",mean(df_var_3),",",mean(df_var_3),"),c(0,500),col='red')",sep="")
        eval(parse(text=cmd))
        cmd = paste("lines(c(",mad(df_var_3),",",mad(df_var_3),"),c(0,500),col='green')",sep="")
        eval(parse(text=cmd))
        cmd = paste("lines(c(",median(df_var_3),",",mad(df_var_3),"),c(0,500),col='orange')",sep="")
        eval(parse(text=cmd))
        hist(df_var,breaks= 100)
        cmd = paste("lines(c(",mean(df_var_3),",",mean(df_var_3),"),c(0,500),col='red')",sep="")
        eval(parse(text=cmd))
        cmd = paste("lines(c(",mad(df_var_3),",",mad(df_var_3),"),c(0,500),col='green')",sep="")
        eval(parse(text=cmd))
        cmd = paste("lines(c(",median(df_var_3),",",mad(df_var_3),"),c(0,500),col='orange')",sep="")
        eval(parse(text=cmd))
        
        for(j in 1:3){
          cmd = paste("lines(c(",mean(df_var_3)," + ",hist_sd_2,"*",j,",",mean(df_var_3)," + ",hist_sd_2,"*",j,"),c(0,500),col='blue')",sep="")
          eval(parse(text=cmd))
          cmd = paste("lines(c(",mean(df_var)," - ",hist_sd_2,"*",j,",",mean(df_var)," - ",hist_sd_2,"*",j,"),c(0,500),col='blue')",sep="")
          eval(parse(text=cmd))
        }
      eval(parse(text=cmd))
        print(hist_sd_2)
        deviation = 3
        for(deviation in c(2:2)){
          cmd = paste("matplot(t(data[data$row_",var_l," < ",mean(df_var_3)," + ",hist_sd_2," * ",deviation,",c(1:6)]),type='l',main=sample.table.name,sub='",var_l," < ",deviation,"_sd')",sep='')
          print(cmd)
          eval(parse(text=cmd))
          cmd = paste("matplot(t(data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm > 0,c(1:6)]),type='l',main=sample.table.name,sub='",var_l," > ",deviation,"_sd_UP')",sep='')
          print(cmd)
          eval(parse(text=cmd))
          cmd = paste("matplot(t(data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm < 0,c(1:6)]),type='l',main=sample.table.name,sub='",var_l," > ",deviation,"_sd_DOWN')",sep='')
          print(cmd)
          eval(parse(text=cmd))
          cmd = paste("write.table(data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm > 0,], file = 'output/UP_var_",deviation,"sd_",sample.table.name,".txt', row.name = TRUE, col.names = TRUE, sep = '\t')",sep="")
          print(cmd)
          eval(parse(text=cmd))
          cmd = paste("df.up = data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm > 0,]",sep='')
          eval(parse(text=cmd))
          print(rownames(df.up))
          print(table.name)
          if(table.name == 'df.proteinGroups.nc'){
            print('hit1')
            main_table_name = 'df.proteinGroups'
            id_name = 'Protein.IDs'
          }
          if(table.name == 'df.peptides.nc'){
            print('hit2')
            main_table_name = 'df.peptides'
            id_name = 'Sequence'
            print(main_table_name)
            print(id_name)
          }
          #print(main_table_name)
          #print(id_name)
          direction_list = c('up','down')
          par(mfrow = c(3,3))
          if(table.name == 'df.proteinGroups.nc'){ 
            for(direction_name in direction_list){
            cmd = paste("direction_table <- df.",direction_name,sep='')
            eval(parse(text=cmd))
            for(u in c(1:dim(direction_table)[1])){
              #print(u)
              #print(df.up[u,])
              cmd = paste("hit_row = rownames(df.",direction_name,"[u,])",sep='')
              eval(parse(text=cmd))
              #print(hit_row)
              #print(hit_row)
              cmd = paste("p = ",main_table_name,".qn.log2.ALL[hit_row,]",sep='')
              #print(cmd)
              eval(parse(text=cmd))
              #print(p)
              cmd = paste("protein_name = ",main_table_name,"$Protein.names[",main_table_name,"$",id_name," == hit_row]",sep='')
              #print(cmd)
              eval(parse(text = cmd))
              
              #print(peptide_list)
              #print(protein_name)
              plot(as.numeric(p[,c(1:8)]),type='l',ylim=(c(-3,3)),col='red',main=paste(direction_name,'_',sample,' \n ',protein_name),sub=hit_row,lwd=5)
              axis(2, labels = (c(-1:1)),at=c(-1:1))
              axis(1, labels = timepoint.list,at=c(1:8))
              lines(t(p[,c(9:16)]),col='orange',lwd=5)
              lines(t(p[,c(17:24)]),col='pink',lwd=5)
              lines(t(p[,c(25:32)]),col='blue',lwd=5)
              lines(t(p[,c(33:40)]),col='green',lwd=5)
              peptide_list = df.peptides$Sequence[df.peptides$Protein.names == paste(protein_name)]
              for(peptide_name in peptide_list){
                lines(t(df.peptides.qn.log2.ALL[peptide_name,c(1:8)]),col='red')
                lines(t(df.peptides.qn.log2.ALL[peptide_name,c(9:16)]),col='orange')
                lines(t(df.peptides.qn.log2.ALL[peptide_name,c(17:24)]),col='pink')
                lines(t(df.peptides.qn.log2.ALL[peptide_name,c(25:32)]),col='blue')
                lines(t(df.peptides.qn.log2.ALL[peptide_name,c(33:40)]),col='green')
                #eval(parse(text=cmd))
              }
            }
          }
          }
          
          cmd = paste("write.table(data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm < 0,], file = 'output/DOWN_var_",deviation,"sd_",sample.table.name,".txt', row.name = TRUE, col.names = TRUE, sep = '\t')",sep="")
          print(cmd)
          eval(parse(text=cmd))
          cmd = paste("write.table(data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm < 0 | data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm < 0,], file = 'output/UP_DOWN_var_",deviation,"sd_",sample.table.name,".txt', row.name = TRUE, col.names = TRUE, sep = '\t')",sep="")
          print(cmd)
          eval(parse(text=cmd))
          cmd = paste("df.down = data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm < 0,]",sep='')
          eval(parse(text=cmd))
          print(rownames(df.down))
          if(rbind_text == ''){
            rbind_text = paste("data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm < 0,],data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm > 0,]",sep='')
          }else{
          rbind_text = paste(rbind_text,",data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm < 0,],data[data$row_",var_l," > ",mean(df_var_3)," + ",hist_sd_2," * ",deviation," & data$row_lm > 0,]",sep='')
          }
        }
      }
    }
    cmd = paste(table.name,".",cell_type,".rbind = rbind(",rbind_text,")",sep='')
    print(cmd)
    eval(parse(text=cmd))
    cmd = paste("write.table(",table.name,".",cell_type,".rbind, file = 'output/",table.name,".",cell_type,".rbind', row.name = TRUE, col.names = TRUE, sep = '\t')",sep="")
    print(cmd)
    eval(parse(text=cmd))
  }
}

#proteinGroups.Sai.rbind_text

rbind_text


save.image(workspace_filename_location)
#if(data_location != 'local'){
#  save.image(local_workspace_filename_location)
#}