setwd("/run/user/sgarnett/gvfs/sftp:host=blackburn2.uct.ac.za,user=sgarnett/blackburn3/sgarnett/NSC_3/MaxQuant/iTRAQ_January_2013/R/")
getwd()
workspace_location = getwd()

workspace_filename= 'workspace/Cambridge_iTRAQ_January_2013.new.RData'
workspace_filename_location = paste(workspace_location,'/',workspace_filename,sep='')

print(workspace_filename_location)
load(workspace_filename_location)
ls()

for(number in c(1:dim(df.NS.proteinGroups.qn.CM.log2)[1])){
  t_test = t.test(df.NS.proteinGroups.qn.log2[number,c(1:6)],alternative = c('two.sided'),mu=0)
  print(t_test)
  str(t_test)
  plot(t(df.NS.proteinGroups.qn.log2[number,c(1:6)]),type='l',col='blue',sub=t_test$p.value)
  #lines(t(df.NS.proteinGroups.qn.CM.log2[number,c(9:14)]),type='l',col='green')
}