import os
import sys

def is_sequence(arg):
    return (not hasattr(arg, "strip") and
            hasattr(arg, "__getitem__") or
            hasattr(arg, "__iter__"))

def write_variable(identifier,entry,dirname,variable):
	print(dirname)
	print(identifier)
	variable.variable_dic[identifier] = entry
	if is_sequence(entry):
		identifier_line = "%s = c('%s')" %(identifier,"','".join(entry))
		variable.variable_dic['%s_line' %(identifier)] = identifier_line+'\n'
	variable.variable_dic[identifier] = entry
	#raw_input()
	variable_file = open('%s/rscripts/variable.py' %(dirname),'w')
	variable_file.write("variable_dic = %s" %(variable.variable_dic))
	variable_file.close()
	
	for key in variable.variable_dic.keys():
		print '%s : %s' %(key,variable.variable_dic[key])
	print(dirname)
	#raw_input('MQ_function')
	
	
	
def tools(self,R_file_name,add_list = []):
	add_list = add_list+[variable.variable_dic['table_name_line']]
	create_R_file(self.R_dir,R_file_name,add_list)
	Run_rscript(R_file_name,self.R_dir)
	status(R_file_name)
	return

def rstudio(self,R_file_name,add_list = []):
	add_list = add_list+[variable.variable_dic['table_name_line']]
	create_R_file(self.R_dir,R_file_name,add_list)
	Run_rstudio(R_file_name,self.R_dir)
	status(R_file_name)
	return


def sweave(self,R_file_name):
	add_list = [variable.variable_dic['table_name_line']]
	#add_line = ['table_name = "%s"' %(table_name)]
	add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
	Simple_Run_Sweave_file(self.R_dir,R_file_name,add_list,add_file_name)
	status(R_file_name)
	return

def knitr(self,R_file_name,add_list = []):
	add_list = add_list+[variable.variable_dic['table_name_line'],variable.variable_dic['set_number_line']]
	if variable.variable_dic['edit'] == 'yes':
		create_R_file(self.R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,self.R_dir)
	else:
		#add_line = ['table_name = "%s"' %(table_name)]
		add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
		Run_knitr(self.R_dir,R_file_name,add_list,add_file_name)
		status(R_file_name)
	return

def knitr_selection(self,R_file_name):
	add_list = [variable.variable_dic['table_name_line'],variable.variable_dic['uniprot_list_entry'],variable.variable_dic['set_number_line']]
	if variable.variable_dic['edit'] == 'yes':
		create_R_file(self.R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,self.R_dir)
	else:
		#add_line = ['table_name = "%s"' %(table_name)]
		add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
		Run_knitr(self.R_dir,R_file_name,add_list,add_file_name)
		status(R_file_name)
	return