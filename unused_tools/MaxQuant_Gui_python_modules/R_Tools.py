print('R_Tools')

import Tkinter, tkFileDialog
from Tkinter import *

class R_Tools_App():
	def __init__(self,master,variable):
		
		
		tool_frame = LabelFrame(master,text="Tools",padx=5, pady=5)
		tool_frame.place(y=250,x=50)
		
		view_frame = LabelFrame(master,text="View",padx=5, pady=5)
		view_frame.place(y=320,x=50)
		
		test_frame = LabelFrame(master,text="Test",padx=5, pady=5)
		test_frame.place(y=400,x=50)
		self.R_dir = 'Tools'
		
		self.hi_there = Button(tool_frame, text="Extract_LFQ", command=lambda i='Extract_ALL_Label_Free_Quantiation_Values.R': self.tools(i)).pack(side=LEFT)
		
		self.hi_there = Button(tool_frame, text="Extract_iTRAQ", command=lambda i='Extract_8_plex_iTRAQ.R': self.tools(i)).pack(side=LEFT)
		
		self.hi_there = Button(tool_frame, text="Normalise", command=self.Normalise).pack(side=LEFT)
		self.hi_there = Button(tool_frame, text="Normalise\nMean, Media", command=lambda i='Normalisation_mean.R': self.knitr(i)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Plot", command=self.Plot).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Boxplot", command=self.Boxplot).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Heatmap", command=self.Heatmap).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text='MatPlot', command=self.matplot).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Report", command=lambda i='Report.R': self.sweave(i)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="knitr\nReport", command=lambda i='knitr_Report.R': self.knitr(i)).pack(side=LEFT)
		
		self.hi_there = Button(tool_frame, text="log2", command=self.log2).pack(side=LEFT)
		
		self.hi_there = Button(tool_frame, text="Stat", command=lambda i='stat.R': self.tools(i)).pack(side=LEFT)
		self.hi_there = Button(tool_frame, text="Stat 2", command=lambda i='stat_2.R': self.tools(i)).pack(side=LEFT)
		
		self.hi_there = Button(tool_frame, text="diff", command=lambda i='diff.R': self.tools(i)).pack(side=LEFT)
		
		self.hi_there = Button(tool_frame, text="first ratio", command=lambda i='first_ratio.R': self.tools(i)).pack(side=LEFT)
		
		
		self.hi_there = Button(view_frame, text="FDR", command=lambda i='fdr.R': self.knitr(i)).pack(side=LEFT)
		self.hi_there = Button(view_frame, text="Volcano\nPlot", command=lambda i='volcano.R': self.knitr(i)).pack(side=LEFT)
		
		#self.hi_there = Button(view_frame, text="cmeans", command=lambda i='cmeans.R': self.rstudio(i)).pack(side=LEFT)
		self.hi_there = Button(test_frame, text="cmeans", command=lambda i='cmeans.R': self.knitr(i)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="cmeans\nselection", command=lambda i='cmeans_selection.R': self.knitr_selection(i)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="matplot\nselection", command=lambda i='matplot_selection.R': self.knitr_selection(i)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="matplot\nsectional", command=lambda i='matplot_sectional.R': self.knitr_selection(i)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="matplot\nsectional\nselection", command=lambda i='matplot_sectional_selection.R': self.knitr_selection(i)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="cmeans\nhouskeeping\nnormalise", command=lambda R_file_name='cmeans_housekeeping_normalise.R',add_list = [variable.variable_dic['uniprot_list_entry'],variable.variable_dic['set_number_line']]: self.knitr(R_file_name,add_list)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="matplot\nhouskeeping\nnormalise", command=lambda R_file_name ='matplot_housekeeping.R',add_list = [variable.variable_dic['uniprot_list_entry'],variable.variable_dic['set_number_line']]: self.knitr(R_file_name,add_list)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="Housekeeping Genes", command=lambda i='Load_Housekeeping_Genes.R', add_list = [variable.variable_dic['uniprot_list_line']]: self.tools(i,add_list)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="Open_Rstudio", command=lambda i='new.R': self.rstudio(i)).pack(side=LEFT)
		
		#self.hi_there = Button(view_frame, text="close", command=self.close).pack(side=LEFT)
		
		
		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = RadioButton_List_App(root,variable_list,mylist,1000,300,'no')
		
		self.number = Entry_App(root,'set_number',1100,400)
	
		
	def tools(self,R_file_name,add_list = []):
		add_list = add_list+[variable.variable_dic['table_name_line']]
		create_R_file(self.R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,self.R_dir)
		status(R_file_name)
		return
	
	def rstudio(self,R_file_name,add_list = []):
		add_list = add_list+[variable.variable_dic['table_name_line']]
		create_R_file(self.R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,self.R_dir)
		status(R_file_name)
		return
	
	
	def sweave(self,R_file_name):
		add_list = [variable.variable_dic['table_name_line']]
		#add_line = ['table_name = "%s"' %(table_name)]
		add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
		Simple_Run_Sweave_file(self.R_dir,R_file_name,add_list,add_file_name)
		status(R_file_name)
		return
	
	def knitr(self,R_file_name,add_list = []):
		add_list = add_list+[variable.variable_dic['table_name_line'],variable.variable_dic['set_number_line']]
		if variable.variable_dic['edit'] == 'yes':
			create_R_file(self.R_dir,R_file_name,add_list)
			Run_rstudio(R_file_name,self.R_dir)
		else:
			#add_line = ['table_name = "%s"' %(table_name)]
			add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
			Run_knitr(self.R_dir,R_file_name,add_list,add_file_name)
			status(R_file_name)
		return
	
	def knitr_selection(self,R_file_name):
		add_list = [variable.variable_dic['table_name_line'],variable.variable_dic['uniprot_list_entry'],variable.variable_dic['set_number_line']]
		if variable.variable_dic['edit'] == 'yes':
			create_R_file(self.R_dir,R_file_name,add_list)
			Run_rstudio(R_file_name,self.R_dir)
		else:
			#add_line = ['table_name = "%s"' %(table_name)]
			add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
			Run_knitr(self.R_dir,R_file_name,add_list,add_file_name)
			status(R_file_name)
		return
	def matplot(self):
		R_dir = 'Tools'
		R_file_name = 'matplot.R'
		#table_name([variable.variable_dic['selected_file_list'][0],'edited',variable.variable_dic['selected_quant_list'][0],variable.variable_dic['selected_sample_prefix_list'][0]])
		add_list = [variable.variable_dic['table_name_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		#write_variable('selected_suffix_list',['','.na.z'])
		status(R_file_name)
		return
	
	def log2(self):
		R_dir = 'Tools'
		R_file_name = 'log2.R'
		#table_name([variable.variable_dic['selected_file_list'][0],'edited',variable.variable_dic['selected_quant_list'][0],variable.variable_dic['selected_sample_prefix_list'][0]])
		add_list = [variable.variable_dic['table_name_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		#write_variable('selected_suffix_list',['','.na.z'])
		status(R_file_name)
		return
	
	def Stat(self):
		R_dir = 'Tools'
		R_file_name = 'stat.R'
		#table_name([variable.variable_dic['selected_file_list'][0],'edited',variable.variable_dic['selected_quant_list'][0],variable.variable_dic['selected_sample_prefix_list'][0]])
		add_list = [variable.variable_dic['table_name_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		#write_variable('selected_suffix_list',['','.na.z'])
		status(R_file_name)
		return
	
	def Normalise(self):
		R_dir = 'Tools'
		R_file_name = 'Normalisation.R'
		#table_name([variable.variable_dic['selected_file_list'][0],'edited',variable.variable_dic['selected_quant_list'][0],variable.variable_dic['selected_sample_prefix_list'][0]])
		add_list = [variable.variable_dic['table_name_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		#write_variable('selected_suffix_list',['','.na.z'])
		status(R_file_name)
		return
	
	def Plot(self):
		print "PLOT"
		R_dir = 'Tools'
		R_file_name = 'plot.R'
		#table_name([variable.variable_dic['selected_file_list'][0],'edited',variable.variable_dic['selected_quant_list'][0],variable.variable_dic['selected_sample_prefix_list'][0]])
		add_list = [variable.variable_dic['table_name_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)
		return
		
	def Boxplot(self):
		print "PLOT"
		R_dir = 'Tools'
		R_file_name = 'boxplot.R'
		#table_name([variable.variable_dic['selected_file_list'][0],'edited',variable.variable_dic['selected_quant_list'][0],variable.variable_dic['selected_sample_prefix_list'][0]])
		add_list = [variable.variable_dic['table_name_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)
		return
	
	def Heatmap(self):
		print "PLOT"
		R_dir = 'Tools'
		R_file_name = 'heatmaps.R'
		#table_name([variable.variable_dic['selected_file_list'][0],'edited',variable.variable_dic['selected_quant_list'][0],variable.variable_dic['selected_sample_prefix_list'][0]])
		add_list = [variable.variable_dic['table_name_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)
		return