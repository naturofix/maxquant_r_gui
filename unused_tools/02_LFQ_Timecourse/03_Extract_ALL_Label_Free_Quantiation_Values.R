setwd("/home/sgarnett/Documents/RData/SH-SY5Y/Differentiation/data/140508_3rep")
print(getwd())
print("Loading : /home/sgarnett/Documents/RData/SH-SY5Y/Differentiation/data/140508_3rep/workspace/Test.RData")
load("/home/sgarnett/Documents/RData/SH-SY5Y/Differentiation/data/140508_3rep/workspace/Test.RData")
setwd(working_directory)
selected_file_list = c('proteinGroups.txt.edited')
experiment_name_list = c('Ref_85min','Ref_Default_60min','Ref_slope-02_01','Ref_slope-02_02','Ref_slope-02_03','SH7_R-3_01','SH7_R-3_02','SH7_R-3_03')
selected_quant_list = c('iBAQ')
sample_name_list = experiment_name_list










all_quant_list = c('Intensity','iBAQ','LFQ.intensity')
for(file_name in c(selected_file_list)){
	for(quant in selected_quant_list){
		cat(paste('\n\t**',file_name,'**\n\n'))
		table_name = paste('df.',file_name,sep='')
		print(table_name)
		
		
		cmd = paste('data = ',table_name,sep='')
		print(cmd)
		eval(parse(text=cmd))
		
    quant_column_name_list = colnames(data)[grep(quant,colnames(data))]
		#quant_column_name_list = c()
		#for(sample in sample_name_list){
		#	quant_column_name_list = c(quant_column_name_list,paste(quant,'.',sample,sep=''))
		#}
		cat('\nQuant Columns :\n')
		cat(paste('\t',quant_column_name_list,'\n'))
		
		print(quant_column_name_list)
		for(character in remove_character_list){
			#print(character)
			#print(paste(character))
			quant_column_name_list = gsub(paste(character), ".", quant_column_name_list)
		}
		
		print(quant_column_name_list)

		quant_table_name = paste(table_name,'.',quant,sep='')
		cat(paste('\nCREATE TABLE :',quant_table_name,'\n'))
		cmd = paste(quant_table_name,' = data.frame(',table_name,'[,1])',sep='')
		print(cmd)
		eval(parse(text=cmd))
		for(quant_column in quant_column_name_list){
			cmd = paste(quant_table_name,'$',quant_column,' = ',table_name,'$',quant_column,sep='')
			print(cmd)
			eval(parse(text=cmd))
		}
		
		cmd = paste('rownames(',quant_table_name,') = rownames(',table_name,')',sep='')
		print(cmd)
		eval(parse(text=cmd))
		cmd = paste(quant_table_name,' = ',quant_table_name,'[-1]',sep='')
		print(cmd)
		eval(parse(text=cmd))
    
		}
	}


print(paste('saved ',workspace_filename))
save.image(workspace_filename)
