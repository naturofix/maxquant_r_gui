version_number = '1.7'
version_colour = 'purple'
global version_colour

import os
import sys
import socket
import Tkinter, tkFileDialog
from Tkinter import *
import tkFont
#import rpy2
import time
import re
import subprocess
import platform
import time
from datetime import date

today = date.today()
today_file = str(today).replace('-','')
print today
global today_file

pdf_viewer = 'okular'
file_explorer = 'nautilus'
system_name = platform.system()
if system_name == 'Darwin':
	pdf_viewer = "open -a Skim"
	file_explorer = 'open -a finder'

#test abilty to edit

global python_path
python_path = os.getcwd()



sys.path.insert(1, python_path)
import MQ_functions



file_list = os.listdir(python_path)
status_list = []
global status_list

top = 35
top_min = 40

global top
global top_min

def ReLoad_Functions():
	R_dir = '00_config'
	R_file_name = 'Load_functions.R'
	#add_list = ["source('%s/00_functions/00_01_functions.R')\n" %(python_path)]
	add_list = ["function_location = '%s/rscripts/00_functions/'" %(dirname)]
	Copy_Functions()
	create_R_file(R_dir,R_file_name,add_list,variable.variable_dic['save_list'])
	Run_rscript(R_file_name,R_dir)
	status(R_file_name)

def Load():
	experiment_name()
	maxquant_file_list  = ['proteinGroups.txt','peptides.txt','summary.txt','parameters.txt']
	write_variable('file_list',maxquant_file_list)
	
	#experiment_name()
	
	
	R_dir = '01_Load_Data'
	R_file = '01_Load.R'
	add_list = [variable.variable_dic['file_list_line'],"file_path = '%s'\n" %(dirname)]
	tools(R_dir,R_file,add_list)
	
		
	R_dir = '01_Load_Data'
	R_file = '02_CLEAN_UP_DATA.R'
	tools(R_dir,R_file)
	status('Cleaned up data')
	Extract_Sample_Info()
	status('extract sample info')

def tar():
	print 'tar'
	file_list = os.listdir(dirname)
	if 'tar' not in file_list:
		os.system('mkdir %s/%s' %(dirname,'tar'))
	cmd = "tar -C %s -cf %s/tar/%s.tar images tables output/selected/" %(dirname,dirname,variable.variable_dic['experiment_name'])
	print(cmd)
	os.system(cmd)
	print 'dropbox uploader'
	cmd = '%s/dropbox_uploader.sh upload %s/tar/%s.tar Thesis_Data/%s.tar' %(python_path,dirname,variable.variable_dic['experiment_name'],variable.variable_dic['experiment_name'])
	print cmd
	os.system(cmd)

def load_line():
	print(dirname)
	file_list = os.listdir(dirname)
	hit = 0
	if 'workspace' in file_list:
		workspace_file_list = os.listdir('%s/workspace' %(dirname))
		if len(workspace_file_list) == 1:
			if '.RData' in workspace_file_list[0]:
				workspace_filename = '%s/workspace/%s' %(dirname,workspace_file_list[0])
				print workspace_filename
				hit = 1
			else:
				print 'no workspace file'
		else:
			print workspace_file_list
			raw_input('more than one workspace')
	else:
		print 'no existing workspaces'
		
	if hit == 0:
		workspace_filename = '%s/workspace/workspace.RData' %(dirname)
	#raw_input('enter')
	write_variable('workspace_filename',workspace_filename)
	load_line = 'setwd("%s")\nprint(getwd())\nprint("Loading : %s")\nload("%s")\nworkspace_filename = "%s"\n' %(dirname,workspace_filename,workspace_filename,workspace_filename)
	load_list = [load_line]	
	global load_list
	print load_line
	write_variable('load',load_line)
	#raw_input('enter')
	
	
def Initialise():
	file_list = os.listdir(dirname)
	if 'rscripts' not in file_list:
		os.mkdir('%s/rscripts' %(dirname))
	rscript_file_list = os.listdir('%s/rscripts' %(dirname))
	if 'variable.py' not in rscript_file_list:
		variable_dic = {}
		f = open('%s/rscripts/variable.py' %(dirname),'w')
		f.write('variable_dic = {}')
		f.close()
	sys.path.insert(1, dirname+'/rscripts')
	import variable
	#reload(variable)
	print_variable()
	
	write_variable('base_dir',base_dir)
	global variable
	
	#raw_input('enter 1 ...')
	write_variable('python_path',python_path)
	write_variable('edit','no')
	
	path_list = dirname.split('/')
	print path_list
	path_length = len(path_list)-1
	while path_list[path_length] == 'txt' or path_list[path_length] == 'combined':
		print(path_list[path_length])
		path_length = path_length-1
	print path_list[path_length]
	project_name = '%s_%s_%s' %(path_list[path_length-2],path_list[path_length-1],path_list[path_length])
	#raw_input(project_name)
	
	
	#raw_input()
	#project_name = List_App(root,'project',path_list)
	#raw_input(get(project_name))
	#project_name = 'Test'
	project_name_line = "project_name = '%s'\n" %(project_name)
	write_variable('project_name',project_name)
	#workspace_filename = '%s/workspace/%s.RData' %(dirname,project_name)
	#write_variable('workspace_filename',workspace_filename)
	variable_default('experiment_name','')

	write_variable('table_name','')
	write_variable('quant_list','')
	write_variable('exit_code',0)
	
	preamble()
	
	#load_line = 'setwd("%s")\nprint(getwd())\nprint("Loading : %s")\nload("%s")\n' %(dirname,workspace_filename,workspace_filename)
	#load_list = [load_line]	
	#global load_list
	
	#write_variable('load',load_line)
	load_line()
	
	
	#raw_input('load line enter ...')
	source_list = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname)]
	write_variable('source_list',source_list)
	
	save_list = ["print(working_directory)\n","print(workspace_filename)\n","save.image(workspace_filename)\n"]
	write_variable('save_list',save_list)
	#write_variable('workspace_filename',workspace_filename)
	cmd = 'python %s/01_Load_Data/initialise.py %s %s' %(python_path,python_path,dirname)
	print cmd
	os.system(cmd)
	#raw_input('initialise.py enter ...')
	print_variable()

	sys.path.insert(1, dirname+'/rscripts')
	import variable
	reload(variable)
	print_variable()
	load_list = [variable.variable_dic['load']]
	global load_list
	save_list = ["print(working_directory)\n","print(workspace_filename)\n","save.image(workspace_filename)\n"]
	global save_list
	
	R_dir = '00_functions'
	R_file_name = '00_01_functions.R'
	add_list = ["function_location = '%s/rscripts/00_functions/'" %(dirname)]
	Copy_Functions()
	
	#raw_input('before create workspace enter ....')
	#cmd = 'cp -r %s/01_Load_Data/00_00_create_workspace.R %s/rscripts/' %(python_path,dirname)
	#print(cmd)
	#os.system(cmd)
	R_file_name = '00_00_create_workspace.R'
	cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
	cmd = '%s %s/rscripts/%s' %(rscript_command,dirname,R_file_name)
	print(cmd)
	os.system(cmd)
	status('Create Workspace')
	write_variable('initialise','yes')
	#raw_input('after create workspace enter ...')

def preamble_edit():
	preamble_file_name = os.path.join(python_path,'preamble.txt')

	Open_File(preamble_file_name)
		
	read_file = open('%s' %(preamble_file_name))
	cmd = 'kate %s' %(preamble_file_name)
	print(cmd)
	os.system(cmd)
	preamble()

def preamble():
	preamble_file_name = os.path.join(python_path,'preamble.txt')
	read_file = open(preamble_file_name,'r')
	read_list = read_file.readlines()
	read_file.close()
	print(read_list)
	read_list.append('\nwd = "%s"\n' %(dirname))
	read_list.append('\nworking_directory = "%s"\n' %(dirname))
	read_line = ''.join(read_list)
	write_variable('preamble',read_list)
	write_variable('preamble_list_line',read_line)
	
def experiment_name():
	experiment_tk = Tkinter.Tk()
	experiment_tk.geometry("360x150+100+100")
	experiment_tk.title('Set Experiment Name')
	
	Entry_App(experiment_tk,'experiment_name',20,20,'no',30)
	def close():
		experiment_tk.destroy()
	close_button = Button(experiment_tk, bg='red',text="close", command=close)
	close_button.place(x=150,y=110)
	
def design():
	experiment_tk = Tkinter.Tk()
	experiment_tk.geometry("360x150+100+100")
	experiment_tk.title('Set ')
	
	Entry_App(experiment_tk,'design',20,20,'no',30)
	def close():
		experiment_tk.destroy()
	close_button = Button(experiment_tk, bg='red',text="close", command=close)
	close_button.place(x=150,y=110)
	
	design_list = variable.variable_dic['design'].split(',')
	#new_design_list = []
	#for entry in design_list:
	#	new_design_list.append(int(entry))
	#print new_design_list
	write_variable('design_list',design_list)
	


def Copy_Functions():
	cmd = 'mkdir %s/rscripts/00_functions' %(dirname)
	print(cmd)
	os.system(cmd)
	cmd = 'cp -r %s/00_functions %s/rscripts/' %(python_path,dirname)
	print(cmd)
	os.system(cmd)
	#create_R_file(R_dir,R_file_name,add_list)

def Get_File_name_path(dirname):
	new_file_tk = Tkinter.Tk()
	new_file_name = tkFileDialog.askopenfilename(parent=new_file_tk,initialdir=dirname,title='Please select a File')
	new_file_tk.destroy()
	return(new_file_name)
	
def Get_Path(dirname):
	new_file_tk = Tkinter.Tk()
	new_file_name = tkFileDialog.askdirectory(parent=new_file_tk,initialdir=dirname,title='Please select a File')
	new_file_tk.destroy()
	return(new_file_name)

def Open_File(open_file_path):
	os.system('kate %s' %(open_file_path))

def common_prefix(strings):
    if len(strings) == 1:#rule out trivial case
        return strings[0]

    prefix = strings[0]

    for string in strings[1:]:
        while string[:len(prefix)] != prefix and prefix:
            prefix = prefix[:len(prefix)-1]
        if not prefix:
            break

    return prefix

def is_sequence(arg):
    return (not hasattr(arg, "strip") and
            hasattr(arg, "__getitem__") or
            hasattr(arg, "__iter__"))

def write_variable(identifier,entry):
	variable.variable_dic[identifier] = entry
	#print '%s : %s\n' %(identifier,entry)
	if is_sequence(entry):
		identifier_line = "%s = c('%s')" %(identifier,"','".join(entry))
		identifier_line_int = "%s = c(%s)" %(identifier,",".join(entry))
		#print '%s_line: %s '%(identifier,identifier_line) 
		#print '%s_line_int: %s '%(identifier,identifier_line_int) 
		variable.variable_dic['%s_line' %(identifier)] = identifier_line+'\n'
		variable.variable_dic['%s_line_int' %(identifier)] = identifier_line_int+'\n'
		if len(entry) == 1:
			#print '%s_entry: %s_entry = "%s"\n'%(identifier,identifier_line,entry[0])
			variable.variable_dic['%s_entry' %(identifier)] = '%s_entry = "%s"\n' %(identifier,entry[0])
	else:
		if "_line" not in identifier:
			#print '%s_line : %s = %s\n' %(identifier,identifier,entry)
			variable.variable_dic[identifier+'_line'] = '%s = %s\n' %(identifier,entry)
			#print '%s_line_str : %s = "%s"\n' %(identifier,identifier,entry)
			variable.variable_dic[identifier+'_line_str'] = '%s = "%s"\n' %(identifier,entry)
	#for key in variable.variable_dic.keys():
	#	print('%s : %s' %(key,variable.variable_dic[key]))
	#raw_input()
	variable_file = open('%s/rscripts/variable.py' %(dirname),'w')
	variable_file.write("variable_dic = %s" %(variable.variable_dic))
	variable_file.close()

def variable_default(entry_name,default_value):
	if entry_name not in variable.variable_dic.keys():
		write_variable(entry_name,default_value)

def add_variable_list(key,new_entry):
	try:
		old_list = variable.variable_dic[key]
	except:
		old_list = []
	if new_entry not in old_list:
		old_list.append(new_entry)
		write_variable(key,old_list)
	
def print_variable():
	print '\n\n\n\n\n'
	print dirname
	f = open('%s/rscripts/variable.py' %(dirname))
	read_list = f.readlines()
	f.close()
	for read_line in read_list:
		print read_line
	print '\n\n'
	key_list = variable.variable_dic.keys()
	key_list.sort()
	for key in key_list:
		print '%s : %s' %(key,variable.variable_dic[key])
	#raw_input()
	
	

def status(entry,new_experimental_design = 'no',new_load = 'no'):
	if entry+'\n' not in status_list:
		status_list.append(entry+'\n')
	status_line = '\n'.join(status_list)
	if new_experimental_design == 'yes':
		exp = StringVar()
		Label(status_frame, textvariable=exp).pack()
		Extract_Conditions()
		exp.set('\ncondition_1\t:\t%s\ncondition_2\t:\t%s' %('\t'.join(condition_1),'\t'.join(condition_2)))
		new_experimental_design = 'no'
		
	#v.set(status_line)
	complete_line = '%s complete\t%s' %(entry,variable.variable_dic['exit_code'])
	write_variable('exit_code','')
	print complete_line
	new_status(complete_line)
	return
	#running.set(complete_line)

def new_status(status_entry = ''):
	if status_entry == '':
		status_entry = variable.variable_dic['status_entry']
	else:
		write_variable('status_entry',status_entry)
	#print(status_entry)
	new_status_line.set(status_entry)
	root.update_idletasks()
	
	
def Experimental_Design():
	ExpDesign_TK = Tkinter.Tk()
	#app = App(root)
	#ExpDesign_TK.geometry("800*800+100+100")
	#def Generate_Experimental_Design():
	file_list = os.listdir('%s/rscripts' %(dirname))
	print file_list
	if "Experimental_design.txt" not in file_list:
		#if 'summary.txt' in file_list:
			#summary_file = open('%s/summary.txt' %(dirname), 'r')
			#summary_list = summary_file.readlines()
			#summary_file.close()
			#experimental_design_list = ['Sample\tCondition(1 or 2)\n']
			#for summary_line in summary_list:
			##print summary_line
				#summary_line_list = summary_line.split('\t')
				#print summary_line_list[0:3]
				#if summary_line_list[1] == '' and summary_line_list[0] != 'Total':
					#experimental_design_list.append(summary_line_list[0]+'\t0\n')
		#else:
		colname_file = open('%s/rscripts/column_name_%s' %(dirname,variable.variable_dic['quant_table_name']))
		column_list = colname_file.readlines()
		colname_file.close()
		print column_list
		column_list = [x.replace('"','') for x in column_list]
		column_list = [x.replace('\n','') for x in column_list]
		quant = variable.variable_dic['selected_quant_list'][0]+'.'
		print quant
		column_list = [x.replace(quant,'') for x in column_list]
		print(column_list)
		#raw_input()
		experimental_design_list = ['Sample\tCondition(1 or 2)\tClass\n']
		for column_name in column_list:
			experimental_design_list.append(column_name+'\t0\n')
			#raw_input()
		print experimental_design_list
		#raw_input()
		f = open('%s/rscripts/Experimental_design.txt' %(dirname),'w')
		f.writelines(experimental_design_list)
		#read_lines = f.readlines()
		f.close()
		return(experimental_design_list)
	
	def openExp():
		f = open('%s/rscripts/Experimental_design.txt' %(dirname),'rw')
		read_lines = f.readlines()
		f.close()
		ExpDesign.delete(1.0,END)
		ExpDesignString = ''
		for i in read_lines:
			ExpDesignString +=  i
		ExpDesign.insert(END,ExpDesignString)
		return

	def openFiles(selection):
		if selection == 'Experimental Design':
			openExp()
		else:
			print 'no a valid option'
		return

	def SaveExpDesign():
		ExpDesignUpdate = ExpDesign.get(1.0,END)
		outToFile = open('%s/rscripts/Experimental_design.txt' %(dirname),'w')
		outToFile.write(ExpDesignUpdate)
		outToFile.close()
		return
	def Refresh():
		cmd = 'rm %s/rscripts/Experimental_design.txt' %(dirname)
		print(cmd)
		os.system(cmd)
		#Generate_Experimental_Design()
		
	
	def ExpClose():
		ExpDesign.forget()
		save_button.forget()
		close_button.forget()
		ExpDesign_TK.destroy()
		Extract_Conditions()
		new_experimental_design = 'yes'
		status("Experimental Design",new_experimental_design)

	menubar = Menu(ExpDesign_TK)
	filemenu = Menu(menubar,tearoff=0)
	filemenu.add_command(label="Open Experiment Design", command=openExp)

	filemenu.add_separator()
	filemenu.add_command(label="Quit",command=ExpDesign_TK.quit)
	menubar.add_cascade(label='File',menu=filemenu)

	ExpDesign = Text(ExpDesign_TK)
	ExpDesign.insert(END, "Select either condition 1 or 2")
	ExpDesign.pack()
	openExp()
	#Generate_Experimental_Design()
	save_button = Button(ExpDesign_TK,text='Save Experimental Design',command=SaveExpDesign)
	save_button.pack(side='top')
	close_button = Button(ExpDesign_TK,text='close',command=ExpClose)
	close_button.pack(side='top')
	refresh = Button(ExpDesign_TK,text='refresh',command=Refresh)
	refresh.pack(side='top')


def tools(R_dir,R_file_name,add_list = []):
	new_status('running %s' %(R_file_name))
	if R_file_name != '01_Load.R':
		add_list = add_list+[variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
	section_footer = ['\n','list_all = ls()\n',"df_list = grep('df.',list_all)\n","file_name = paste(working_directory,'/rscripts/dataframe_list.txt',sep='')\n","print(file_name)\n","write.table(list_all[df_list],file=file_name,row.names=FALSE,col.names=FALSE)\n",'\n','\n']
	create_R_file(R_dir,R_file_name,add_list,section_footer)
	if variable.variable_dic['edit_list_entry'] == 'rstudio':
		Run_rstudio(R_file_name,R_dir)
	else:
		Run_rscript(R_file_name,R_dir)
	status(R_file_name)
	#new_status('ready')
	return

def tools_list(R_dir,R_file_name,key_list = []):
	new_status('running %s' %(R_file_name))
	add_list = []
	for key in key_list:
		#print(key)
		add_list.append(variable.variable_dic[key])
	#add_list = add_list+[variable.variable_dic['table_name_line'],"data=''"]
	#print(add_list)
	time.sleep(1)
	add_list = add_list+[variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
	section_footer = ['\n','list_all = ls()\n',"df_list = grep('df.',list_all)\n","file_name = paste(working_directory,'/rscripts/dataframe_list.txt',sep='')\n","#print(file_name)\n","write.table(list_all[df_list],file=file_name,row.names=FALSE,col.names=FALSE)\n",'\n','\n']
	#create_R_file(R_dir,R_file_name,add_list,section_footer)
	if variable.variable_dic['edit_list_entry'] == 'rstudio':
		create_R_file(R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,R_dir)
	else:
		create_R_file(R_dir,R_file_name,add_list,section_footer)
		Run_rscript(R_file_name,R_dir)
	status(R_file_name)
	#new_status('ready')
	return
	
	
def rstudio(R_dir,R_file_name,add_list = []):
	add_list = add_list+[variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
	create_R_file(R_dir,R_file_name,add_list)
	
	status(R_file_name)
	return


def sweave(R_dir,R_file_name,add_list = []):
	add_list = [variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
	#add_line = ['table_name = "%s"' %(table_name)]
	add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
	Simple_Run_Sweave_file(R_dir,R_file_name,add_list,add_file_name)
	status(R_file_name)
	return

def knitr_list(R_dir,R_file_name,key_list = ['table_name_line','data_line_str']):
	new_status('running %s' %(R_file_name))
	add_list = []
	for key in key_list:
		#print(key)
		add_list.append(variable.variable_dic[key])
	#add_list = add_list+[variable.variable_dic['table_name_line'],"data=''"]
	add_list = add_list+[variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
	#print(add_list)
	time.sleep(1)
	#raw_input()
	if variable.variable_dic['edit_list_entry'] == 'rstudio':
		create_R_file(R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,R_dir)
	else:
		add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
		#add_line = ['table_name = "%s"' %(table_name)]
		if variable.variable_dic['edit_list_entry'] == 'pdf':
			print 'view'
			pdf_file_name = R_file_name.replace('.R','%s.pdf' %(add_file_name))
			cmd = '%s %s/rscripts/%s' %(pdf_viewer,dirname,pdf_file_name)
			print(cmd)
			subprocess.Popen(cmd,shell=True)
			print('\n%s/rscripts/%s\n' %(dirname,pdf_file_name))
		elif variable.variable_dic['edit_list_entry'] == 'html':
			print 'view'
			html_file_name = R_file_name.replace('.R','%s.html' %(add_file_name))
			cmd = 'firefox %s/rscripts/%s' %(dirname,html_file_name)
			print(cmd)
			subprocess.Popen(cmd,shell=True)
			print('\n%s/rscripts/%s\n' %(dirname,html_file_name))
		else:
			
			Run_knitr(R_dir,R_file_name,add_list,add_file_name)
			status(R_file_name)
	new_status('ready')
	return



def knitr(R_dir,R_file_name,add_list = []):
	new_status('running %s' %(R_file_name))
	#if add_list == []:
	add_list = [variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
	#add_list = add_list+["data=''"]
	#print(add_list)
	time.sleep(1)
	#raw_input()
	if variable.variable_dic['edit_list_entry'] == 'rstudio':
		create_R_file(R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,R_dir)
	else:
		add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
		#add_line = ['table_name = "%s"' %(table_name)]
		if variable.variable_dic['edit_list_entry'] == 'pdf':
			print 'view'
			pdf_file_name = R_file_name.replace('.R','%s.pdf' %(add_file_name))
			cmd = '%s %s/rscripts/%s' %(pdf_viewer,dirname,pdf_file_name)
			print(cmd)
			subprocess.Popen(cmd,shell=True)
			print('\n%s/rscripts/%s\n' %(dirname,pdf_file_name))
		elif variable.variable_dic['edit_list_entry'] == 'html':
			print 'view'
			html_file_name = R_file_name.replace('.R','%s.html' %(add_file_name))
			cmd = 'firefox %s/rscripts/%s' %(dirname,html_file_name)
			print(cmd)
			subprocess.Popen(cmd,shell=True)
			print('\n%s/rscripts/%s\n' %(dirname,html_file_name))
		#if variable.variable_dic['edit_list_entry'] == 'view':
			#print 'view'
			#pdf_file_name = R_file_name.replace('.R','%s.pdf' %(add_file_name))
			#os.system('okular %s/rscripts/%s' %(dirname,pdf_file_name))
			#print('\n%s/rscripts/%s\n' %(dirname,pdf_file_name))
			
		else:
			
			Run_knitr(R_dir,R_file_name,add_list,add_file_name)
			status(R_file_name)
	return
	
def view(R_dir,R_file_name,add_list = []):
	add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
	pdf_file_name = R_file_name.replace('.R','%s.pdf' %(add_file_name))
	os.system('%s %s/rscripts/%s' %(pdf_viewer,dirname,pdf_file_name))
	status(R_file_name)
	return

def knitr_selection(R_file_name,add_list = []):
	add_list = add_list + [variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
	#raw_input(add_list)
	if variable.variable_dic['edit'] == 'yes':
		create_R_file(R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,R_dir)
	else:
		#add_line = ['table_name = "%s"' %(table_name)]
		add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
		Run_knitr(self.R_dir,R_file_name,add_list,add_file_name)
		status(R_file_name)
	return

def Select_Directory(path):
	global running
	root = Tkinter.Tk()
	dirname = tkFileDialog.askdirectory(parent=root,initialdir=path,title='Please select a MaxQuant "txt" directory')
	root.destroy()
	file_list = os.listdir(dirname)
	print(file_list)
	
	if 'rscripts' not in file_list:
		os.mkdir('%s/rscripts' %(dirname))
	return dirname





def R_characters(edit_list):
	R_character_list = ['-',' ','(',')']
	new_list = []
	for edit_entry in edit_list:
		for R_character in R_character_list:
			edit_entry = edit_entry.replace(R_character,'.')
		new_list.append(edit_entry)
	return(new_list)




#write_variable('sample_prefix_list',[''])





	
def move_file(R_dir,R_file_name):
	read_file = open('%s/%s/%s' %(python_path,R_dir,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	#load_list = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n']
	write_list = [variable.variable_dic['load']]+read_list[2:]
	write_file = open('%s/rscripts/%s' %(dirname,R_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()
	
def create_R_file(R_dir,R_file_name,add_list = [],end_list = []):
	read_file = open('%s/%s/%s' %(python_path,R_dir,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	#load_list = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n']+add_list+['\n','\n']
	write_list = [variable.variable_dic['load']]+["setwd('%s')\n" %(dirname)]+add_list
	#print(write_list)
	#print(len(write_list))
	write_list = write_list+['\n']*(top-len(write_list))+read_list[top_min:]+['\n']*2+end_list
	#for line in write_list:
		#print line
	write_file = open('%s/rscripts/%s' %(dirname,R_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()
	
def create_Run_Sweave_file(R_dir,R_file_name,add_list = []):
	read_file = open('%s/%s/%s' %(python_path,R_dir,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	header = ["\documentclass{article}\n","\\begin{document}\n","Load MaxQuant data into R\n","<<>>=\n"]
	setwd=["setwd('%s/rscripts')\n" %(dirname)]
	section_footer = ["setwd('%s/rscripts/')\n" %(dirname),'@\n']
	#load_list = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n',"setwd('%s/rscripts/')\n" %(dirname)]+add_list+['\n','\n']
	
	#load_list = [variable.variable_dic['load']]
	Rnw_file_name = R_file_name.replace('.R','.Rnw')
	tex_file_name = R_file_name.replace('.R','.tex')
	pdf_file_name = R_file_name.replace('.R','.pdf')
	write_list = header+[variable.variable_dic['load']]+setwd+add_list+['\n','\n']+section_footer
	for file_name in variable.variable_dic['selected_file_list']:
		for quant in variable.variable_dic['selected_quant_list']:
			for suffix in variable.variable_dic['selected_suffix_list']:
				
				file_header = [file_name+'\n',"<<figure_%s_%s, fig=TRUE>>=\n" %(file_name.replace('.','_'),suffix.replace('.','_')),'file_name = "%s"\n' %(file_name),"suffix = '%s'\n" %(suffix),"quant = '%s'\n" %(quant)]
							
				write_list = write_list+file_header+read_list[18:]+section_footer
	#print(write_list)
	document_footer = ["\\end{document}\n"]
	write_list = write_list+document_footer
	write_file = open('%s/rscriptsdata_lin/%s' %(dirname,Rnw_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()
	
	#cmd = 'rstudio %s/rscripts/%s' %(dirname,Rnw_file_name)
	#print(cmd)
	#os.system(cmd)
	
	os.system('cd %s/rscripts/' %(dirname))
	os.chdir('%s/rscripts/' %(dirname))
	cmd = "R CMD Sweave %s/rscripts/%s" %(dirname,Rnw_file_name)
	#pdflatex my_sweave_file.tex
	print(cmd)
	os.system(cmd)
	cmd = 'pdflatex %s/rscripts/%s' %(dirname,tex_file_name)
	print(cmd)
	os.system(cmd)
	
	cmd = 'cp %s/rscripts/%s %s/images/%s' %(dirname,pdf_file_name,dirname,pdf_file_name)
	print(cmd)
	os.system(cmd)
	
	cmd = '%s %s/rscripts/%s' %(pdf_viewer,dirname,pdf_file_name)
	print(cmd)
	os.system(cmd)
	status('summary')
	
def Simple_Run_Sweave_file(R_dir,R_file_name,add_list = [],new_file_name = ''):
	read_file = open('%s/%s/%s' %(python_path,R_dir,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	header = ["\documentclass[a4paper,margin=1in]{article}\n","\usepackage[section] {placeins}\n","\\begin{document}\n","Load MaxQuant data into R\n","<<>>=\n"]
	setwd=["setwd('%s/rscripts')\n" %(dirname)]
	section_footer = ['\n@\n']
	#variable.variable_dic['load'] = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n',"setwd('%s/rscripts/')\n" %(dirname)]+add_list+['\n','\n']
	
	#variable.variable_dic['load'] = [variable.variable_dic['load']]
	Rnw_file_name = R_file_name.replace('.R','%s.Rnw' %(new_file_name))
	tex_file_name = R_file_name.replace('.R','%s.tex' %(new_file_name))
	pdf_file_name = R_file_name.replace('.R','%s.pdf' %(new_file_name))
	write_list = header+[variable.variable_dic['load']]+setwd+add_list+['\n','\n']+section_footer
	
	file_header = ["<<figure_%s, fig=TRUE>>=\n" %(R_file_name.replace('.','_'))]
				
	write_list = write_list+read_list[18:]
	print(write_list)
	document_footer = ["\\end{document}\n"]
	write_list = write_list+document_footer
	write_file = open('%s/rscripts/%s' %(dirname,Rnw_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()
	
	#cmd = 'rstudio %s/rscripts/%s' %(dirname,Rnw_file_name)
	#print(cmd)
	#os.system(cmd)
	
	os.system('cd %s/rscripts/' %(dirname))
	os.chdir('%s/rscripts/' %(dirname))
	if variable.variable_dic['edit'] == 'yes':
		cmd = "rstudio %s/rscripts/%s" %(dirname,Rnw_file_name)
		#pdflatex my_sweave_file.tex
		print(cmd)
		os.system(cmd)
	else:

		cmd = "R CMD Sweave %s/rscripts/%s" %(dirname,Rnw_file_name)
		#pdflatex my_sweave_file.tex
		print(cmd)
		os.system(cmd)
		cmd = 'pdflatex %s/rscripts/%s' %(dirname,tex_file_name)
		print(cmd)
		os.system(cmd)
		
		cmd = '%s %s/rscripts/%s' %(pdf_viewer,dirname,pdf_file_name)
		print(cmd)
		os.system(cmd)
	status('summary')
	
def Run_knitr(R_dir,R_file_name,add_list = [],new_file_name = ''):
	new_status('running %s' %(R_file_name))
	#print(add_list)
	read_file = open('%s/%s/%s' %(python_path,R_dir,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	#header = ["```{r code, echo=FALSE,dpi=300}\nopts_chunk$set(dev = 'pdf')\n"]
	header = ["```{r code, echo=FALSE}\nopts_chunk$set(dev = 'pdf')\n"]
	#header = ["```{r code, echo=FALSE,dpi=200}\n"]
	setwd=["setwd('%s/rscripts')\n" %(dirname)]
	section_footer = ['\n','list_all = ls()\n',"df_list = grep('df.',list_all)\n","file_name = paste(working_directory,'/rscripts/dataframe_list.txt',sep='')\n","print(file_name)\n","write.table(list_all[df_list],file=file_name,row.names=FALSE,col.names=FALSE)\n",'\n','\n']
	#variable.variable_dic['load'] = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n',"setwd('%s/rscripts/')\n" %(dirname)]+add_list+['\n','\n']
	
	#variable.variable_dic['load'] = [variable.variable_dic['load']]
	Rmd_file_name = R_file_name.replace('.R','%s.Rmd' %(new_file_name))
	md_file_name = R_file_name.replace('.R','%s.md' %(new_file_name))
	html_file_name = R_file_name.replace('.R','%s.html' %(new_file_name))
	pdf_file_name = R_file_name.replace('.R','%s.pdf' %(new_file_name))
	images_dir = '%s/images/' %(dirname)
	html_path = images_dir+html_file_name
	pdf_path = images_dir+pdf_file_name
	knitr_file_name = 'kinitr_%s' %(R_file_name)
	write_list = header+[variable.variable_dic['load']]+setwd+add_list+['\n','\n']
	#for line in write_list:
	#	print line
	if 'list_all = ls()\n' not in read_list:
		write_list = write_list+['\n']*(top-len(write_list))+read_list[top_min:]+section_footer
	else:
		write_list = write_list+['\n']*(top-len(write_list))+read_list[top_min:]
	
				
	#write_list = write_list+read_list[18:]
	#print(write_list)
	#document_footer = ["\\end{document}\n"]
	#write_list = write_list+document_footer
	write_file = open('%s/rscripts/%s' %(dirname,Rmd_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()
	
	md_file_line = """
setwd("%s/rscripts/")

# Load packages
require(knitr)
require(markdown)

# Create .md, .html, and .pdf files
knit("%s")
markdownToHTML('%s', '%s', options=c("use_xhml"))
system("pandoc -V geometry:paperwidth=10in -V geometry:paperheight=10in --variable fontsize=6 -s %s -o %s")
system("%s %s",wait=FALSE)
""" %(dirname,Rmd_file_name,md_file_name,html_file_name,html_file_name,pdf_file_name,pdf_viewer,pdf_file_name)

#system("pandoc -V geometry:paperwidth=10in -V geometry:paperheight=10in --variable fontsize=6 -s %s -o %s")
#system("okular %s",wait=FALSE) %(html_file_name,pdf_file_name,pdf_file_name))
	knitr_write_file = open('%s/rscripts/%s' %(dirname,knitr_file_name),'w')
	knitr_write_file.writelines(md_file_line)
	knitr_write_file.close()
	
	Run_rscript(knitr_file_name,dirname)
	print('\n%s/rscripts/%s\n' %(dirname,pdf_file_name))
	#cmd = 'Rscript %s/rscripts/%s' %(dirname,knitr_file_name)
	#print(cmd)
	#os.system(cmd)
	

	
def Run_Latex(R_file_name,add_list = []):
	read_file = open('%s/rscripts/%s' %(dirname,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	header = ["\documentclass{article}\n","\usepackage[section] {placeins}\n","\\begin{document}\n","Load MaxQuant data into R\n","<<>>=\n"]
	setwd=["setwd('%s/rscripts')\n" %(dirname)]
	section_footer = ['\n@\n']
	#variable.variable_dic['load'] = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n',"setwd('%s/rscripts/')\n" %(dirname)]+add_list+['\n','\n']
	
	#variable.variable_dic['load'] = [variable.variable_dic['load']]
	Rnw_file_name = R_file_name.replace('.R','.Rnw')
	tex_file_name = R_file_name.replace('.R','.tex')
	pdf_file_name = R_file_name.replace('.R','.pdf')
	write_list = header+variable.variable_dic['load']+setwd+add_list+['\n','\n']+section_footer
	
	file_header = ["<<figure_%s, fig=TRUE>>=\n" %(R_file_name.replace('.','_'))]
				
	write_list = write_list+read_list
	#print(write_list)
	document_footer = ["\\end{document}\n"]
	write_list = write_list+document_footer
	write_file = open('%s/rscripts/%s' %(dirname,Rnw_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()
	
	#cmd = 'rstudio %s/rscripts/%s' %(dirname,Rnw_file_name)
	#print(cmd)
	#os.system(cmd)
	
	os.system('cd %s/rscripts/' %(dirname))
	os.chdir('%s/rscripts/' %(dirname))
	cmd = "rstudio %s/rscripts/%s" %(dirname,Rnw_file_name)
	#pdflatex my_sweave_file.tex
	#raw_input()
	print(cmd)
	#os.system(cmd)

	cmd = "R CMD Sweave %s/rscripts/%s" %(dirname,Rnw_file_name)
	#pdflatex my_sweave_file.tex
	print(cmd)
	os.system(cmd)
	cmd = 'pdflatex %s/rscripts/%s' %(dirname,tex_file_name)
	print(cmd)
	os.system(cmd)
	
	cmd = '%s %s/rscripts/%s' %(pdf_viewer,dirname,pdf_file_name)
	print(cmd)
	os.system(cmd)
	status('summary')
	
	
#def Edit_R_script(R_dir,R_file_name,add_list= []):
	#file_list = os.listdir('%s/%s' %(python_path,R_dir))
	#if 'edit_%s' %(R_file_name) in file_list:
		#read_file = open('%s/%s/edit_%s' %(python_path,R_dir,R_file_name),'r')
		#read_list = read_file.readlines()
		#read_file.close()
		##variable.variable_dic['load'] = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n']+add_list+['\n','\n']
		#write_list = variable.variable_dic['load']+add_list+['\n','\n']+read_list[18:]
		#write_file = open('%s/%s/edit_%s' %(python_path,R_dir,R_file_name),'w')
		#write_file.writelines(write_list)
		##write_file.close()
		
		#cmd = 'rstudio %s/%s/edit_%s' %(python_path,R_dir,R_file_name)
		#print(cmd)
		#os.system(cmd)
		#complete = raw_input('is edit complete :')
		#if complete == 'yes':
			#cmd = 'cp %s/%s/edit_%s %s/rscripts/%s' %(python_path,R_dir,R_file_name,dirname,R_file_name)
			#print(cmd)
			#os.system(cmd)
			#Run_rscript(R_file_name,R_dir)
		
		
	#else:
		#cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
		#print(cmd)
		#os.system(cmd)
		#cmd = 'cp %s/rscripts/%s %s/%s/edit_%s' %(dirname,R_file_name,python_path,R_dir,R_file_name)
		#print(cmd)
		#os.system(cmd)
	
def Run_rscript(R_file_name,R_dir = python_path):
	new_status('running %s' %(R_file_name))
	if variable.variable_dic['edit'] == 'yes':
		cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
		print(cmd)
		os.system(cmd)
		copy_cmd = 'cp %s/rscripts/%s %s/%s/%s' %(dirname,R_file_name,python_path,R_dir,R_file_name)
		#print copy_cmd
		write_variable('copy_cmd',copy_cmd)
		#copy_to_source = raw_input('copy to %s (y/n) : ' %R_dir)
		#if copy_to_source == 'y':
		#	os.system(copy_cmd)
	else:
			
		cmd = '%s %s/rscripts/%s' %(rscript_command,dirname,R_file_name)
		print(cmd)
		exit_code = 0
		exit_code = os.system(cmd)
		#subprocess.Popen(cmd,shell=True)
		print exit_code
		if int(exit_code) != 0:
			print(exit_code)
		write_variable('exit_code',exit_code)
		
			#raw_input('enter to continue')

def Run_rstudio(R_file_name,R_dir = python_path):
	new_status('running rstudio %s' %(R_file_name))
	cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
	print(cmd)
	os.system(cmd)
	copy_cmd = 'cp %s/rscripts/%s %s/%s/%s' %(dirname,R_file_name,python_path,R_dir,R_file_name)
	print copy_cmd
	write_variable('copy_cmd',copy_cmd)
	os.system(copy_cmd)



def Extract_Conditions():
	f = open('%s/rscripts/Experimental_design.txt' %(dirname),'r')
	read_list = f.readlines()
	#read_lines = f.readlines()
	f.close()
	print read_list
	condition_1 = []
	condition_2 = []
	class_list = []
	for read_line in read_list[1:]:
		print read_line
		read_line_list = read_line.replace('\n','').split('\t')
		print read_line_list
		R_line_list = R_characters(read_line_list)
		print(R_line_list)
		#print R_line_list[1]
		if len(R_line_list) > 1:
			if R_line_list[1] == '1':
				condition_1.append(R_line_list[0])
			if R_line_list[1] == '2':
				condition_2.append(R_line_list[0])
		if len(R_line_list) > 2:
		  class_list.append(R_line_list[2])
		  
	write_variable('condition_1',condition_1)
	#write_variable('condition_1_line', "condition_1 = c('%s')\n" %("','".join(condition_1)))
	write_variable('condition_2',condition_2)
	#write_variable('condition_2_line', "condition_2 = c('%s')\n" %("','".join(condition_2)))
	write_variable('condition_list_line','condition_list = c("condition_1","condition_2")\n')
	
	write_variable('class_list',class_list)
	print condition_1
	print condition_2
	global condition_1
	global condition_2
	return


def Sample_Names():
	replace_list = [' ','-']
	full_sample_name_list = condition_1+condition_2
	corrected_sample_name_list = []
	for sample_name in full_sample_name_list:
		for replace_entry in replace_list:
			c_sample_name = sample_name.replace(replace_entry,'.')
		corrected_sample_name_list.append(c_sample_name)
	print corrected_sample_name_list
	global corrected_sample_name_list
	#sample_line = "sample_name_list = c('%s')\n" %("','".join(corrected_sample_name_list))
	write_variable('sample_name_list',corrected_sample_name_list)
	global sample_line


def Extract_Sample_Info():
	summary_file = open('%s/summary.txt' %(dirname), 'r')
	summary_list = summary_file.readlines()
	summary_file.close()
	print(summary_list[0:2])
	sample_name_list = []
	raw_file_name_list = []
	for summary_line in summary_list[1:]:
	#print summary_line
		summary_line_list = summary_line.split('\t')
		print summary_line_list[0:3]
		if summary_line_list[1] == '' and summary_line_list[0] != 'Total':
			sample_name_list.append(summary_line_list[0])
		else:
			raw_file_name_list.append(summary_line_list[0])
	print sample_name_list
	print raw_file_name_list
	write_variable('sample_name_list',sample_name_list)
	sample_name_list_R = R_characters(sample_name_list)
	write_variable('sample_name_list_R',sample_name_list_R)
	
	write_variable('raw_file_name_list',raw_file_name_list)


def Extract_Quant():
	R_dir = '02_Label_Free'
	R_file_name = '03_Extract_ALL_Label_Free_Quantiation_Values.R'
	write_variable('selected_file_list',['proteinGroups.txt.edited'])
	add_list = [variable.variable_dic['selected_file_list_line'],variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],'sample_name_list = sample_name_list\n']
	#selected_file_line = 'selected_file_list = c("proteinGroups.txt.edited")'
	#add_list.append(selected_file_line)
	create_R_file(R_dir,R_file_name,add_list)
	#Edit_R_script(R_dir,R_file_name,add_list)
	Run_rscript(R_file_name,R_dir)
	return


def table_name(data_list):
	table_name_list = ['df']+[data_list]
	table_name_line = 'table_name = "df.'+'.'.join(data_list)+'"\n'
	print table_name
	write_variable('table_name_line',table_name_line)
	write_variable('table_name_list',table_name_list)
	
	
def Data_line():
	entry_list = []
	table_order_list = ['selected_file_list','selected_sample_edit_list','selected_quant_list','selected_suffix_list','selected_silac_list','selected_norm_list','selected_mod_list','selected_mod_list_2','selected_mod_list_3','selected_mod_list_4','selected_mod_list_5']
	for key in table_order_list:
		try:
			if variable.variable_dic[key] != ['_'] and variable.variable_dic[key] != ['']:
				entry_list.append(variable.variable_dic[key][0])
				if key == 'selected_file_list':
					#if 'proteinGroups.txt' in variable.variable_dic[key][0] or 'peptides.txt' in variable.variable_dic[key][0]:
					#	if variable.variable_dic['selected_sample_edit_list'] = '_':
					#		entry_list.append('edited')
					original_table = '.'.join(['df']+entry_list)
		except:
			print '%s key not found' %(key)
	#print entry_list
	write_variable('original_table',original_table)
	table_name_list = ['df']+entry_list
	table_name = '.'.join(table_name_list)
	entry_line = 'table_name = "%s"\n' %(table_name)
	#print entry_line
	#raw_input()
	data_line_message.set(entry_line)
	write_variable('table_name',table_name)
	write_variable('table_name_line',entry_line)
	write_variable('table_name_list',table_name_list)
	
	read_file = open('%s/rscripts/dataframe_list.txt' %(dirname))
	dataframe_list = read_file.readlines()
	read_file.close()
	#print dataframe_list
	#print(table_name)
	#words = [w.replace('[br]', '<br />') for w in words]
	dataframe_list = [d.replace('"','') for d in dataframe_list] 
	dataframe_list = [d.replace('\n','') for d in dataframe_list]
	#print(dataframe_list)
	#dataframe_list.replace('\n','')
	if table_name in dataframe_list:
		print '\n\n%s \n\n\tis in workspace\n' %(table_name)
		data_line_message.set('%s' %(entry_line))
		new_status('ready')
	else:
		print '\n\n%s \n\n\tis not in the R workspace, please create it\n' %(table_name)
		data_line_message.set('%s' %(entry_line))
		new_status('table not found')
		
	table_order_list = ['selected_result_list']
	for key in table_order_list:
		try:
			if variable.variable_dic[key] != ['_'] and variable.variable_dic[key] != ['']:
				entry_list.append(variable.variable_dic[key][0])
				if key == 'selected_file_list':
					entry_list.append('edited')
		except:
			print '%s key not found' %(key)
	stat_table_name_list = ['df']+entry_list
	stat_table_name = '.'.join(stat_table_name_list)
	stat_entry_line = 'stat_table_name = "%s"\n' %(stat_table_name)
	#print entry_line
	#raw_input()
	#data_line_message.set(entry_line)
	write_variable('stat_table_name',stat_table_name)
	write_variable('stat_table_name_line',stat_entry_line)
	write_variable('stat_table_name_list',stat_table_name_list)
	
	read_file = open('%s/rscripts/dataframe_list.txt' %(dirname))
	dataframe_list = read_file.readlines()
	read_file.close()
	#print dataframe_list
	#print(table_name)
	#words = [w.replace('[br]', '<br />') for w in words]
	dataframe_list = [d.replace('"','') for d in dataframe_list] 
	dataframe_list = [d.replace('\n','') for d in dataframe_list]
	#print(dataframe_list)
	#dataframe_list.replace('\n','')
	if stat_table_name in dataframe_list:
		print '\n\n%s \n\n\tis in workspace\n' %(stat_table_name)
	else:
		print '\n\n%s \n\n\tis not in the R workspace, please create it\n' %(stat_table_name)
	
	#### CREATE QUANT TABLE NAME ###
	entry_list = []
	#table_order_list = ['selected_file_list','selected_quant_list','selected_suffix_list','selected_sample_edit_list','selected_silac_list','selected_norm_list','selected_mod_list','selected_mod_list_2','selected_mod_list_3']
	table_order_list = ['selected_file_list','selected_quant_list','selected_sample_edit_list','selected_silac_list']
	for key in table_order_list:
		try:
			if variable.variable_dic[key] != ['_'] and variable.variable_dic[key] != ['']:
				entry_list.append(variable.variable_dic[key][0])
				if key == 'selected_file_list':
					entry_list.append('edited')
		except:
			print '%s key not found' %(key)
	#print entry_list
	quant_table_name_list = ['df']+entry_list
	quant_table_name = '.'.join(quant_table_name_list)
	entry_line = 'quant_table_name = "%s"\n' %(quant_table_name)
	#print entry_line
	#raw_input()
	#data_line_message.set(entry_line)
	write_variable('quant_table_name',quant_table_name)
	write_variable('quant_table_name_line',entry_line)
	write_variable('quant_table_name_list',quant_table_name_list)
	column_file_name = 'column_name_%s' %(variable.variable_dic['quant_table_name'])
	columns_file_list = os.listdir('%s/rscripts' %(dirname))
	if column_file_name in columns_file_list:
		colname_file = open('%s/rscripts/column_name_%s' %(dirname,variable.variable_dic['quant_table_name']))
		column_list = colname_file.readlines()
		colname_file.close()
		#print column_list
		column_list = [x.replace('"','') for x in column_list]
		column_list = [x.replace('\n','') for x in column_list]
		#print(column_list)
		prefix = common_prefix(column_list)
		print(prefix)
	#raw_input()
		write_variable('quant_sample_name_list',column_list)
		print(column_list)
	
class Entry_App:
	def __init__(self,master,entry_name,x_position=0,y_position=0,write='no',box_width=3):
		try:
			variable.variable_dic[entry_name]
		except:
			write_variable(entry_name,20)
		self.entry_name = entry_name
		Entry_Frame = LabelFrame(master,text=entry_name)
		if x_position == 'side':
			Entry_Frame.pack(side=eval(y_position))
		else:
			Entry_Frame.place(x = x_position,y=y_position)
		v = StringVar()
		self.e = Entry(Entry_Frame,textvariable=v,width=box_width)
		self.e.pack()

		self.e.delete(0, END)
		self.e.insert(0, variable.variable_dic[entry_name])
		
		v.set(variable.variable_dic[self.entry_name])
		s = v.get()
		#print(s)
		
		self.b = Button(Entry_Frame, text="set", width=3,height=1, command=self.set_button)
		self.b.pack()
		
		label = Label(Entry_Frame, width=box_width+5)
		label.pack()
		#print(Entry_Frame.update())
		#print (Entry_Frame.winfo_width())
		#print (Entry_Frame.winfo_height())
		#print (Entry_Frame.winfo_geometry())
		#print(root.update())
		#print (root.winfo_width())
		#print (root.winfo_height())
		#print (root.winfo_geometry())
		Entry_Frame.update()
		if write == 'x':
			add_x = Entry_Frame.winfo_width()
			write_variable('list_x_position',x_position+add_x)
		if write == 'y':
			add_y = Entry_Frame.winfo_height()
			write_variable('list_y_position',y_position+add_y)
		
	def set_button(self):
		s = self.e.get()
		#print(s)
		write_variable(self.entry_name,s)
		#write_variable(self.entry_name+'_line','%s = %s\n' %(self.entry_name,s))
		#return(s)
		
		
		

class List_App:
	def __init__(self,master,list_name,mylist,x_position=0,y_position=0,write = 'x'):
		#y_position = 250
		List_Frame = LabelFrame(master,text=list_name)
		List_Frame.place(x = x_position,y=y_position)
		#mylist = []
		def close():
			List_Frame.destroy()
		close_button = Button(List_Frame, fg='red',text="close", command=close).pack(side=TOP)
		self.list_name = list_name
		variable_default('selected_'+self.list_name,[''])
		self.opt = []
		#print(mylist)
		self.cb = []
		self.cb_v = []
		for ix, text in enumerate(mylist):
			#print(ix)
			y_value = ix+1
			self.cb_v.append(StringVar())
			off_value=0  #whatever you want it to be when the checkbutton is off
			self.cb.append(Checkbutton(List_Frame, text=text, wraplength=150, onvalue=text,offvalue=off_value,variable=self.cb_v[ix],command=self.chkbox_checked))
			if ix == 0:
				self.cb[ix].pack(anchor = W)
			else:
				self.cb[ix].pack(anchor = W)
			#place(x=100, y=(y_value*20)+200)
			self.opt.append(off_value)
			self.cb[-1].deselect() #uncheck the boxes initially.
		#for selection_entry in variable.variable_dic['selected_'+self.list_name]:
		#	self.cb_v[ix].set(selection_entry)
		#self.cb_v[ix].set(variable.variable_dic['selected_'+self.list_name])
		label = Label(List_Frame, width=20)
		label.pack()
		#print(List_Frame.update())
		#print (List_Frame.winfo_width())
		#print (List_Frame.winfo_height())
		#print (List_Frame.winfo_geometry())
		#print(root.update())
		#print (root.winfo_width())
		#print (root.winfo_height())
		#print (root.winfo_geometry())
		List_Frame.update()
		if write == 'x':
			add_x = List_Frame.winfo_width()
			write_variable('list_x_position',x_position+add_x)
		#place(x=110, y=(y_value*20+200))

		
		
	
	
	def chkbox_checked(self):
		for ix, item in enumerate(self.cb):
			#print self.cb[ix].get()
			self.opt[ix]=(self.cb_v[ix].get())
		#print self.opt
		#print self.cb_v.get()
		selected_intensity_list = [x for x in self.opt if x != '0']
		#selected_intensity_list.remove('0')
		#print(selected_intensity_list)
		selected_intensity_list = R_characters(selected_intensity_list)
		#print(selected_intensity_list)
		write_variable('selected_'+self.list_name,selected_intensity_list)
		#write_variable(self.list_name+'_entry','%s = "%s"\n' %(self.list_name,entry))
		Data_line()
class RadioButton_List_App:
	def __init__(self,master,list_name,mylist,x_position=0,y_position=0,write_x = 'yes'):
		#y_position = 250
		List_Frame = LabelFrame(master,text=list_name)
		if x_position == 'side':
			List_Frame.pack(side=eval(y_position))
			write_x = 'no'
		else:
			List_Frame.place(x = x_position,y=y_position)
		#mylist = []
		self.list_name = list_name
		self.opt = []
		#print(mylist)
		self.cb = []
		self.cb_v = []
		v = StringVar()
		v.set("L")
		for ix, text in enumerate(mylist):
			#print(ix)
			y_value = ix+1
			self.cb_v.append(StringVar())
			off_value=0  #whatever you want it to be when the checkbutton is off
			self.cb.append(Radiobutton(List_Frame, text=text,wraplength=150,value = ix,variable=v,command=lambda i=mylist[ix]: self.chkbox_checked(i)))
			self.cb[ix].pack(anchor = W)
			#place(x=100, y=(y_value*20)+200)
			#self.opt.append(off_value)
			#self.cb[-1].deselect() #uncheck the boxes initially.
		#self.cb.set(variable.variable_dic[self.list_name+'_entry'])
		label = Label(List_Frame, width=20)
		label.pack()
		#print(List_Frame.update())
		#print (List_Frame.winfo_width())
		#print (List_Frame.winfo_height())
		#print (List_Frame.winfo_geometry())
		#print(root.update())
		#print (root.winfo_width())
		#print (root.winfo_height())
		#print (root.winfo_geometry())
		List_Frame.update()
		if write_x == 'yes':
			add_x = List_Frame.winfo_width()
			write_variable('list_x_position',x_position+add_x)
		#place(x=110, y=(y_value*20+200))
		
	
	def chkbox_checked(self,entry):
		print '%s_entry : %s' %(self.list_name,entry)
		write_variable(self.list_name+'_entry',entry)
		
class SpinBox_List_App:
	def __init__(self,master,list_name,mylist,x_position=0,y_position=0,write = 'x'):
		
		#y_position = 250
		List_Frame = LabelFrame(master,text=list_name)
		if x_position == 'side':
			List_Frame.pack(side=eval(y_position))
			write_x = 'no'
		else:
			List_Frame.place(x = x_position,y=y_position)
		#mylist = []
		self.list_name = list_name
		variable_default('selected_'+self.list_name,[''])
		self.opt = []
		#print(mylist)
		self.cb = []
		self.cb_v = []
		v = StringVar()
		v.set("L")
		self.entry = Spinbox(List_Frame,values=mylist,command = self.chkbox_checked)
		#self.entry = OptionMenu(List_Frame,values=mylist,command = self.chkbox_checked)

		self.entry.pack()
		
		self.entry.delete(0,'end')
		self.entry.insert(0,variable.variable_dic['selected_'+self.list_name][0])
		label = Label(List_Frame, width=10)
		label.pack()
		#print(List_Frame.update())
		#print (List_Frame.winfo_width())
		#print (List_Frame.winfo_height())
		#print (List_Frame.winfo_geometry())
		#print(root.update())
		#print (root.winfo_width())
		#print (root.winfo_height())
		#print (root.winfo_geometry())
		List_Frame.update()
		if write == 'x':
			add_x = List_Frame.winfo_width()
			write_variable('list_x_position',x_position+add_x)
		if write == 'y':
			add_y = List_Frame.winfo_height()
			write_variable('list_y_position',y_position+add_y)
		#place(x=110, y=(y_value*20+200))
	def chkbox_checked(self):
		entry = self.entry.get()
		#print(entry)
		write_variable(self.list_name+'_entry',entry)
		#selected_intensity_list = [x for x in self.opt if x != '0']
		#selected_intensity_list.remove('0')
		#print(selected_intensity_list)
		selected_intensity_list = R_characters([entry])
		#print(selected_intensity_list)
		write_variable('selected_'+self.list_name,selected_intensity_list)
		#raw_input(selected_intensity_list)
		Data_line()
		print selected_intensity_list

class Extract_App:
	def __init__(self,master):
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		Extract_Frame = LabelFrame(master,text = 'Quant')
		Extract_Frame.place(x=x_position,y=y_position)
		f = open('%s/proteinGroups.txt' %(dirname), 'r')
		read_lines = f.readlines()
		f.close()
		#print read_lines[0]
		intensity_list = ['','Intensity','LFQ.intensity','iBAQ']
		#intensity_list = []
		#for intensity in expected_intensity_list:
		#	if intensity in read_lines[0]:
		#		intensity_list.append(intensity)
		self.opt = []
		
		mylist = intensity_list
		#print(mylist)
		self.cb = []
		self.cb_v = []
		for ix, text in enumerate(mylist):
			#print(ix)
			y_value = ix+1
			self.cb_v.append(StringVar())
			off_value=0  #whatever you want it to be when the checkbutton is off
			self.cb.append(Checkbutton(Extract_Frame, text=text, onvalue=text,offvalue=off_value,variable=self.cb_v[ix],command=self.chkbox_checked))
			self.cb[ix].pack(side=BOTTOM)
			#place(x=100, y=(y_value*20)+200)
			self.opt.append(off_value)
			self.cb[-1].deselect() #uncheck the boxes initially.
		label = Label(Extract_Frame, width=20)
		label.pack(side=BOTTOM)
		#place(x=110, y=(y_value*20+200))

		
		self.hi_there = Button(Extract_Frame, text="Extract", command=self.Extract).pack(side=BOTTOM)
		#List_Frame.update()
		#add_x = List_Frame.winfo_width()
		#write_variable('list_x_position',x_position+add_x)
	
	def chkbox_checked(self):
		for ix, item in enumerate(self.cb):
			self.opt[ix]=(self.cb_v[ix].get())
		#print self.opt
		selected_intensity_list = [x for x in self.opt if x != '0']
		#selected_intensity_list.remove('0')
		#print(selected_intensity_list)
		write_variable('selected_quant_list',selected_intensity_list)
	
	def Extract(self):
		Extract_Quant()
		status('Extracted iBAQ Values')
		
class Sample_App:
	def __init__(self,master):
		self.Extract_Frame = LabelFrame(master,text="Sample Names",padx=5, pady=5)
		self.Extract_Frame.place(y=250)
		self.close_frame = LabelFrame(self.Extract_Frame,text="")
		self.close_frame.pack(side=BOTTOM)
		
		f = open('%s/proteinGroups.txt' %(dirname), 'r')
		read_lines = f.readlines()
		f.close()
		#print read_lines[0]
		experiment_list = variable.variable_dic['sample_name_list']
		sample_name_list_R = R_characters(sample_name_list)
		write_variable('sample_name_list_R',sample_name_list_R)
		
		#intensity_list = []
		#for intensity in experiment_list:
		#	if intensity in read_lines[0]:
		#		intensity_list.append(intensity)
		self.opt = []
		
		mylist = variable.variable_dic['quant_list']
		#print(mylist)
		self.cb = []
		self.cb_v = []
		for ix, text in enumerate(mylist):
			print(ix)
			y_value = ix+1
			self.cb_v.append(StringVar())
			off_value=0  #whatever you want it to be when the checkbutton is off
			self.cb.append(Checkbutton(self.Extract_Frame, text=text, onvalue=text,offvalue=off_value,variable=self.cb_v[ix],command=self.chkbox_checked))
			self.cb[ix].pack(side=RIGHT)
			#place(x=100, y=(y_value*20)+200)
			self.opt.append(off_value)
			self.cb[-1].deselect() #uncheck the boxes initially.
		label = Label(self.Extract_Frame, width=20)
		label.pack(side=RIGHT)
		#place(x=110, y=(y_value*20+200))
		
		self.extract_button = Button(self.Extract_Frame, text="Extract_Sample", command=self.Extract_Tool).pack(side=BOTTOM)
		
		self.extract_button = Button(self.close_frame, text="close",fg="red", command=self.close).pack(side=RIGHT)
		
	def Extract(self):
		print common_prefix(variable.variable_dic['selected_experiment_list'])
		#Extract_Quant()
		status('Extracted iBAQ Values')
		R_dir = '02_LFQ_Timecourse'
		R_file_name = '03_Extract_single_Sample.R'
		sample_prefix = common_prefix(variable.variable_dic['selected_experiment_list'])
		print sample_prefix
		try:
			sample_prefix_list = variable.variable_dic['sample_prefix_list']
		except:
			sample_prefix_list = []
		if sample_prefix not in sample_prefix_list:
			sample_prefix_list.append(sample_prefix)
			write_variable('sample_prefix_list',sample_prefix_list)
			x_position = variable.variable_dic['list_x_position']
			y_position = variable.variable_dic['list_y_position']
			variable_list = 'sample_prefix_list'
			mylist = variable.variable_dic[variable_list]
			test_list = List_App(root,variable_list,mylist,x_position,y_position)
		#raw_input()
		write_variable('sample_prefix_line','sample_prefix = "%s"' %(sample_prefix))
		add_list = [variable.variable_dic['selected_experiment_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['sample_prefix_line']]
		#add_list = [variable.variable_dic['selected_experiment_list_line']]
		#add_list = [variable.variable_dic['selected_experiment_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['sample_prefix_list_line']]
		#add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)
		return
	
	def Extract_Tool(self):
		print common_prefix(variable.variable_dic['selected_experiment_list'])
		#Extract_Quant()
		status('Extracted iBAQ Values')
		R_dir = 'Tools'
		R_file_name = 'Extract_single_Sample.R'
		sample_prefix = common_prefix(variable.variable_dic['selected_experiment_list'])
		print sample_prefix
		try:
			sample_prefix_list = variable.variable_dic['sample_prefix_list']
		except:
			sample_prefix_list = []
		if sample_prefix not in sample_prefix_list:
			sample_prefix_list.append(sample_prefix)
			write_variable('sample_prefix_list',sample_prefix_list)
			x_position = variable.variable_dic['list_x_position']
			y_position = variable.variable_dic['list_y_position']
			variable_list = 'sample_prefix_list'
			mylist = variable.variable_dic[variable_list]
			test_list = List_App(root,variable_list,mylist,x_position,y_position)
		#raw_input()
		write_variable('sample_prefix_line','sample_prefix = "%s"' %(sample_prefix))
		add_list = [variable.variable_dic['selected_experiment_list_line'],variable.variable_dic['table_name_line'],variable.variable_dic['sample_prefix_line']]
		#add_list = [variable.variable_dic['selected_experiment_list_line']]
		#add_list = [variable.variable_dic['selected_experiment_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['sample_prefix_list_line']]
		#add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)
		return
		
	
	def chkbox_checked(self):
		for ix, item in enumerate(self.cb):
			self.opt[ix]=(self.cb_v[ix].get())
		#print self.opt
		selected_intensity_list = [x for x in self.opt if x != '0']
		#selected_intensity_list.remove('0')
		#print(selected_intensity_list)
		selected_intensity_list = R_characters(selected_intensity_list)
		#print(selected_intensity_list)
		write_variable('selected_experiment_list',selected_intensity_list)
		sample_prefix = common_prefix(variable.variable_dic['selected_experiment_list'])
		#print sample_prefix
	
	def close(self):
		self.Extract_Frame.destroy()

class View_App:
	def __init__(self,master):
		view_frame = Frame(master)
		view_frame.pack()
		self.hi_there = Button(view_frame, text="Boxplot", command=self.Boxplot)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Heatmap", command=self.Heatmap)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Plot", command=self.Plot)
		self.hi_there.pack(side=LEFT)
		
	def Boxplot(self):
		print 'boxplot'
		R_dir = '02_Label_Free'
		R_file_name = '05_View_boxplot_2.R'
		add_list = [variable.variable_dic['selected_file_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line']]
		#selected_file_line = 'selected_file_list = c("proteinGroups.txt.edited")'
		#add_list.append(selected_file_line)
		#create_R_file(R_dir,R_file_name)
		create_Run_Sweave_file(R_dir,R_file_name,add_list)
		#Edit_R_script(R_dir,R_file_name,add_list)
		#Run_rscript(R_file_name,R_dir)
		
	
	def Heatmap(self):
		print "Heatmap"
		R_dir = '02_Label_Free'
		R_file_name = '05_View_heatmaps.R'
		add_list = [variable.variable_dic['selected_file_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line']]
		#selected_file_line = 'selected_file_list = c("proteinGroups.txt.edited")'
		#add_list.append(selected_file_line)
		create_Run_Sweave_file(R_dir,R_file_name,add_list)
		#create_R_file(R_dir,R_file_name)
		
		#Run_rscript(R_file_name,R_dir)
		return
		
	def Plot(self):
		print "PLOT"
		R_dir = '02_Label_Free'
		R_file_name = '05_View_plot.R'
		add_list = [variable.variable_dic['selected_file_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line']]
		#selected_file_line = 'selected_file_list = c("proteinGroups.txt.edited")'
		#add_list.append(selected_file_line)
		create_Run_Sweave_file(R_dir,R_file_name,add_list)
		#create_R_file(R_dir,R_file_name)
		#Run_rscript(R_file_name,R_dir)
		return


class LFQ_App:
	def __init__(self, master):
		#LFQ2_frame = Frame(master)
		#LFQ2_frame.pack(side=BOTTOM)
		self.R_dir = '02_Label_Free'
		self.LFQ_frame = LabelFrame(master,text="2 Condition LFQ",padx=5, pady=5)
		self.LFQ_frame.place(y=240,x=100)
		self.Process_frame = LabelFrame(self.LFQ_frame,text="Individual Functions within Process Data",padx=5, pady=5)
		self.Process_frame.pack(side=BOTTOM)
		self.close_frame = LabelFrame(self.Process_frame,text="")
		self.close_frame.pack(side=BOTTOM)
		
		close_button = Button(self.close_frame, fg='red',text="close", command=self.close).pack(side=LEFT)

		extract_button = Button(self.LFQ_frame, text="Extract Quant Values", command=self.Extract).pack(side=LEFT)
		
		self.hi_there = Button(self.LFQ_frame, text="Process Data", command=self.Process_Data)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(self.Process_frame, text="Report", command=self.Report)
		self.hi_there.pack(side=RIGHT)
		
		self.hi_there = Button(self.Process_frame, text="Write", command=self.Write)
		self.hi_there.pack(side=RIGHT)
		
		self.hi_there = Button(self.Process_frame, text="Select", command=self.Select)
		self.hi_there.pack(side=RIGHT)
		
		self.hi_there = Button(self.Process_frame, text="Variance", command=self.Variance)
		self.hi_there.pack(side=RIGHT)
		
		self.hi_there = Button(self.Process_frame, text="Ratios", command=self.Ratios)
		self.hi_there.pack(side=RIGHT)

						
		normalise_button = Button(self.Process_frame, text="Normalise", command=self.Normalise).pack(side=RIGHT)
		

		self.hi_there = Button(self.Process_frame, text="View", command=self.View)
		self.hi_there.pack(side=RIGHT)
		
	def close(self):
		print 'close'
		self.LFQ_frame.destroy()
		self.hi_there.forget()
		
		
	def knitr(self,R_file_name):
		add_list = [variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
		if variable.variable_dic['edit'] == 'yes':
			create_R_file(self.R_dir,R_file_name,add_list)
			Run_rstudio(R_file_name,self.R_dir)
		else:
			#add_line = ['table_name = "%s"' %(table_name)]
			add_file_name = '_'+variable.variable_dic['table_name'].replace('.','_')+'_'
			Run_knitr(self.R_dir,R_file_name,add_list,add_file_name)
			status(R_file_name)
		return

	def Report(self):
		R_dir = '02_Label_Free'
		R_file_name = 'Report.R'
		for suffix in variable.variable_dic['selected_suffix_list']:
			table_name = 'df.proteinGroups.txt.edited.iBAQ%s' %(suffix)
			add_line = ['table_name = "%s"' %(table_name)]
			add_file_name = '_'+table_name.replace('.','_')+'_'
			Simple_Run_Sweave_file(R_dir,R_file_name,add_line,add_file_name)

	def ExpClose(self):
		extract_button.forget()
		normalise_button.forget()
		status("LFQ Closed")

	def Process_Data(self):
		R_dir = '02_Label_Free'
		write_variable('selected_suffix_list',['','.na.z'])
		write_variable('selected_file_list',["proteinGroups.txt.edited"])
		add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_file_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line'],varaibel.variable_dic['table_name_line'],variable_variable_dic['table_name_list_list']]
		R_file_list =['04_Normalisation.R','06_Ratio_Experimental_Design.R','07_Variance.R','08_Select.R','09_Write.R']
		for R_file_name in R_file_list:
			create_R_file(R_dir,R_file_name,add_list)
			Run_rscript(R_file_name,R_dir)
			status(R_file_name)
		return

	def Write(self):
		R_dir = '02_Label_Free'
		R_file_name = '09_Write.R'
		add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line']]
		selected_file_line = 'selected_file_list = c("proteinGroups.txt.edited")'
		add_list.append(selected_file_line)
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)


	def Select(self):
		R_dir = '02_Label_Free'
		R_file_name = '08_Select.R'
		add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)
		
	def Variance(self):
		R_dir = '02_Label_Free'
		R_file_name = '07_Variance.R'
		add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line']]
		selected_file_line = 'selected_file_list = c("proteinGroups.txt.edited")'
		add_list.append(selected_file_line)
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)


	def Ratios(self):
		R_dir = '02_Label_Free'
		R_file_name = '06_Ratio_Experimental_Design.R'
		
		add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line'],variable.variable_dic['table_name_line'],variable.variable_dic['table_name_list_line']]
		#selected_file_line = 'selected_file_list = c("proteinGroups.txt.edited")'
		#add_list.append(selected_file_line)
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)

	def View(self):
		View_app = View_App(root)

	def Normalise(self):
		R_dir = '02_Label_Free'
		R_file_name = '04_Normalisation.R'
		create_R_file(R_dir,R_file_name)
		Run_rscript(R_file_name,R_dir)
		write_variable('selected_suffix_list',['','.na.z'])
		status(R_file_name)
		return

	def Extract(self):
		#runnint.set("Extract LFQ...")
		Sample_Names()
		Extract_app = Extract_App(root)
		#status("Extract")


		
class SQlite_App:
	def __init__(self,master):
		sql_window = Tkinter.Tk()
		sql_window.title("SQLite Window")
		SQ_frame = LabelFrame(sql_window,text='Master',padx=5, pady=5)
		SQ_frame.place(x=50,y=80)
		
		self.path_line = StringVar()
		file_label = Label(SQ_frame, textvariable=self.path_line).pack(side=TOP)
		try:
			SQlite_path = variable.variable_dic['SQlite_path']
			print(SQlite_path)
			self.path_line.set(SQlite_path)
		except:
			self.path_line.set(dirname)
		
		self.hi_there = Button(SQ_frame, text="Generic", command=self.Generic)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="MaxQuant", command=self.MaxQuant)
		self.hi_there.pack(side=LEFT)
		
	def Generic(self):
		generic_SQlite_app = Generic_SQlite_App(root)
		
	def MaxQuant(self):
		maxquant_SQlite_app = MaxQuant_SQlite_App(root)

class Generic_SQlite_App():
	def __init__(self,master):
		Generic_frame = LabelFrame(master,text = 'Generic')
		Generic_frame.place(x=50,y=150)
		
		self.file_line = StringVar()
		file_label = Label(Generic_frame, textvariable=self.file_line).pack(side=TOP)
		try:
			tab_file_name = variable.variable_dic['tab_file_name']
			print(tab_file_name)
			self.file_line.set(tab_file_name)
		except:
			self.file_line.set('no file selected')
		
		new_file = Button(Generic_frame,text='Load New File',command=self.new_file).pack(side=LEFT)
		
		generic_load = Button(Generic_frame,text='TAB file',command=self.tab).pack(side=LEFT)
				
		database_info = Button(Generic_frame,text='Database Info',command=lambda i='database_info.py': self.run_python_script(i)).pack(side=LEFT)
		
		simple_extract = Button(Generic_frame,text='Houskeeping Genes',command=lambda i='simple_extract.py': self.run_python_script(i)).pack(side=LEFT)
		

		
	def run_python_script(self,python_file_name):
		cmd = 'python %s/SQlite/Generic_Tools/%s' %(python_path,python_file_name)
		print(cmd)
		os.system(cmd)
		print cmd
		print(python_file_name)
		
	
	def new_file(self):
		print 'tab'
		file_path = Get_File_name_path(dirname)
		file_name = os.path.basename(file_path)
		print(file_name)
		path_name = os.path.dirname(file_path)
		print path_name
		write_variable('tab_file_path',file_path)
		write_variable('tab_file_name',file_name)
		write_variable('SQlite_path',path_name)
		
		self.file_line.set(file_name)
		sqlite_app.path_line.set(path_name)
		
	def tab(self):
		file_path = variable.variable_dic['tab_file_path']
		cmd = 'python %s/SQlite/Generic_Tools/import_tab_file.py %s' %(python_path,file_path)
		print(cmd)
		os.system(cmd)
		
		
		

class MaxQuant_SQlite_App:
	def __init__(self,master):
		SQ_frame = LabelFrame(master,text='SQlite MaxQuant',padx=5, pady=5)
		SQ_frame.place(x=50,y=200)
		
		self.hi_there = Button(SQ_frame, text="Load Data", command=self.Load)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="Peptides search Protein", command=self.peptide_protein)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="Peptides search sequence", command=self.peptide_sequence)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="Peptides search mass", command=self.peptide_mass)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="msmsScans search mass", command=self.msmsScans_mass)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="Inclusion search protein", command=self.inclusion_protein)
		self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="SELECT", command=self.SELECT)
		self.hi_there.pack(side=LEFT)
		
		trim = Button(SQ_frame, text="TRIM", command=self.TRIM).pack(side=LEFT)
		
		self.hi_there = Button(SQ_frame, text="close", command=self.close)
		self.hi_there.pack(side=LEFT)
		
		generic_load = Button(Generic_frame,text='Genetic Load',command=self.generic_load).pack(side=LEFT)
		
	def generic_load(self):
		print 'load'
		
	def Load(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s LOAD ""' %(python_path,dirname)
		print(cmd)
		os.system(cmd)
	def peptide_protein(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s Peptides "%s/select/protein_list.txt"' %(python_path,dirname,dirname)
		print(cmd)
		os.system(cmd)
	def peptide_sequence(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s peptide_sequence "%s/select/peptide_list.txt"' %(python_path,dirname,dirname)
		print(cmd)
		os.system(cmd)
	
	def peptide_mass(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s peptide_mass "%s/select/mass_list.txt"' %(python_path,dirname,dirname)
		print(cmd)
		os.system(cmd)
	
	def msmsScans_mass(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s msmsScans_mass "%s/select/mass_list.txt"' %(python_path,dirname,dirname)
		print(cmd)
		os.system(cmd)
	def inclusion_protein(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s inclusion_protein "%s/select/protein_list.txt"' %(python_path,dirname,dirname)
		print(cmd)
		os.system(cmd)
	def SELECT(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s SELECT "%s/select/protein_list.txt"' %(python_path,dirname,dirname)
		print(cmd)
		os.system(cmd)
	def TRIM(self):
		cmd = 'python %s/SQlite/00_MQ_sqlite_compiled.py %s TRIM "%s/select/protein_list.txt"' %(python_path,dirname,dirname)
		print(cmd)
		os.system(cmd)
	def close(self):
		trim.pack_forget()
		



class Edit_R_App:
	def __init__(self,master):
		edit_tk = Tkinter.Tk()
		
		Edit_R_Frame = LabelFrame(edit_tk,text="Edit",padx=5, pady=5)
		Edit_R_Frame.place(y=110,x=1100)
		
		self.hi_there = Button(edit_tk, text="Clear All",bg='red',command=self.Clear_All)
		self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="Clear Images and Tables",bg='red',command=self.Clear_images_and_tables)
		self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="Clear Write",bg='red',command=self.Clear_Write)
		self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="Re_Initialise", command=self.Re_initialise)
		self.hi_there.pack(side=BOTTOM)
		
		#self.hi_there = Button(Edit_R_Frame, text="copy", command=self.copy).pack(side=BOTTOM)
		
		#self.hi_there = Button(Edit_R_Frame, text="run", command=self.run)
		#self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="Reload\nFunctions", command=ReLoad_Functions)
		self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="git push\update bb2", command=self.update_git)
		self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="git push", command=self.git_push)
		self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="git pull", command=self.git_pull)
		self.hi_there.pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="Data Info", command=lambda R_file_name='info.R',R_dir = 'Tools': self.rscript(R_file_name,R_dir)).pack(side=LEFT)
		
		self.hi_there = Button(edit_tk, text="Data Folder", command=self.data_folder).pack(side=BOTTOM)
		
		self.hi_there = Button(edit_tk, text="Script Folder", command=self.script_folder).pack(side=LEFT)
		
		self.hi_there = Button(edit_tk, text="edit code", bg='blue',command=self.edit_code).pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="edit variable", bg='blue',command=self.edit_variable).pack(side=LEFT)
		
		self.hi_there = Button(edit_tk, text="variable", bg='blue',command=self.variable).pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="preamble", bg='blue',command=self.preamble).pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Rename Columns", command=lambda R_file_name='rename.R',R_dir = 'Tools': tools_list(R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(edit_tk, text="Rename df", command=lambda R_file_name='df_name.R',R_dir = 'Tools': tools_list(R_dir,R_file_name,['result_list_entry_line_str'])).pack(side=LEFT)

		self.hi_there = Button(edit_tk, text="add_file", command=self.add_file).pack(side=TOP)
	
	def edit_variable(self):
		import MQ_functions
		cmd = 'python scripts/edit_variable.py %s' %(dirname)
		print(cmd)
		os.system(cmd)
		#MQ_functions.edit_variable(dirname)


	def add_file(self):
		load_file_list = variable.variable_dic['file_list']
		print(load_file_list)
		file_name = raw_input("table_name : ")
		load_file_list.append(file_name)
		print(load_file_list)
		write_variable('file_list',list(set(load_file_list)))
		variable_list = 'file_list'
		

	
	def preamble(self):
		preamble_edit()
		
	def variable(self):
		print_variable
	
	def edit_code(self):
		cmd = 'kate %s' %(os.path.join(python_path,'Run_Load.py'))
		subprocess.Popen(cmd,shell=True)
		#os.system(cmd)
	
	def script_folder(self):
		cmd = '%s %s' %(file_explorer,python_path)
		subprocess.Popen(cmd,shell=True)
		#os.system(cmd)
	def data_folder(self):
		cmd = '%s %s' %(file_explorer,dirname)
		subprocess.Popen(cmd,shell=True)
		#os.system(cmd)
	def update_git(self):
		os.system('git add *')
		os.system('git rm --cached location.txt')
		commit_line_add = raw_input("update - ")
		os.system('git commit -am "update - %s"' %(commit_line_add))
		cmd = 'bash %s/git_update.bash' %(python_path)
		print cmd
		read_file = open('%s/git_update.bash' %(python_path),'r')
		read_list = read_file.readlines()
		read_file.close()
		
		for read_line in read_list:
			print read_line
		os.system(cmd)
	def git_push(self):
		cmd = 'git add *'
		os.system(cmd)
		os.system('git rm --cached location.txt')
		cmd = 'git commit -am "before pull"'
		os.system(cmd)
		cmd = 'git push origin'
		os.system(cmd)
	def git_pull(self):
		cmd = 'git add *'
		os.system(cmd)
		os.system('git rm --cached location.txt')
		cmd = 'git commit -am "before pull"'
		os.system(cmd)
		cmd = 'git pull origin'
		os.system(cmd)

	def Re_initialise(self):
		Initialise()
		
	def rscript(self,R_file_name,R_dir,add_list = []):
		add_list = add_list+[variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
		create_R_file(R_dir,R_file_name,add_list)
		Run_rscript(R_file_name,R_dir)
		status(R_file_name)
		return
		
	def copy(self):
		copy_cmd = variable.variable_dic['copy_cmd']
		print copy_cmd
		#copy_to_source = raw_input('copy (y/n) : ')
		os.system(copy_cmd)
		
	def Functions(self):
		
		#cmd = 'cp -r %s/00_functions %s/rscripts/' %(python_path,dirname)
		#print(cmd)
		#os.system(cmd)
		R_dir = '00_config'
		R_file_name = 'Load_functions.R'
		#add_list = ["source('%s/00_functions/00_01_functions.R')\n" %(python_path)]
		add_list = ["function_location = '%s/rscripts/00_functions/'" %(dirname)]
		Copy_Functions()
		create_R_file(R_dir,R_file_name,add_list,variable.variable_dic['save_list'])
		Run_rscript(R_file_name,R_dir)

		status(R_file_name)
		#Run_rscript(R_file_name,R_dir,add_list)

	def edit(self):
		write_variable('edit','yes')
	def run(self):
		write_variable('edit','no')
	
	def Clear_All(self):
		folder_list = ['images','workspace','output','rscripts','tables']
		for folder_name in folder_list:
			cmd = 'rm -r %s/%s' %(dirname,folder_name)
			print(cmd)
			os.system(cmd)
			
	def Clear_images_and_tables(self):
		folder_list = ['images','tables']
		for folder_name in folder_list:
			folder_name = '%s/%s' %(dirname,folder_name)
			cmd = 'rm -r %s' %(folder_name)
			print(cmd)
			os.system(cmd)
			cmd = 'mkdir %s' %(folder_name)
			print(cmd)
			os.system(cmd)
			
	def Clear_Write(self):
		folder_list = ['output/selected']
		for folder_name in folder_list:
			folder_name = '%s/%s' %(dirname,folder_name)
			cmd = 'rm -r %s' %(folder_name)
			print(cmd)
			os.system(cmd)
			cmd = 'mkdir %s' %(folder_name)
			print(cmd)
			os.system(cmd)

			

class LFQ_TimeCourse_App:
	def __init__(self,master):
		self.R_dir = '02_LFQ_Timecourse'
		
		#LFQ_Time_Frame = Frame(master).place(x=500,y=200)
		LFQ_Time_Frame = LabelFrame(master,text="LFQ Time Course",padx=5, pady=5)
		LFQ_Time_Frame.place(y=250,x=700)
		#self.extract_button = Button(LFQ_Time_Frame, text="Extract LFQ", command=self.Extract).place(x=300,y=100)
		
		self.extract_button = Button(LFQ_Time_Frame, text="Select Samples", command=self.separate_samples).pack(side=LEFT)
		
		self.extract_button = Button(LFQ_Time_Frame, text="C Means", command=self.cmeans).pack(side=LEFT)
		
		self.extract_button = Button(LFQ_Time_Frame, text="cluster", command=self.cluster).pack(side=LEFT)
		
		self.hi_there = Button(LFQ_Time_Frame, text="matplot", command=lambda i='matplot.R': self.rscript(i)).pack(side=LEFT)
		
		
		
		try:
			#variable.variable_dic['sample_prefix_list']
			x_position = variable.variable_dic['list_x_position']
			y_position = variable.variable_dic['list_y_position']
			variable_list = 'sample_prefix_list'
			mylist = variable.variable_dic[variable_list]
			test_list = List_App(root,variable_list,mylist,x_position,y_position)
		except:
			print 'no samples selected'
	
	def rscript(self,R_file_name):
		add_list = [variable.variable_dic['table_name_line_str'],variable.variable_dic['experiment_name_line_str']]+variable.variable_dic['preamble']
		create_R_file(self.R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,self.R_dir)
		status(R_file_name)
	
	def separate_samples(self):
		
		experiment_list = variable.variable_dic['sample_name_list']
		
		print experiment_list
		sample_app = Sample_App(root)
	
	def Extract(self):
		print 'Extract'
		Extract_Sample_Info()
		Extract_app = Extract_App(root)
		
	def cmeans(self):
		R_dir = '02_LFQ_Timecourse'
		R_file_name = 'cmeans.R'
		sample_prefix = common_prefix(variable.variable_dic['selected_experiment_list'])
		add_variable_list('sample_prefix_list',sample_prefix)
		#raw_input()
		
		add_list = [variable.variable_dic['selected_experiment_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_sample_prefix_list_line']]
		#add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,R_dir)
		status(R_file_name)
		return
	
	def cluster(self):
		R_dir = '02_LFQ_Timecourse'
		R_file_name = 'iTRAQ_cluster_dens.R'
		sample_prefix = common_prefix(variable.variable_dic['selected_experiment_list'])
		add_variable_list('sample_prefix_list',sample_prefix)
		#raw_input()
		add_list = [variable.variable_dic['selected_experiment_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['sample_prefix_line']]
		#add_list = [variable.variable_dic['sample_name_list_line'],variable.variable_dic['selected_quant_list_line'],variable.variable_dic['selected_suffix_list_line'],variable.variable_dic['condition_1_line'],variable.variable_dic['condition_2_line'],variable.variable_dic['condition_list_line']]
		create_R_file(R_dir,R_file_name,add_list)
		Run_rstudio(R_file_name,R_dir)
		status(R_file_name)
		return
		

class Custom_App:
	def __init__(self,master):
		edit_tk = Tkinter.Tk()
		
		self.hi_there = Button(edit_tk, text="Load IPA data",command=self.Load_IPA)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Load List",command=self.Load_List)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Load Function List",command=self.Load_Function)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Table List",command=self.table_list)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="View List",command=self.view_list)
		self.hi_there.pack(side=TOP)
		
		
		self.hi_there = Button(edit_tk, text="Edit List",command=self.edit_list)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Delete List",command=self.delete_list)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Merge Data Frames",command=self.merge_df)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Rename df",command=self.rename_df)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Rename and Remove df",command=self.rename_and_remove_subs)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="Remove df",command=self.remove_subs)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="save df",command=self.save_df)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="reload df",command=self.reload_df)
		self.hi_there.pack(side=TOP)
		
		self.hi_there = Button(edit_tk, text="reload df path",command=self.reload_df_path)
		self.hi_there.pack(side=TOP)
		
	def view_list(self):
		R_file_name='view_list.R'
		R_dir = 'Tools'
		key_list = ['uniprot_list_entry_line_str']
		tools_list(R_dir,R_file_name,key_list)
		
	def delete_list(self):
		uniprot_list = variable.variable_dic['uniprot_list']
		list_name = variable.variable_dic['uniprot_list_entry']
		print uniprot_list
		print list_name
		uniprot_list.remove(list_name)
		print uniprot_list
		#raw_input()
		uniprot_list.sort()
		write_variable('uniprot_list',uniprot_list)
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,variable.variable_dic['uniprot_x'],variable.variable_dic['uniprot_y'])
		
		R_file_name='remove_list.R'
		R_dir = 'Tools'
		key_list = ['uniprot_list_entry_line_str']
		tools_list(R_dir,R_file_name,key_list)
		
	def edit_list(self):
		file_name = str(variable.variable_dic['uniprot_list_entry'])
		print(file_name)
		uniprot_list = variable.variable_dic['uniprot_list']
		new_file_name = file_name + '.edited'
		uniprot_list.append(new_file_name)
		uniprot_list = list(set(uniprot_list))
		uniprot_list.sort()
		print(uniprot_list)
		#raw_input()
		write_variable('uniprot_list',uniprot_list)
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,variable.variable_dic['uniprot_x'],variable.variable_dic['uniprot_y'])
		
		
		R_file_name ='edit_list.R'
		key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line_str','sd_cutoff_line','t_test_list_entry_line_str']
		knitr_list('Tools',R_file_name,key_list)

	def Load_Function(self):
		base_dir = variable.variable_dic['base_dir']
		file_path = Get_File_name_path(base_dir)
		print file_path
		add_list = ['file_path = "%s"\n' %(file_path)]
		tools('Tools','Load_Function_List.R',add_list)
		#raw_input('loaded files ...')
		file_name_list = file_path.split('/')
		print(file_name_list)
		file_name = file_name_list[len(file_name_list)-1]
		print(file_name)
		search_list = ['affects','increases','decreases']
		
		uniprot_list = variable.variable_dic['uniprot_list']
		for search_entry in search_list:
			write_file_name = '%s.%s' %(file_name,search_entry)
			if write_file_name not in uniprot_list:
				uniprot_list.append(write_file_name)
		uniprot_list.sort()
		write_variable('uniprot_list',uniprot_list)
		
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,variable.variable_dic['uniprot_x'],variable.variable_dic['uniprot_y'])
		
		df_list = ['MOL_Merge_GE_SILAC_Diff_fer_log2_ratio','UA_Merge_GE_SILAC_Diff_fer_slope_z_score','MOl_LFQ_S1_S2_S3_H9_H10_intensity']
		
		for df_name in df_list:
			write_variable('table_name','df.%s' %(df_name.replace('\n','')))
			for search_entry in search_list:
				new_entry = '%s.%s' %(file_name,search_entry)

				write_variable('uniprot_list_entry',new_entry)
				print variable.variable_dic['uniprot_list_entry_line_str']
				R_file_name ='00_barplot_selection.R'
				key_list = ['uniprot_list_entry_line_str']
				knitr_list('Tools',R_file_name,key_list)

	
	def Load_List(self):
		base_dir = variable.variable_dic['base_dir']
		file_path = Get_File_name_path(base_dir)
		print file_path
		add_list = ['file_path = "%s"\n' %(file_path)]
		tools('Tools','Load_List.R',add_list)
		file_name_list = file_path.split('/')
		print(file_name_list)
		file_name = file_name_list[len(file_name_list)-1]
		print(file_name)
		uniprot_list = variable.variable_dic['uniprot_list']
		if file_name not in uniprot_list:
			uniprot_list.append(file_name)
			uniprot_list.sort()
			write_variable('uniprot_list',uniprot_list)
		
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,variable.variable_dic['uniprot_x'],variable.variable_dic['uniprot_y'])
		#command=lambda R_file_name ='Load_Housekeeping_Genes.R', add_list = [variable.variable_dic['uniprot_list_line']]: tools(self.R_dir,R_file_name,add_list)).pack(side=LEFT)
		
	def table_list(self):
		table_name = variable.variable_dic['table_name']
		list_name = table_name.replace('df.','')
		uniprot_list = variable.variable_dic['uniprot_list']
		if list_name not in uniprot_list:
			uniprot_list.append(list_name)
			uniprot_list.sort()
			write_variable('uniprot_list',uniprot_list)
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,variable.variable_dic['uniprot_x'],variable.variable_dic['uniprot_y'])
		tools('Tools','Table_List.R')
		
	def reload_df(self):
		print 'Custom'
		file_list = os.listdir(dirname)
		print file_list
		file_name_path = Get_File_name_path(dirname)
		print(file_name_path)
		
		file_name_list = file_name_path.split('/')
		file_name = file_name_list[len(file_name_list)-1]
		print(file_name_path)
		file_path = '/'.join(file_name_list[0:len(file_name_list)-1])
		load_file_list = variable.variable_dic['file_list']
		print(load_file_list)
		load_file_list.append(file_name)
		new_load_file_list = list(set(load_file_list))
		new_load_file_list.sort()
		print(new_load_file_list)
		write_variable('file_list',new_load_file_list)
		variable_list = 'file_list'
		
		x_position = variable.variable_dic['file_list_x']
		y_position = variable.variable_dic['file_list_y']
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')
		
		cmd = 'cp %s %s' %(file_name_path,dirname)
		print(cmd)
		os.system(cmd)
		
		R_dir = 'Tools'
		R_file = 'simple_load.R'
		add_list = ["file_list = c('%s')\n" %(file_name),"file_path = '%s'\n" %(file_path)]
		tools(R_dir,R_file,add_list)
		
	def reload_df_path(self):
		print 'Custom'
		file_list = os.listdir(dirname)
		print file_list
		file_path = Get_Path(dirname)
		print(file_path)
		file_path_list = os.listdir(file_path)
		for file_name_entry in file_path_list:
			file_name_path = '%s/%s' %(file_path,file_name_entry)
			file_name_list = file_name_path.split('/')
			file_name = file_name_list[len(file_name_list)-1]
			print(file_name_path)
			file_path = '/'.join(file_name_list[0:len(file_name_list)-1])
			load_file_list = variable.variable_dic['file_list']
			print(load_file_list)
			load_file_list.append(file_name)
			new_load_file_list = list(set(load_file_list))
			new_load_file_list.sort()
			print(new_load_file_list)
			write_variable('file_list',new_load_file_list)
			variable_list = 'file_list'
			
			x_position = variable.variable_dic['file_list_x']
			y_position = variable.variable_dic['file_list_y']
			mylist = variable.variable_dic[variable_list]
			test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')
			
			cmd = 'cp %s %s' %(file_name_path,dirname)
			print(cmd)
			os.system(cmd)
			
			R_dir = 'Tools'
			R_file = 'simple_load.R'
			add_list = ["file_list = c('%s')\n" %(file_name),"file_path = '%s'\n" %(file_path)]
			tools(R_dir,R_file,add_list)
		
	def save_df(self):
		R_dir = "Tools"
		R_file = 'save_df.R'
		tools(R_dir,R_file)

	def Load_IPA(self):
		print 'Custom'
		file_list = os.listdir(dirname)
		print file_list
		file_name_path = Get_File_name_path(dirname)
		print(file_name_path)
		read_file = open(file_name_path,'r')
		read_list = read_file.readlines()
		read_file.close()
		heading_list = read_list[0].split('\t')
		print(heading_list)
		#List_App(root,'heading_list',heading_list)
		print(file_name_path)
		file_name_list = file_name_path.split('/')
		file_name = file_name_list[len(file_name_list)-1]
		print(file_name_path)
		file_path = '/'.join(file_name_list[0:len(file_name_list)-1])
		load_file_list = variable.variable_dic['file_list']
		print(load_file_list)
		#load_file_list = ['_','df.CP_heatmap_Gene_Expression_vs_SILAC_log_neg_p_value.txt','df.CP_heatmap_Gene_Expression_vs_SILAC_z_score.txt','df.CP_slope_inv_r_squared.txt','df.CP_slope_inv_r_squared_0_2.txt','df.Chart_Gene_Expression_Jignesh_2012_NS_NES.txt','df.H10_12.txt','df.H10_12_Export.txt','df.H9_08.txt','df.MOL_S1_S2_S3_H9_H10_log_2_ratio','df.Merge_CP_NSC_Diff','df.Merge_CP_all','df.Merge_CP_all.rm','df.Merge_NSC_Diff_CP_z_score','df.Mol_Gene_Expression_vs_SILAC_log_ratio.txt','df.Mol_comp_SILAC_FULL_140822.txt','df.NSC_DIff_CP_H10_rmrm_fer_heatmap.txt','df.NSC_Diff_CP_H9_fer_heatmap.txt','df.NSC_Diff_CP_H9_fer_heatmap_nlog_p_value.txt','df.NSC_Diff_CP_S1_fer_heatmap.txt','df.NSC_Diff_CP_S2_fer_heatmap.txt','df.NSC_Diff_CP_S3_fer_heatmap.txt','df.NSC_Diff_CP_slope_inv_r_squared.txt','df.NSC_Diff_CP_slope_inv_r_squared_0_2.txt','df.NSC_Diff_DF_S3_fer_edited.txt','df.NSC_Diff_Mol_H10_rmrm_fer_comp.txt','df.NSC_Diff_Mol_H9_fer_comp.txt','df.NSC_Diff_Mol_S1_fer_comp.txt','df.NSC_Diff_Mol_S2_fer_comp.txt','df.NSC_Diff_Mol_S3_fer_comp.txt','df.NSC_Diff_slope_no_cutoff.txt','df.NSC_Diff_slope_no_cutoffs.txt','df.S1_12.txt','df.S2_12.txt','df.S3_12.txt','df.Upstream_heatmap_140822_SILAC_Full_1sd_z_score.txt','df.Upstream_heatmap_Gene_Expression_vs_SILAC_mean_rm_z_score.txt','df.heatmap_SILAC_Full_140822_1sd_z_score.txt','df.upstream_slope_no_cutoff_heatmap.txt']
		#new_load_file_list = []
		#for entry in load_file_list:
			#new_load_file_list.append(entry.replace('df.',''))
		#load_file_list = new_load_file_list
			
		load_file_list.append(file_name)
		#print(load_file_list)
		#raw_input()
		new_load_file_list = list(set(load_file_list))
		#print(new_load_file_list)
		new_load_file_list.sort()
		print(new_load_file_list)
		#raw_input()
		write_variable('file_list',new_load_file_list)
		variable_list = 'file_list'
		
		#sys.path.insert(1, dirname+'/rscripts')
		#import variable
		#reload(variable)
		
		x_position = variable.variable_dic['file_list_x']
		y_position = variable.variable_dic['file_list_y']
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')
		
		#self.Initialise()
		cmd = 'cp %s %s' %(file_name_path,dirname)
		print(cmd)
		os.system(cmd)
		
		R_dir = '01_Load_Data'
		R_file = '01_Load_IPA.R'
		add_list = ["file_list = c('%s')\n" %(file_name),"file_path = '%s'\n" %(file_path)]
		tools(R_dir,R_file,add_list)
		
		R_dir = '01_Load_Data'
		R_file = 'rowname.R'
		write_variable('table_name',"df."+file_name)
		tools_list(R_dir,R_file)
		
		#R_file_list = ['01_Load.R']
		#for R_file_name in R_file_list:
		#	if variable.variable_dic['edit_list_entry'] == 'rstudio':
		#		cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
		#	else:
		#		cmd = 'Rscript %s/rscripts/%s' %(dirname,R_file_name)
		#	print(cmd)
		#	os.system(cmd)
		#	status('Load')
		
		
		#for R_file_name in R_file_list:
			#if variable.variable_dic['edit_list_entry'] == 'rstudio':
				#cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
			#else:
				#cmd = 'Rscript %s/rscripts/%s' %(dirname,R_file_name)
			#print(cmd)
			#os.system(cmd)
			#status('Load')
	
	def merge_df(self):
		print 'merge'
        
        
		os.system('mkdir %s/input' %(dirname))
		
		R_file_name='info.R'
		R_dir = 'Tools'
		tools(R_dir,R_file_name)
		
		read_file = open('%s/input/data.txt' %(dirname),'r')
		read_list = read_file.readlines()
		read_file.close()
		
		write_list = ['# delete all data frames except those to be merged\n']
		write_list = write_list+read_list
		write_list.append('\n#new_table_name : ')
		write_file = open('%s/input/merge.txt' %(dirname),'w')
		write_file.writelines(write_list)
		write_file.close()
		
		open_file_path = '%s/input/merge.txt' %(dirname)
		Open_File(open_file_path)
		
		read_file = open('%s' %(open_file_path))
		read_list = read_file.readlines()
		read_file.close()
		
		
        
		new_entry_list = []
		for entry in read_list:
			if entry[0] != '#':
				new_entry_list.append(entry.replace('\n',''))
			if '#new_table_name : ' in entry:
				new_table_name = entry.replace('#new_table_name : ','').replace('#new_table_name :','').replace('\n','')
		if len(new_table_name) != '':
			write_variable('merge_list_entry_line','merge_list = c("%s")\n' %('","'.join(new_entry_list)))
			write_variable('new_table_name_line','new_table_name = "%s"\n' %(new_table_name))
			R_file_name ='generic_merge.R'
			key_list = ['set_number_line','table_name_line','data_line_str','merge_list_entry_line','new_table_name_line','sd_cutoff_line','t_test_list_entry_line_str']
			knitr_list('custom',R_file_name,key_list)
		
			load_file_list = variable.variable_dic['file_list']
			print(load_file_list)
			load_file_list.append(new_table_name)
			print(load_file_list)
			load_file_list = list(set(load_file_list))
			load_file_list.sort()
			write_variable('file_list',load_file_list)
	
	def rename_df(self):
		print('rename')
        
        
		os.system('mkdir %s/input' %(dirname))
		
		table_name = variable.variable_dic['table_name']
		write_list = [table_name]
		write_file = open('%s/input/rename.txt' %(dirname),'w')
		write_file.writelines(write_list)
		write_file.close()
		
		#os.system('kate %s/input/rename.txt' %(dirname))
				
		open_file_path = '%s/input/rename.txt' %(dirname)
		Open_File(open_file_path)
		
		read_file = open('%s' %(open_file_path))
		
		#read_file = open('%s/input/rename.txt' %(dirname))
		read_list = read_file.readlines()
		read_file.close()
		
		#print(read_list)
		new_table_name = read_list[0]
		#print(new_table_name)
		write_variable('new_table_name',new_table_name)
		#raw_input()
		load_file_list = variable.variable_dic['file_list']
		#print(load_file_list)
		hit = 0
		for i in range(len(load_file_list)):
			if load_file_list[i] == table_name.replace('df.',''):
				load_file_list[i] = new_table_name.replace('df.','')
				hit = 1
		if hit == 0:
			load_file_list.append(new_table_name.replace('df.',''))
		load_file_list = list(set(load_file_list))
		load_file_list.sort()
		#print(load_file_list)	
		#raw_input()
		
		R_file_name='df_name.R'
		R_dir = 'Tools'
		tools_list(R_dir,R_file_name,['new_table_name_line_str'])

		write_variable('file_list',load_file_list)
		variable_list = 'file_list'
		x_position = variable.variable_dic['file_list_x']
		y_position = variable.variable_dic['file_list_y']
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')
			
	def rename_and_remove_subs(self):
		print('rename')
        
        
		os.system('mkdir %s/input' %(dirname))
		
		table_name = variable.variable_dic['table_name']
		write_list = [table_name]
		write_file = open('%s/input/rename.txt' %(dirname),'w')
		write_file.writelines(write_list)
		write_file.close()
		
		#os.system('kate %s/input/rename.txt' %(dirname))
		#read_file = open('%s/input/rename.txt' %(dirname))
		
				
		open_file_path = '%s/input/rename.txt' %(dirname)
		Open_File(open_file_path)
		
		read_file = open('%s' %(open_file_path))
		
		read_list = read_file.readlines()
		read_file.close()
		
		#print(read_list)
		new_table_name = read_list[0]
		#print(new_table_name)
		write_variable('new_table_name',new_table_name)
		#raw_input()
		load_file_list = variable.variable_dic['file_list']
		#print(load_file_list)
		hit = 0
		for i in range(len(load_file_list)):
			if new_table_name.replace('df.','') in load_file_list[i]:
				load_file_list[i] = new_table_name.replace('df.','')
				hit = 1
		if hit == 0:
			load_file_list.append(new_table_name.replace('df.',''))
		load_file_list = list(set(load_file_list))
		load_file_list.sort()
		#print(load_file_list)	
		#raw_input()
		
		R_file_name='df_rename_remove.R'
		R_dir = 'Tools'
		tools_list(R_dir,R_file_name,['new_table_name_line_str'])
		write_variable('file_list',load_file_list)
		variable_list = 'file_list'
		x_position = variable.variable_dic['file_list_x']
		y_position = variable.variable_dic['file_list_y']
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')
		
	def remove_subs(self):
		table_name = variable.variable_dic['table_name']
		load_file_list = variable.variable_dic['file_list']
		#print(load_file_list)
		hit = 0
		for i in range(len(load_file_list)):
			if table_name.replace('df.','') in load_file_list[i]:
				load_file_list[i] = "_"
				hit = 1
		load_file_list = list(set(load_file_list))
		load_file_list.sort()
		#print(load_file_list)	
		#raw_input()
		
		R_file_name='df_remove.R'
		R_dir = 'Tools'
		tools_list(R_dir,R_file_name)
		variable_list = 'file_list'
		write_variable('file_list',load_file_list)
		x_position = variable.variable_dic['file_list_x']
		y_position = variable.variable_dic['file_list_y']
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')
		print 'rm(%s) and its subsidiaries' %(table_name)

		
		
class R_Tools_App:
	def __init__(self,master):
		
		main_tool_frame = LabelFrame(master,text="")
		main_tool_frame.place(y=400,x=5)
		
		normalise_frame = LabelFrame(main_tool_frame,text="Transform Data")
		normalise_frame.pack(side=TOP,anchor = W)
		
		result_frame = LabelFrame(master,text="Result")
		result_frame.place(y=400,x=680)
		
		output_frame = LabelFrame(master,text="Output")
		output_frame.place(y=600,x=800)
		
		stat_frame = LabelFrame(main_tool_frame,text="Statistics (data.stat)")
		stat_frame.pack(side=TOP,anchor = W)
		
		variable_list = 'cutoff_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,680,445,'no')
		
		
		view_frame = LabelFrame(main_tool_frame,text="View")
		view_frame.pack(side=TOP,anchor=W)
		
		paired_frame = LabelFrame(main_tool_frame,text="Paired")
		paired_frame.pack(side=TOP,anchor = W)
		
		
		test_frame = LabelFrame(main_tool_frame,text="Test")
		test_frame.pack(side=TOP,anchor=W)
		
		custom_frame = LabelFrame(main_tool_frame,text="Custom")
		custom_frame.pack(side=TOP,anchor=W)
		
		workflow_frame = LabelFrame(main_tool_frame,text="Workflow")
		workflow_frame.pack(side=TOP,anchor=W)
		
		
		#scrollbar = Scrollbar(master)
		#scrollbar.pack(side=RIGHT, fill=Y)
		#listbox = Frame(main_tool_frame, yscrollcommand=scrollbar.set)
		#for i in range(1000):
			#listbox.insert(END, str(i))
		#listbox.pack(side=LEFT, fill=BOTH)

		#scrollbar.config(command=listbox.yview)
		self.R_dir = 'Tools'
		
		
		
		self.hi_there = Button(normalise_frame, text="Remove Zero's\nna,na.z,z2na", command=lambda R_file_name='Normalisation.R': tools(self.R_dir,R_file_name)).pack(side=LEFT)
		self.hi_there = Button(stat_frame, text="mean,var,sd,cv\nmin,max,count", command=lambda R_file_name='stat.R': tools(self.R_dir,R_file_name)).pack(side=LEFT)
		#self.hi_there = Button(stat_frame, text="t test\nln", command=lambda R_file_name ='stat_2.R': tools(self.R_dir,R_file_name)).pack(side=LEFT)
		self.hi_there = Button(stat_frame, text="T tests\nLinear Regression", command=lambda R_file_name ='t_test_sd.R', key_list = ['table_name_line','sd_cutoff_line','var_cutoff_line','design_list_line_int']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		self.hi_there = Button(normalise_frame, text="Normalise\nqn,mean,median,sum", command=lambda R_file_name='Normalisation_mean.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)
		
		experimental_design_list = Button(paired_frame, text="Experiment\nDesign", command=Experimental_Design)
		experimental_design_list.pack(side=LEFT)
		
		self.hi_there = Button(paired_frame, text="Paired Ratio", command=lambda R_file_name='Paired_Ratio.R', key_list = ['selected_quant_list_entry','condition_1_line','condition_2_line','condition_list_line','table_name_line','data_line_str']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(paired_frame, text="T test", command=lambda R_file_name='Paired_T_test.R', key_list = ['selected_quant_list_entry','condition_1_line','condition_2_line','condition_list_line','table_name_line','data_line_str']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(paired_frame, text="Multiple\nTesting\nCorrection", command=lambda R_file_name='Paired_multi_t_correction.R', key_list = ['selected_quant_list_entry','condition_1_line','condition_2_line','condition_list_line','t_test_list_entry_line_str','table_name_line','stat_table_name_line','sd_cutoff_line','data_line_str','selected_selected_result_data_list_entry']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=RIGHT,anchor=E)
		self.hi_there = Button(paired_frame, text="Result Multiple\nTesting\nCorrection", command=lambda R_file_name='Result_multi_t_correction.R', key_list = ['selected_quant_list_entry','condition_1_line','condition_2_line','condition_list_line','t_test_list_entry_line_str','table_name_line','stat_table_name_line','sd_cutoff_line','data_line_str','selected_selected_result_data_list_entry']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=RIGHT,anchor=E)

		#self.hi_there = Button(paired_frame, text="Paired\nSelect", command=lambda R_file_name='Paired_Select.R', key_list = ['table_name_line','data_line_str','t_test_list_entry_line_str','sd_cutoff_line','t_cutoff_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		#self.hi_there = Button(paired_frame, text="Paired\nWrite", command=lambda R_file_name='Paired_Write.R', key_list = ['table_name_line','data_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str']: tools_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		#self.hi_there = Button(paired_frame, text="Paired\nView\nFinal", command=lambda R_file_name='Paired_View_Final.R', key_list = ['table_name_line','data_line_str','condition_1_line','condition_2_line','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str','set_number_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		
		
		self.hi_there = Button(view_frame, text="Generate\nReport", command=lambda R_file_name ='knitr_Report.R': knitr_list(self.R_dir,R_file_name)).pack(side=LEFT)
		
		#self.hi_there = Button(view_frame, text="View\nReport", command=lambda R_file_name ='knitr_Report.R': view(self.R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Plot", command=lambda R_file_name='plot.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Boxplot", command=lambda R_file_name='boxplot.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text="Heatmap", command=lambda R_file_name='heatmaps.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text='PCA', command=lambda R_file_name='PCA.R',key_list = ['table_name_line','class_list_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)

		self.hi_there = Button(view_frame, text='MatPlot', command=lambda R_file_name='matplot.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text='Correlation', command=lambda R_file_name='correlation.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text='Variance', command=lambda R_file_name='variance.R',key_list = ['table_name_line','sd_cutoff_line','var_cutoff_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)

		
		self.hi_there = Button(view_frame, text='Histogram', command=lambda R_file_name='histogram.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)

		
		self.hi_there = Button(view_frame, text="FDR", command=lambda R_file_name ='fdr.R',key_list = ['table_name_line','selected_t_test_list_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		self.hi_there = Button(view_frame, text="Volcano\nPlot", command=lambda R_file_name ='volcano.R',key_list = ['table_name_line','selected_t_test_list_line','sd_cutoff_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text='SILAC QC\nGlobal', command=lambda R_file_name='SILAC_Global_QC.R': knitr(self.R_dir,R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(view_frame, text='SILAC QC\nSample', command=lambda R_file_name='SILAC_Sample_QC.R': knitr(self.R_dir,R_file_name)).pack(side=BOTTOM)

		self.hi_there = Button(normalise_frame, text="log2", command=lambda R_file_name='log2.R': tools(self.R_dir,R_file_name)).pack(side=LEFT)
		


		
		#self.hi_there = Button(view_frame, text="cmeans", command=lambda i='cmeans.R': self.rstudio(i)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="cmeans", command=lambda R_file_name ='cmeans.R', key_list = ['set_number_line','table_name_line','data_line_str'] : knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="cmeans\nselection", command=lambda R_file_name ='cmeans_selection.R', key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line'] : knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="matplot\nselection", command=lambda R_file_name ='matplot_selection.R', key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line','sd_cutoff_line','t_test_list_entry_line_str'] : knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		
		
		self.hi_there = Button(test_frame, text="matplot\nsectional", command=lambda R_file_name ='matplot_sectional.R', key_list = ['set_number_line','table_name_line','data_line_str'] : knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="matplot\nsectional\nselection", command=lambda R_file_name ='matplot_sectional_selection.R', key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line'] : knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="barplot", command=self.barplot).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="barplot\nselection", command=self.barplot_selection).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="barplot\nselect all", command=self.barplot_select_all).pack(side=LEFT)
		
		self.hi_there = Button(test_frame, text="cmeans\nhouskeeping\nnormalise", command=lambda R_file_name='cmeans_housekeeping_normalise.R',key_list = ['uniprot_list_entry_line','set_number_line','table_name_line','data_line_str']: self.knitr_list(R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(normalise_frame, text="housekeeping gene\nnormalise", command=lambda R_file_name ='matplot_housekeeping.R',key_list = ['uniprot_list_entry_line','set_number_line','table_name_line','data_line_str']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(normalise_frame, text="timecourse\nratio's", command=lambda R_file_name ='timecourse_ratios.R',key_list = ['table_name_line','data_line_str']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)

		self.hi_there = Button(stat_frame, text="Prot2Pep", fg='blue', command=self.prot2pep).pack(side=LEFT)
		
		self.hi_there = Button(stat_frame, text="Search\nPeptides", fg='blue', command=lambda R_file_name ='search_peptides.R',key_list = ['uniprot_list_entry_line','table_name_line','data_line_str']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		self.hi_there = Button(stat_frame, text="Search\nProteins", fg='blue', command=lambda R_file_name ='search_proteins.R',key_list = ['uniprot_list_entry_line','table_name_line','data_line_str']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		self.hi_there = Button(stat_frame, text="Detailed\nSummary", fg='green', command=lambda R_file_name ='Detailed_Summary.R',key_list = ['table_name_line','data_line_str']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)

		
		self.hi_there = Button(result_frame, text="Simple\nSelect", command=lambda R_file_name='Simple_Select.R', key_list = ['table_name_line','stat_table_name_line','data_line_str','t_test_list_entry_line_str','sd_cutoff_line','t_cutoff_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)

		self.hi_there = Button(result_frame, text="Select", command=lambda R_file_name='Select.R', key_list = ['table_name_line','stat_table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(result_frame, text="View\nResult", command=lambda R_file_name='View_Final.R', key_list = ['table_name_line','data_line_str','sample_name_list_R_line','sd_cutoff_line','t_cutoff_line','t_test_list_entry_line_str','selected_result_data_list_entry_line_str','set_number_line']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(result_frame, text="View_lm", command=lambda R_file_name='View_lm.R', key_list = ['table_name_line','data_line_str','sample_name_list_R_line','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str','set_number_line','design_list_line_int']: knitr_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(result_frame, text="MQ Write", command=lambda R_file_name='Write.R', key_list = ['table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str']: tools_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.hi_there = Button(result_frame, text="Custom\nWrite", command=lambda R_file_name='custom_write.R', key_list = ['table_name_line','data_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str','uniprot_list_entry_line_str']: tools_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)
		self.hi_there = Button(result_frame, text="Generic\nWrite", command=lambda R_file_name='Generic_Write.R', key_list = ['table_name_line','data_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str','original_table_line']: tools_list(self.R_dir,R_file_name,key_list)).pack(side=LEFT)

		self.hi_there = Button(output_frame, text="tar results", command=self.tar).pack(side=LEFT)
		
		self.hi_there = Button(output_frame, text="tar 2015 IPA", command=self.IPA_2015).pack(side=LEFT)
		
		#self.hi_there = Button(output_frame, text="dropbox\nupload", command=self.dropbox_upload).pack(side=LEFT)
		
		#self.hi_there = Button(stat_frame, text="diff", command=lambda R_file_name ='diff.R': tools(self.R_dir,R_file_name)).pack(side=LEFT)
		
		#self.hi_there = Button(stat_frame, text="first ratio", command=lambda R_file_name ='first_ratio.R': tools(self.R_dir,R_file_name)).pack(side=LEFT)
		
		#self.hi_there = Button(test_frame, text="Housekeeping Genes", command=lambda R_file_name ='Load_Housekeeping_Genes.R', add_list = [variable.variable_dic['uniprot_list_line']]: tools(self.R_dir,R_file_name,add_list)).pack(side=LEFT)
		
		#self.hi_there = Button(view_frame, text="close", command=self.close).pack(side=LEFT)
		
		self.hi_there = Button(workflow_frame, text="LFQ", command=self.workflow_LFQ).pack(side=LEFT)
		
		self.hi_there = Button(workflow_frame, text="Stat-Report", command=self.workflow_stat_report).pack(side=LEFT)
		
		self.hi_there = Button(workflow_frame, text="first_element_ratio - Report", command=self.workflow_FER).pack(side=LEFT)
		
		self.hi_there = Button(workflow_frame, text="Select - slope", command=self.workflow_slope).pack(side=LEFT)
		
		self.hi_there = Button(workflow_frame, text="barplot all", command=self.workflow_barplot_all).pack(side=LEFT)

		self.hi_there = Button(workflow_frame, text="barplot all\nselect", command=self.workflow_barplot_select_all).pack(side=LEFT)
		
		self.hi_there = Button(workflow_frame, text="barplot MOL_UA", command=self.workflow_barplot_mol_ua).pack(side=LEFT)
		
		self.custom_dir = 'new_tools'
		custom_file_list = os.listdir('%s/%s' %(python_path,'new_tools'))
		for custom_file_name in custom_file_list:
			if '.R' in custom_file_name:
				self.hi_there = Button(custom_frame, text=custom_file_name, command=lambda R_file_name =custom_file_name: knitr_list(self.custom_dir,R_file_name)).pack(side=LEFT)
	
	def prot2pep(self):
		R_file_name ='peptides4proteins.R'
		key_list = ['uniprot_list_entry_line_str','table_name_line','data_line_str']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		uniprot_list = variable.variable_dic['uniprot_list']
		list_name = variable.variable_dic['uniprot_list_entry']
		print uniprot_list
		print list_name
		uniprot_list.append(list_name+'.pep')
		print uniprot_list
		#raw_input()
		uniprot_list.sort()
		write_variable('uniprot_list',uniprot_list)
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,variable.variable_dic['uniprot_x'],variable.variable_dic['uniprot_y'])
	
	def barplot(self):
		R_file_name ='00_barplot.R'
		#key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line_str','sd_cutoff_line','t_test_list_entry_line_str']
		knitr_list('Tools',R_file_name)
	
	def workflow_barplot_select_all(self):
		print 'workflow'
		
		file_list = variable.variable_dic['file_list']
		write_list = []
		print(file_list)
		#raw_input()
		for file_name in file_list:
			print file_name
			if file_name != '_' or file_name != '':
				write_list.append(file_name+'\n')
		write_file = open('%s/input/df_list.txt' %(dirname),'w')
		write_file.writelines(write_list)
		write_file.close()
		
		#os.system('kate %s/input/df_list.txt' %(dirname))
		#read_file = open('%s/input/df_list.txt' %(dirname))
				
		open_file_path = '%s/input/df_list.txt' %(dirname)
		Open_File(open_file_path)
		
		read_file = open('%s' %(open_file_path))
		file_list = read_file.readlines()
		read_file.close()
		write_list = []
		uniprot_list = variable.variable_dic['uniprot_list']
		for file_name in uniprot_list:
			print file_name
			if file_name != '_' or file_name != '':
				write_list.append(file_name+'\n')
		write_file = open('%s/input/uniprot_list.txt' %(dirname),'w')
		write_file.writelines(write_list)
		write_file.close()
		
		#os.system('kate %s/input/uniprot_list.txt' %(dirname))
		#read_file = open('%s/input/uniprot_list.txt' %(dirname))
		
				
		open_file_path = '%s/input/uniprot_list.txt' %(dirname)
		Open_File(open_file_path)
		
		read_file = open('%s' %(open_file_path))
		uniprot_list = read_file.readlines()
		read_file.close()
		
		
		
		for file_name in file_list:
			write_variable('table_name','df.%s' %(file_name.replace('\n','')))
			for uniprot_entry in uniprot_list:
				print(file_name)
				
				write_variable('uniprot_list_entry',uniprot_entry.replace('\n',''))
				#write_variable('table_name_line_str','table_name = "df.%s"\n' %(file_name.replace('\n','')))
				print variable.variable_dic['table_name_line_str']
				R_file_name ='00_barplot_selection.R'
				key_list = ['uniprot_list_entry_line_str']
				knitr_list('Tools',R_file_name,key_list)
				#raw_input()
	
	def workflow_barplot_mol_ua(self):
		
		file_list = ['MOL_GE_SILAC_Full_Diff_complete_FER_log_ratio','MOL_Diff_complete_LFQ_intensity','UA_Merge_GE_SILAC_Diff_fer_slope_z_score']
		
		for file_name in file_list:
			write_variable('table_name','df.%s' %(file_name.replace('\n','')))
			R_file_name ='00_barplot_selection.R'
			key_list = ['uniprot_list_entry_line_str']
			knitr_list('Tools',R_file_name,key_list)
			entry = variable.variable_dic['uniprot_list_entry_line_str']

		
	
	def workflow_barplot_all(self):
		print 'workflow'
		
		file_list = variable.variable_dic['file_list']
		write_list = []
		print(file_list)
		#raw_input()
		for file_name in file_list:
			print file_name
			if file_name != '_' or file_name != '':
				write_list.append(file_name+'\n')
		write_file = open('%s/input/df_list.txt' %(dirname),'w')
		write_file.writelines(write_list)
		write_file.close()
		
		#os.system('kate %s/input/df_list.txt' %(dirname))
		#read_file = open('%s/input/df_list.txt' %(dirname))
		
				
		open_file_path = '%s/input/df_list.txt' %(dirname)
		Open_File(open_file_path)
		
		read_file = open('%s' %(open_file_path))
		read_list = read_file.readlines()
		read_file.close()
		
		
		for file_name in read_list:
			print(file_name)
			write_variable('table_name','df.%s' %(file_name.replace('\n','')))
			#write_variable('table_name_line_str','table_name = "df.%s"\n' %(file_name.replace('\n','')))
			print variable.variable_dic['table_name_line_str']
			R_file_name ='00_barplot.R'
			#key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line_str','sd_cutoff_line','t_test_list_entry_line_str']
			knitr_list('Tools',R_file_name)
			#raw_input()
	
	
	def barplot_select_all(self):
		R_file_name ='00_barplot_select_all.R'
		key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line_str','sd_cutoff_line','t_test_list_entry_line_str']
		knitr_list('Tools',R_file_name,key_list)
	
	def barplot_selection(self):
		R_file_name ='00_barplot_selection.R'
		key_list = ['set_number_line','table_name_line','data_line_str','uniprot_list_entry_line_str','sd_cutoff_line','t_test_list_entry_line_str']
		knitr_list('Tools',R_file_name,key_list)
		
	
	def workflow_LFQ(self):
		Load()
		
		write_variable('table_name','df.proteinGroups.txt.edited')
		R_file_name='Extract_ALL_Label_Free_Quantiation_Values.R'
		add_list = [variable.variable_dic['quant_list_line']]
		knitr('Tools',R_file_name,add_list)
		
		write_variable('table_name',"df.proteinGroups.txt.edited.LFQ.intensity")
		R_file_name='Normalisation.R'
		knitr(self.R_dir,R_file_name)
		
		write_variable('table_name',"df.proteinGroups.txt.edited.LFQ.intensity.z2na")
		workflow_stat_report(self)
		
	
	def workflow_stat_report(self):
		R_file_name='stat.R'
		knitr(self.R_dir,R_file_name)

		R_file_name ='t_test_sd.R'
		key_list = ['table_name_line','sd_cutoff_line','var_cutoff_line','design_list_line_int']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		R_file_name ='knitr_Report.R'
		knitr_list(self.R_dir,R_file_name)
		
		tar()
		
	def workflow_FER(self):
		#set table name : could be changes to start with current table name 
		write_variable('table_name',"df.proteinGroups.txt.edited.LFQ.intensity.z2na")
		
		# run timecourse ratios
		R_file_name ='timecourse_ratios.R'
		key_list = ['table_name_line','data_line_str']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		#set table name
		write_variable('table_name',"df.proteinGroups.txt.edited.LFQ.intensity.z2na.first_element_ratio")
		
		# log sample
		R_file_name='log2.R'
		knitr(self.R_dir,R_file_name)
		
		write_variable('table_name',"df.proteinGroups.txt.edited.LFQ.intensity.z2na.first_element_ratio.log2")
		experiment_list = variable.variable_dic['quant_sample_name_list']
		print experiment_list
		first_sample = [experiment_list[0]]
		print first_sample
		write_variable('selected_reverse_sample_list',first_sample)
		
		R_dir = 'Tools'
		R_file_name ='remove_column.R'
		key_list = ['selected_quant_list_line','selected_reverse_sample_list_line']
		knitr_list(R_dir,R_file_name,key_list)
		
		
		write_variable('table_name',"df.proteinGroups.txt.edited.LFQ.intensity.z2na.first_element_ratio.log2.rm")
		R_file_name='Normalisation.R'
		knitr(self.R_dir,R_file_name)
		
		write_variable('table_name',"df.proteinGroups.txt.edited.LFQ.intensity.z2na.first_element_ratio.log2.rm.z2na")
		
		R_file_name='stat.R'
		knitr(self.R_dir,R_file_name)
		
		R_file_name ='t_test_sd.R'
		key_list = ['table_name_line','sd_cutoff_line','var_cutoff_line','design_list_line_int']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		R_file_name ='knitr_Report.R'
		knitr_list(self.R_dir,R_file_name)
		
		R_file_name='histogram.R'
		knitr(self.R_dir,R_file_name)
 
		tar()
	
	def workflow_slope(self):
		design()
		#raw_input()
		R_file_name='stat.R'
		knitr(self.R_dir,R_file_name)
		
		R_file_name ='t_test_sd.R'
		key_list = ['table_name_line','sd_cutoff_line','var_cutoff_line','design_list_line_int']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		R_file_name ='knitr_Report.R'
		knitr_list(self.R_dir,R_file_name)
		
		R_file_name='histogram.R'
		knitr(self.R_dir,R_file_name)
		
		
		R_file_name='Select.R'
		write_variable('stat_table_name','"%s.%s"' %(variable.variable_dic['table_name'],'stat'))
		write_variable('t_test_list_entry','r.squared')
		write_variable('cutoff_list_entry','slope')
		write_variable('sd_cutoff',0.25)
		write_variable('t_cutoff',0.8)
		key_list = ['table_name_line','stat_table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line']
		knitr_list(self.R_dir,R_file_name,key_list)
		#raw_input(variable.variable_dic['selected_result_data_list_entry_line_str'])

		
		write_variable('selected_result_data_list_entry','sd1_t')
		#raw_input(variable.variable_dic['selected_result_data_list_entry_line_str'])
		R_file_name='Write.R'
		key_list = ['table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		
		R_file_name='Select.R'
		write_variable('stat_table_name','"%s.%s"' %(variable.variable_dic['table_name'],'stat'))
		write_variable('t_test_list_entry','inv_r.squared')
		write_variable('cutoff_list_entry','slope')
		write_variable('sd_cutoff',0.25)
		write_variable('t_cutoff',0.2)
		key_list = ['table_name_line','stat_table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line']
		knitr_list(self.R_dir,R_file_name,key_list)
		##raw_input(variable.variable_dic['selected_result_data_list_entry_line_str'])
		
		write_variable('t_test_list_entry','inv_r.squared')
		write_variable('cutoff_list_entry','slope')
		write_variable('selected_result_data_list_entry','all')
		#raw_input(variable.variable_dic['selected_result_data_list_entry_line_str'])
		R_file_name='Write.R'
		key_list = ['table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str']
		knitr_list(self.R_dir,R_file_name,key_list)
		write_variable('selected_result_data_list_entry','all.na')
		
		
		#raw_input(variable.variable_dic['selected_result_data_list_entry_line_str'])
		R_file_name='Write.R'
		key_list = ['table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line','selected_result_data_list_entry_line_str']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		R_file_name='Select.R'
		write_variable('stat_table_name','"%s.%s"' %(variable.variable_dic['table_name'],'stat'))
		write_variable('t_test_list_entry','t_test')
		write_variable('cutoff_list_entry','mean')
		write_variable('sd_cutoff',0.5)
		write_variable('t_cutoff',0.05)
		key_list = ['table_name_line','stat_table_name_line','data_line_str','t_test_list_entry_line_str','cutoff_list_entry_line_str','sd_cutoff_line','t_cutoff_line']
		knitr_list(self.R_dir,R_file_name,key_list)
		
		tar()
 
 
 
		
	def tar(self):
		tar()
		
	def IPA_2015(self):
		print 'IPA'
		IPA_list = ['NSC_Diff','NSC_SILAC']
		for IPA_entry in IPA_list:
			ipa_path = '/MS_Experiments/2015/Neural_Stem_Cells/IPA/%s/' %(IPA_entry)
			file_list = os.listdir(ipa_path)
			if 'tar' not in file_list:
				os.system('mkdir %s/%s' %(ipa_path,'tar'))
			else:
				file_list.remove('tar')
			folder_list = []
			for file_name_entry in file_list:
				path = '%s/%s' %(ipa_path,file_name_entry)
				if os.path.isdir(path):
					folder_list.append(file_name_entry)
			folder_line_list = ' '.join(folder_list)
			cmd = "tar -C %s -cf %s/tar/%s_IPA.tar %s" %(ipa_path,ipa_path,IPA_entry,folder_line_list)
			print(cmd)
			os.system(cmd)
			print 'dropbox uploader'
			cmd = '%s/dropbox_uploader.sh upload %s/tar/%s_IPA.tar doctorate/Thesis/Thesis_Data/%s_IPA.tar' %(python_path,ipa_path,IPA_entry,IPA_entry)
			print cmd
			#raw_input()
			os.system(cmd)
		




class R_App:
	def __init__(self,master):
		#R_frame = Frame(master).pack()
		R_program_frame = LabelFrame(master,text='R Program',fg='white',bg=version_colour)
		R_program_frame.place(y=35,x=20)
		R_frame = LabelFrame(R_program_frame,text="",bg=version_colour)
		R_frame.pack(side=TOP,anchor=W)
		Load_frame = LabelFrame(R_frame,text='LOAD',bg=version_colour)
		Load_frame.pack(side=LEFT,anchor=W)
		R_frame_2 = LabelFrame(R_program_frame,text="Extract",bg=version_colour)
		R_frame_2.pack(side=TOP,anchor=W)
		SILAC_frame = LabelFrame(R_frame_2)
		SILAC_frame.pack(side=RIGHT,anchor=E)
		extract_frame = LabelFrame(R_frame_2)
		extract_frame.pack(side=RIGHT,anchor=W)

		R_frame_3 = LabelFrame(R_program_frame,text="",bg=version_colour)
		R_frame_3.pack(side=LEFT)
		
		variable_default('variability',300)
		self.number = Entry_App(SILAC_frame,'variability','side','BOTTOM','no')
		
		

		
		
		#run_line = 'please select and option'
		#running = StringVar()
		#Label(master, textvariable=running).pack()
		#runnint.set(run_line)
		#global running
		try:
			data_line = variable.variable_dic['table_name_line']
		except:
			write_variable('table_name_line','')
			data_line = ''
		data_line_message = StringVar()
		Label(root, textvariable=data_line_message,font = tkFont.Font(family='Arial',size=10,weight='bold'),fg='blue').pack()
		data_line_message.set(data_line)
		global data_line_message
		variable_default('status_entry','ready')
		new_status_line = StringVar()
		status_str = Label(root, textvariable=new_status_line,font = tkFont.Font(family='Arial',size=10,weight='bold'),fg='red')
		status_str.place(x=420,y=40)
		new_status()
		#status_line.set(status_entry)
		global new_status_line
		
		status_frame = Frame(master)
		status_frame.pack(side=BOTTOM)
		global status_frame
		
		v = StringVar()
		Label(status_frame, textvariable=v).pack()
		status_line = '\n'.join(status_list)
		global v
		#status(variable.variable_dic['status_entry'])
		#v.set(status_line)
		
		load_file_list = ['proteinGroups.txt','peptides.txt','parameters.txt','summary.txt']
		intensity_list = ['_','Intensity','LFQ.intensity','iBAQ',"Ratio.H.L","Ratio.H.L.normalized","Reporter",'Ratio.H.L.variability....']
		file_list = os.listdir(dirname)
		#raw_input(file_list)
		if('proteinGroups.txt' in file_list):
			f = open('%s/proteinGroups.txt' %(dirname), 'r')
			read_lines = f.readlines()
			f.close()
		#print read_lines[0]
			intensity_list = ['_','Intensity','LFQ.intensity','iBAQ',"Ratio.H.L","Ratio.H.L.normalized","Reporter",'Ratio.H.L.variability....']
			#raw_input('proteinGroup hit')
		else:
			load_file_list = ['_']
			intensity_list = ['_']
			
		#intensity_list = ['']
		#for intensity in expected_intensity_list:
		#	if intensity in read_lines[0]:
		#		intensity_list.append(intensity)
		write_variable('quant_list',intensity_list)
		#selected_file_list = ['proteinGroups.txt','parameters.txt','summary.txt']
		variable_default('file_list',load_file_list)
		#raw_input(variable.variable_dic['file_list'])
		write_variable('MQ_file_list',file_list)
		write_variable('python_path',python_path)
		suffix_list = ['_','na.z','z2na','c','300']
		write_variable('suffix_list',suffix_list)
		norm_list = ['_','qn','sum','mean','median','first_ratio','first_element_ratio','last_ratio','last_element_ratio','last_element_rev_ratio','mean_ratio','all_data_mean_ratio']
		write_variable('norm_list',norm_list)
		
		selected_result_data_list = ['_','all','all.na','sd1','sd2','t','sd1_t','sd2_t','conserved','selected_sim_1sd','selected_sim_2sd','selected_t','selected_sim_1sd_t','selected_sim_2sd_t','selected_conserved']
		
		write_variable('selected_result_data_list',selected_result_data_list)
		variable_default('selected_result_data_list_entry_line_str','selected_result_data_list_entry = "_"')
		variable_default('selected_result_data_list_line', 'selected_result_data_list = ""')
		t_test_list = ['t_test','t_test_mu0','t_test_mean','t_test_median','t_test_1sd_selection','t_test_0_5sd_selection','t_test_1sd_selection_ts','t_test_0_5sd_selection_ts','t_test_together','t_test_separate','t_test_separate_paired','t_test_separate_paired_two.sided','t_test_separate_ts','t_test_simple_ref_ts','genefilter_rowttests','fdr','bonferroni','BH','holm','hochberg','hommel','BY','r.squared','inv_r.squared']
		write_variable('t_test_list',t_test_list)
		cutoff_list = ['mean','slope']
		write_variable('cutoff_list',cutoff_list)
		variable_default('cutoff_list_entry','mean')
		
		write_variable('sample_edit_list',['_','edited','var','rm','var.rm'])
		write_variable('silac_list',['_','log2.rev'])
		variable_default('uniprot_list',['_'])
		#write_variable('uniprot_list',['_'])
		#if 'uniprot_list' not in variable.variable_dic.keys():
		#	write_variable('uniprot_list',['_'])
		result_list = ['_','stat','log2_ratio','log2_ratio.stat','log2_ratio.stat.p_adjust','condition_1','condition_2','all','selected_sim_1sd','selected_sim_2sd','selected_t','selected_sim_1sd_t','selected_sim_2sd_t','peptide_search','z_score']


		mod_list = ['_','log2','log2_ratio','comparison','replicate','different','similar','hc_mean','hc_median','all','sd_1','sd_2','t','sd_1_t','sd_2_t','peptide_search','protein_search','rm']
		full_mod_list = suffix_list+norm_list+mod_list+selected_result_data_list
		result_full_mod_list = full_mod_list+result_list
		#raw_input(full_mod_list)
		write_variable('mod_list',mod_list)
		write_variable('mod_list_2',full_mod_list)
		write_variable('mod_list_3',full_mod_list)
		write_variable('mod_list_4',full_mod_list)
		write_variable('mod_list_5',result_full_mod_list)
		
		write_variable('result_list',result_list)

		
		variable_default('edit_list_entry','generate')
		write_variable('edit_list_entry','generate')
		
		variable_default('uniprot_list_entry_line','_')
		
		write_variable('data','')
		
		
		y_start_position = 65
		write_variable('list_x_position',400)
		write_variable('list_y_position',y_start_position)
		
		edit_list = ['generate','rstudio','pdf','html']
		write_variable('edit_list',edit_list)
		
		#edit_buttons =  RadioButton_List_App(master,'edit_list',edit_list,950,10,write_x = 'no')
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		edit_buttons =  RadioButton_List_App(master,'edit_list',edit_list,x_position,y_position,'y')
		

		
		write_variable('list_x_position',500)
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'sample_edit_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_start_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'silac_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'result_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		

		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		write_variable('uniprot_x',x_position)
		write_variable('uniprot_y',y_position)
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		y_position = variable.variable_dic['list_y_position']
		#t_test_list = ['t_test','t_test_mean','t_test_median','t_test_1sd_selection','t_test_0_5sd_selection','t_test_1sd_selection_ts','t_test_0_5sd_selection_ts','t_test_together','t_test_separate','t_test_separate_paired','t_test_separate_paired_two.sided','t_test_separate_ts','t_test_simple_ref_ts','genefilter_rowttests','fdr','bonferroni','BH','holm','hochberg','hommel','BY']
		variable_list = 't_test_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'x')
		#test_list = SpinBox_List_App(root,'t_test_list',t_test_list,x_position,y_position,'y')
		



		
		write_variable('list_y_position',y_start_position)
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		#write_variable('file_list',load_file_list)
		variable_list = 'file_list'
		write_variable('file_list_x',x_position)
		write_variable('file_list_y',y_position)
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'quant_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		

		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'suffix_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		#y_position = 50
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'norm_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		#y_position = 50
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'mod_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'x')
		

		

		
				#uniprot_list = os.listdir('%s/info_files' %(python_path))
		#write_variable('uniprot_list',[])

		#x_position = variable.variable_dic['list_x_position']
		#y_position = 80
		#write_variable('uniprot_x',x_position)
		#write_variable('uniprot_y',y_start_position)
		
		write_variable('list_y_position',y_start_position)
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'mod_list_2'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'mod_list_3'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'mod_list_4'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		x_position = variable.variable_dic['list_x_position']
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'mod_list_5'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'y')
		
		y_position = variable.variable_dic['list_y_position']
		variable_list = 'selected_result_data_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'x')
		
		x_position = variable.variable_dic['list_x_position']+20
		
		#y_position = 80

				
		variable_default('set_number',25)
		self.number = Entry_App(root,'set_number',x_position,y_start_position,'y')
		y_position = variable.variable_dic['list_y_position']
		variable_default('sd_cutoff',1)
		self.number = Entry_App(root,'sd_cutoff',x_position,y_position,'y')
		variable_default('t_cutoff',0.05)
		y_position = variable.variable_dic['list_y_position']
		self.number = Entry_App(root,'t_cutoff',x_position,y_position,'y')
		variable_default('var_cutoff',0.05)
		y_position = variable.variable_dic['list_y_position']
		self.number = Entry_App(root,'var_cutoff',x_position,y_position)
		
		#close_button = Button(select_TK, fg='red',text="close", command=close).pack(side=TOP)
		
		#write_variable('result_list',['all','selected_sim_1sd','selected_sim_2sd','selected_t','selected_sim_1sd_t','selected_sim_2sd_t'])
		#x_position = variable.variable_dic['list_x_position']
		#y_position = variable.variable_dic['list_y_position']
		#variable_list = 'result_list'
		#mylist = variable.variable_dic[variable_list]
		#test_list = SpinBox_List_App(root,variable_list,mylist,x_position,100,'no')
		


		
		try:
			x_position = variable.variable_dic['list_x_position']
			y_position = variable.variable_dic['list_y_position']
			variable_list = 'sample_prefix_list'
			mylist = variable.variable_dic[variable_list]
			test_list = List_App(root,variable_list,mylist,x_position,y_position)
		except:
			print 'no prefix list yet'
		#variable_list = 'MQ_file_list'
		#mylist = variable.variable_dic[variable_list]
		#x_position = variable.variable_dic['list_x_position']
		#test_list = List_App(root,'test',['test1','test2'],x_position,100)
		r_tools = R_Tools_App(root)
		
		
		
		self.hi_there = Button(Load_frame, text="MaxQuant Data\nClean up", command=self.Load).pack(side=LEFT)
		
		#self.hi_there = Button(Load_frame, text="MaxQuant Extra", command=lambda R_dir = 'Tools', R_file_name ='Load_Extra.R': tools(R_dir,R_file_name)).pack(side=BOTTOM)
		self.hi_there = Button(Load_frame, text="MaxQuant Extra", command=self.Load_Extra).pack(side=BOTTOM)

		self.hi_there = Button(Load_frame, text="Custom", command=self.Load_Custom).pack(side=LEFT)
		
		self.hi_there = Button(Load_frame, text="Uniprot", command=self.Load_Uniprot_List).pack(side=BOTTOM)
		
		
		
		self.hi_there = Button(R_frame, text="View Table", command=lambda R_dir = 'Tools', R_file_name ='View_Table.R': tools(R_dir,R_file_name)).pack(side=BOTTOM)
		
		self.hi_there = Button(R_frame, text="Summary", command=lambda R_dir = '01_Load_Data', R_file_name ='Data_Overview.R': knitr_list(R_dir,R_file_name,key_list = [])).pack(side=BOTTOM)
		
		self.hi_there = Button(R_frame, text="View MQ File", command=self.view_file).pack(side=BOTTOM)
		
		
		
		experimental_design_list = StringVar()
		
		self.hi_there = Button(extract_frame, text="Extract_LFQ", command=lambda R_file_name='Extract_ALL_Label_Free_Quantiation_Values.R', add_list = [variable.variable_dic['quant_list_line']]: tools('Tools',R_file_name,add_list)).pack(side=LEFT)
		
		self.hi_there = Button(extract_frame, text="Extract_iTRAQ", command=lambda R_file_name='Extract_8_plex_iTRAQ.R': tools('Tools',R_file_name)).pack(side=LEFT)
		
		self.hi_there = Button(SILAC_frame, text="Extract SILAC", command= self.SILAC).pack(side=LEFT)	
		
		#self.extract_button = Button(R_frame, text="Extract LFQ", command=self.Extract).pack(side=LEFT)
		
		#self.extract_button = Button(R_frame_3, text="Sample\nSubset", command=self.separate_samples).pack(side=LEFT)
		
		self.extract_button = Button(R_frame_3, text="Select\nSamples", command=self.reverse_samples).pack(side=LEFT)
		
		self.extract_button = Button(R_frame_3, text="Reverse\nSamples", command=lambda R_dir = 'Tools', R_file_name ='reverse_silac.R', key_list = ['selected_quant_list_line','selected_reverse_sample_list_line']: tools_list(R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.extract_button = Button(R_frame_3, text="Remove\nSamples", command=lambda R_dir = 'Tools', R_file_name ='remove_column.R', key_list = ['selected_quant_list_line','selected_reverse_sample_list_line']: tools_list(R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		self.extract_button = Button(R_frame_3, text="Split\nSamples", command=lambda R_dir = 'Tools', R_file_name ='silac_split.R', key_list = ['selected_quant_list_line','selected_reverse_sample_list_line']: tools_list(R_dir,R_file_name,key_list)).pack(side=LEFT)
		
		
		self.hi_there = Button(R_frame_3, text="Rename\nSamples", command=lambda R_file_name='rename.R',R_dir = 'Tools': tools_list(R_dir,R_file_name)).pack(side=LEFT)

		#self.hi_there = Button(R_frame, text="Simple\nSummary", command=self.Simple_Summary).pack(side=LEFT)
		#self.hi_there.pack(side=LEFT)
		
		#self.hi_there = Button(R_frame_2, text="Label Free\n2 conditions\n>3 replicates", command=self.LFQ)
		#self.hi_there.pack(side=LEFT)
		
		#self.hi_there = Button(R_frame_2, text="Label Free\nTime Course", command=self.LFQ_TimeCourse)
		#self.hi_there.pack(side=LEFT)
		
		self.hi_there = Button(root, text="Refresh R", command=self.Refresh).place(x=0,y=0)
		self.hi_there = Button(root, text="README", command=self.readme).place(x=90,y=0)
		self.hi_there = Button(root, text="Experiment_Name", command=experiment_name).place(x=180,y=0)
		variable_default('design_list',[])
		self.hi_there = Button(root, text="Design", command=design).place(x=330,y=0)
		
		self.hi_there = Button(root, text="IPA", command=	self.IPA).place(x=330,y=30)
		
		#self.extract_button = Button(R_frame_2, text="Sample Info", command=self.Sample_Info).pack(side=LEFT)
		
		#self.test = Button(R_frame,text='test list',command=self.test).pack(side=LEFT)
		variable_default('initialise','no')
		if variable.variable_dic['initialise'] == 'no':
			Initialise()
		
		load_line()
		
		ReLoad_Functions()
		preamble()
		
		
	
	def IPA(self):
	  custom_app = Custom_App(root)
	
	

	def readme(self):
		cmd = 'kate %s' %(os.path.join(python_path,'readme.txt'))
		print(cmd)
		subprocess.Popen(cmd,shell =True)
	
	def reverse_samples(self):
		R_dir = 'Tools'
		R_file_name ='sample_names.R'
		#key_list = ['selected_quant_list_line','selected_reverse_sample_list_line']
		tools_list(R_dir,R_file_name)
		
		read_file = open('%s/rscripts/sample_names.txt' %(dirname))
		read_list = read_file.readlines()
		read_file.close()
		
		print(read_list)
		new_list = []
		for entry in read_list:
			new_list.append(entry.replace('\n',''))
		print(new_list)
		#raw_input()
		
		#sample_tk = Tkinter.Tk()
		new_list = List_App(root,'reverse_sample_list',new_list,800,0,'no')

		

		
		
	def SILAC(self):
		print 'SILAC'
		read_file = open('%s/proteinGroups.txt' %(dirname), 'r')
		read_list = read_file.readlines()
		read_file.close()
		print read_list[0]
		heading_list = read_list[0].split('\t')
		print heading_list
		sample_list = []
		for heading in heading_list:
			print heading
			if "Experiment" in heading:
				print heading
				print 'HIT'
				sample_name = heading.replace('Experiment ','')
				print sample_name
				sample_list.append(sample_name)
		R_sample_list = R_characters(sample_list)
		print R_sample_list
		
		
		R_file_name='Extract_SILAC.R'
		silac_list = ["Ratio.H.L","Ratio.H.L.normalized"]
		write_variable('silac_quant_list',silac_list)
		write_variable('silac_sample_list',R_sample_list)
		add_list = ['silac_quant_list_line','silac_sample_list_line','table_name_line','variability_line']
		knitr_list('Tools',R_file_name,add_list)
				
		
	def separate_samples(self):
		experiment_list = variable.variable_dic['sample_name_list']
		
		print experiment_list
		sample_app = Sample_App(root)
		
	def view_file(self):
		file_name = variable.variable_dic['selected_file_list'][0]
		print file_name
		file_path = '%s/%s' %(dirname,file_name)
		print file_path
		cmd = 'soffice -calc %s' %(file_path)
		print(cmd)
		#os.system(cmd)
		subprocess.Popen(cmd,shell =True)
	
	def Load_Uniprot_List(self):
		base_dir = variable.variable_dic['base_dir']
		file_path = Get_File_name_path(base_dir)
		print file_path
		add_list = ['file_path = "%s"\n' %(file_path)]
		tools('Tools','Load_Uniport_List.R',add_list)
		file_name_list = file_path.split('/')
		print(file_name_list)
		file_name = file_name_list[len(file_name_list)-1]
		print(file_name)
		uniprot_list = variable.variable_dic['uniprot_list']
		if file_name not in uniprot_list:
			uniprot_list.append(file_name)
			write_variable('uniprot_list',uniprot_list)
		
		variable_list = 'uniprot_list'
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,variable.variable_dic['uniprot_x'],variable.variable_dic['uniprot_y'])
		#command=lambda R_file_name ='Load_Housekeeping_Genes.R', add_list = [variable.variable_dic['uniprot_list_line']]: tools(self.R_dir,R_file_name,add_list)).pack(side=LEFT)
		
		
		
		
	def Extract(self):
		print 'Extract'
		#Extract_Sample_Info()
		Extract_app = Extract_App(root)
		Extract_Quant()

	def Refresh(self):
	  edit_R_app = Edit_R_App(root)
	  

	
	def View_Summary(self):	
		Run_Latex('latex_summary.R')
		status("View Summary")
		return

		
		status('summary')
		
	def sqlite(self):
		cmd = 'python ~/Documents/programming/python/MQ_parse/00_MQ_sqlite_compiled.py %s' %(dirname)
		print(cmd)
		os.system(cmd)
	

	def Load_Extra(self):

		
		load_file_list = variable.variable_dic['file_list']
		print(load_file_list)
		#maxquant_extra_file_list = ['evidence.txt','msms.txt','msmsScans.txt','msScans.txt','allPeptides.txt']
		#for file_name in maxquant_extra_file_list:
		#	load_file_list.append(file_name)
		#print(load_file_list)
		
		maxquant_extra_file_list = os.listdir(dirname)
		write_variable('maxquant_file_list',maxquant_extra_file_list)
		for file_name in maxquant_extra_file_list:
			if '.txt' in file_name:
				load_file_list.append(file_name)
		print(load_file_list)
		write_variable('file_list',list(set(load_file_list)))
		variable_list = 'file_list'
		
		#sys.path.insert(1, dirname+'/rscripts')
		#import variable
		#reload(variable)
		
		R_dir = 'Tools'
		R_file_name ='Load_Extra.R'
		add_list = ['maxquant_file_list_line']
		tools_list(R_dir,R_file_name,add_list)
		
		x_position = variable.variable_dic['file_list_x']
		y_position = variable.variable_dic['file_list_y']
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')

	def Load_Custom(self):
		
		print 'Custom'
		file_list = os.listdir(dirname)
		print file_list
		file_name_path = Get_File_name_path(dirname)
		print(file_name_path)
		read_file = open(file_name_path,'r')
		read_list = read_file.readlines()
		read_file.close()
		heading_list = read_list[0].split('\t')
		print(heading_list)
		#List_App(root,'heading_list',heading_list)
		print(file_name_path)
		file_name_list = file_name_path.split('/')
		file_name = file_name_list[len(file_name_list)-1]
		print(file_name_path)
		file_path = '/'.join(file_name_list[0:len(file_name_list)-1])
		load_file_list = variable.variable_dic['file_list']
		print(load_file_list)
		load_file_list.append(file_name)
		print(load_file_list)
		write_variable('file_list',list(set(load_file_list)))
		variable_list = 'file_list'
		
		#sys.path.insert(1, dirname+'/rscripts')
		#import variable
		#reload(variable)
		
		x_position = variable.variable_dic['file_list_x']
		y_position = variable.variable_dic['file_list_y']
		mylist = variable.variable_dic[variable_list]
		test_list = SpinBox_List_App(root,variable_list,mylist,x_position,y_position,'no')
		
		#self.Initialise()
		cmd = 'cp %s %s' %(file_name_path,dirname)
		print(cmd)
		os.system(cmd)
		
		R_dir = '01_Load_Data'
		R_file = '01_Load.R'
		add_list = ["file_list = c('%s')\n" %(file_name),"file_path = '%s'\n" %(file_path)]
		tools(R_dir,R_file,add_list)
		
		R_dir = '01_Load_Data'
		R_file = 'rowname.R'
		write_variable('table_name',"df."+file_name)
		tools_list(R_dir,R_file)
		
		#R_file_list = ['01_Load.R']
		#for R_file_name in R_file_list:
		#	if variable.variable_dic['edit_list_entry'] == 'rstudio':
		#		cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
		#	else:
		#		cmd = 'Rscript %s/rscripts/%s' %(dirname,R_file_name)
		#	print(cmd)
		#	os.system(cmd)
		#	status('Load')
		
		
		#for R_file_name in R_file_list:
			#if variable.variable_dic['edit_list_entry'] == 'rstudio':
				#cmd = 'rstudio %s/rscripts/%s' %(dirname,R_file_name)
			#else:
				#cmd = 'Rscript %s/rscripts/%s' %(dirname,R_file_name)
			#print(cmd)
			#os.system(cmd)
			#status('Load')
		
		
		
	def Load(self):
		Load()
		#self.Initialise()


	
	def Sample_Info(self):
		Extract_Sample_Info()
		print("Complete : Extract sample Info")

			#raw_input(R_file_name)
	def summary(self):
		R_file_list = ['summary_stat.R']
		for R_file_name in R_file_list:
			#raw_input(R_file_name)
			cmd = '%s %s/rscripts/%s' %(rscript_dirname,R_file_name)
			print(cmd)
			os.system(cmd)
			status('Load')
			#runnint.set('\nLoaded Data, please select and option')
		#runnint.set('\nCompleted summary, please select another option ...')
		Rnw_file_list = ['summary']
		os.chdir('%s/rscripts/' %(dirname))
		
		cmd = 'python %s/01_Load_Data/summary_tex.py %s %s' %(python_path,python_path,dirname)
		#raw_input(cmd)
		os.system(cmd)
		
		status('summary')
		
	def Simple_Summary(self):	
		R_dir = '01_Load_Data'
		R_file_name = 'simple_summary.R'
		
		#Edit_R_script(R_dir,R_file_name)
		Simple_Run_Sweave_file(R_dir,R_file_name)
		#create_R_file(R_dir,'simple_summary_write.R')
		#Run_rscript('simple_summary_write.R')
		#Run_Latex('simple_summary_write.R')
		status("Simple Summary")
		return

		
		status('summary')

		
	def LFQ(self):
		#runnint.set('LFQ....')
		LFQ_app = LFQ_App(root)
		status('LFQ',new_experimental_design = 'yes')
	
	def LFQ_TimeCourse(self):
		lfq_timecourse_app = LFQ_TimeCourse_App(root)
		
	#def SILAC(self):
		#runnint.set('LFQ....')
	
	#def test_gui(self):
		#runnint.set('Testing ....')
	#	status('Test')
		


class App:
	def __init__(self, master,python_path):
		self.master_frame = Frame(master).pack()
		
		host_name = socket.gethostname()
		data_dir = python_path
		print host_name
		rscript_command = 'Rscript'
		if host_name == 'sgarnett-dell':

			#else:
			#data_dir = '/home/sgarnett/Documents/RData/SH-SY5Y/Differentiation/data/140322_SH7_Dionex_QE_/'
			data_dir = '/home/sgarnett/Documents/RData/Neural_Stem_Cells/SILAC/Experiment_2/data/SCX_13_11_All/'
			data_dir = '/home/sgarnett/Documents/RData/Neural_Stem_Cells/Gene_Experssion/'
			data_dir = '/home/sgarnett/Documents/RData/SH-SY5Y/SH-SY5Y_Differentiation/140322_SH7_Dionex_QE_LFQ'
			#data_dir = '/home/sgarnett/Documents/RData/Neural_Stem_Cells/SILAC/140814_ALL_NSC_SILAC/'
			base_dir = '/home/sgarnett/Documents/RData/'
		elif 'srv' in host_name:
		    data_dir = '/medbio/MS_Experiments'
		    base_dir = '/medbio/MS_Experiments'
		    rscript_command = '/opt/exp_soft/R-3.0.1/bin/Rscript'
		else:
			data_dir = '/MS_Experiments'
			base_dir = '/MS_Experiments'
		
		global base_dir
		location = 'yes'
		if location  == 'yes':
			python_file_list = os.listdir(python_path)
			if 'location.txt' in python_file_list:
				read_file = open(os.path.join(python_path,'location.txt'),'r')
				read_list = read_file.readlines()
				read_file.close()
				data_dir = read_list[0]
		dirname = Select_Directory(data_dir)
		if location  == 'yes':
			write_file = open(os.path.join(python_path,'location.txt'),'w')
			write_file.writelines(dirname)
			write_file.close()

		global dirname
		global rscript_command
		
		rscript_file_list = os.listdir('%s/rscripts' %(dirname))
		if 'variable.py' not in rscript_file_list:
			variable_dic = {}
			f = open('%s/rscripts/variable.py' %(dirname),'w')
			f.write('variable_dic = {}')
			f.close()
		sys.path.insert(1, dirname+'/rscripts')
		import variable
		#reload(variable)
		print_variable()
		
		write_variable('base_dir',base_dir)
		global variable
		
		self.dir_text = StringVar()
		Label(self.master_frame, textvariable=self.dir_text,font = tkFont.Font(family="Arial",size=12,weight="bold")).pack()
		self.dir_text.set(dirname)

		R_Button = Button(self.master_frame, text="R Program", command=R_App(root)).place(x=10,y=30)
		
		SQLite_Button = Button(self.master_frame, text="Load Data into sqlite", command=self.sqlite).place(x=100,y=30)
		
		
		
		Quit_Button = Button(self.master_frame, text="QUIT", fg="red", command=master.quit).place(x=1100,y=0)
		
		variable_dic_buttion = Button(self.master_frame, text = 'R variables', fg = 'blue',command=self.print_variable).place(x=1100,y=35)
		

	
	def Select_Directory(self,path):
		Select_Directory(path)
		self.dir_text.set(dirname)
		host_name = socket.gethostname()
		#raw_input(host_name)
		#print(python_path)
		#if host_name == 'sgarnett-dell':
		#	dir_file_w = open('%s\location.txt' %(python_path),'w')
		#	dir_file_w.write(dirname)
		#	dir_file_w.close()
		#	#raw_input(dirname)
	
	def sqlite(self):
		sqlite_app = SQlite_App(root)
		global sqlite_app
	def print_variable(
		self):
		print_variable()
		
	def Load_R_app(self):
		R_app = R_App(root)
		


	
root = Tkinter.Tk()
root.title("MQ Gui %s" %(version_number))
#scrollbar = Scrollbar(root)
#scrollbar.pack(side=RIGHT, fill=Y)
#raw_input(python_path)
app = App(root,python_path)
root.geometry("1200x800+100+100")

root.mainloop()
root.destroy()

status_file = open('%s/rscripts/status.txt' %(dirname), 'w')
status_list = status_file.writelines(status_list)
status_file.close





						