Help file under construction

How to use this program :

When the program loads it opens, asking "Please select a MaxQuant 'txt' directory"
	point this to a directory containing the MaxQuant txt files, ie proteinGroups.txt
	the txt file will not be visible, as only folder can be seen



MaxQuant Data Clean Up : imports proteinGroups, peptides, parameters and summary files from maxquant
								it also removes contaminants and reverse hits
								
Summary : generates graphs from the summary.txt file

Custom : imports any tab delimited file into the program and adds it to the file_list scroll bar

Uniprot - allows the import of any tab or csv file, the file is added as a list
				it can be used to import peptide or accession numbers 
				the file is added to the uniprot_list scroll bar
					these file are used only used in certain function, these function use this list to search the data before processing
						housekeeping gene normalised
						Search Peptides
						Search Proteins
						matplot selection
						cmean selection

MaxQuant Extra : imports the remiaing MaxQuant file, do not do this unless you need these files, it increases the memory load


	
Select proteinGroups under file_list
		observe that the table name in blue at the to changes to table_name = df.proteinGroups.txt.edited
			table_name indicates the table currently being used in R
				 ready indicates the table exists and can be used
			table not found indicates the table does not exist and needs to be created using one fo the program fucntions
		Every function in the program creates a new table, by adding a suffix
			suffic's are added to the table name using the scroll bars
				ie file_list,quant_list,suffix_list,norm_list,mod_list
			see below for information of the suffix meanings
		
View MQ File : will allow you to view the original MaxQuant file in open office
View Table : will allow you to view the currently selected table if it exist

Extract_LFQ : extracts all Label Free Quantiation values, and creates new table containing only the intensity columns

Select Intensity under quant_list:
	observe table_name = df.proteinGroups.txt.edited.Intensity
		a table containing only the Intensity values for all sample from the proteinGroups.txt file have been extracted
	
Remove Zero's : generates the tables with suffix na,na.z, z2na

Generate Report : summarised the protein numbers and gives a brief overview of the data


2 Condition Experiment
	nees to have at least 3 replicates per condition, and the same number of replicates per condition
LOAD : MaxQuant Data and Clean Up
Extract SILAC : set the degree of variablity
Remove Zero's
Experimental Design : button needs to be pushed twice.
	open a file, all sample from condition 1 should be labelled with a 1 and condtion 2 with a 2
Paired Ratio : divides first the replicates in condtion1 with each other
	generates histograms illustrating the mean and standard deviation
	then repeates the process for condition 2
		The standard deviation for the replicates is used as the sd_cutoff in the main window
	Ratio's are then generated for all comparisons bwtween condition 1 and condition 2
		condition 1 / condition 2
		the final graph indicates shows a summary of the standard deviations
			the standard deviations for condition 1 and condition 2 should be lower than that for their comparisons
				Indicating less variance within replicates than between comparisons. 
	table created
		log2.ratio.similar or log2_ratio.replicate
			log2 ratio of the replicates
		log2_ratio.different or log2_ratio.comparison
			a table with all the ratios of the comparisons
T test : calculates p-values from a number of different t test
	generates FDR plots and volcano plots of the results
		see the suffix list for a description on the t test performed
		
Select : selects proteins passing a selection criteria
		for a 2 condition experiment, result_list needs to be set to log2_ratio.stat
		set the sd_cutoff and t_cutoff, press set to save the changes
	greater than the sd_cutoff
	less than the t_cutoff
		generates new table with these results

MQ Write
	writes a txt file of the results
	choose the result you want with "selected_result_data_list"
	output in /output/selected/
	 


SILAC Experiment
LOAD : MaxQuant Data and Clean Up
Extract SILAC : set the degree of variablity
Remove Zero's

Select Samples
	Reverse Sample - log2 transformes the data and reverses the labels (log2.rev)
	Split samples - Split the sample into two proteinGroups 
		.comparison
		.replicate
	Remove Samples - removes columns from the table (.rm)

		

LOAD
MaxQuant Data \n Clean up : Loads the MaxQuant files (proteinGroups.txt,peptides.txt,summary.txt,parameters.txt)



### Explanation of table_name suffixes
suffix list
na.z : all rows with na or zero values are removes
z2na : zero value are changed to NA, no data is removed

norm_list
qn : quantile normalised by preprocessCore
sum : normalised the sums of all the columns
mean : normalised the means of all the columns
median : normalised the medians of all the columns

first_ratio : divide each row by the first entry of that row
last_ratio : divide eash row by the last entry of that row
mean_ratio : divide eash row by the row mean

mod_list
log2 : log2 transform the data.frame - log2 button
log2_ratio : log2 transformed the Paried Ration data.frame - generated by Paired Ration Button
comparison : different : table of ratio for the sample being compared (ie 1 vs 2 in Experimental Design)
replicate : similar : table of ratios for the replicates (ie 1 vs 1 in the Experimental Design)
hc_median or hc_mean : data normalised by the median or mean of the housekeeping proteins, added as a list by Uniprot button
								and selected using uniprot_list

all : all the data
sd_1 : only data passing the sd_cutoff
sd_2 : only data passing 2x sd_cutoff
t : only data with t-test p-value less than the t_cutoff
sd_1_t : only data passing both the sd_cutoff and t_cutoff
sd_2_t : only data passing both the 2x sd_cutoff and the t_cutoff

t_test_list
All t test are done by row
t_test_together : t.test(entire row)$p.value)
t_test_seperate : t.test(condition1,condition2)$p.value)
t_test_seperate_paired : t.test(condition_1,condition_2,paired=TRUE)$p.value)
t_test_seperate_paired : t.test(condition_1,condition_2,paired=TRUE)$p.value)
t_test_separate_paired_two.sided : t.test(condition_1,condition2,paired=TRUE,alternative = c('two.sided'))$p.value)
t_test_separate_ts = t.test(condition_1,condition2,alternative = c('two.sided')$p.value)

  df.t_test_temp[t,'wilcox_test_together'] = try(wilcox.test(as.numeric(comparison_data[t,]),exact=FALSE,correct=FALSE)$p.value)
  
  
  

  
INSTALLATION INSTRUCTIONS
linux programs required
python 2.7
R.3
Rstudio
okular
kate
pandoc
makrdown
latex
libcurl4-gnutls-dev
libxml2-dev

MAC
install macports to instal linux software
edit /etc/paths to include paths for linux software - there might be a better way
  add the following lines
  /Applications/MacPorts/KDE4/kate.app/Contents/MacOS
  /Applications/MacPorts/KDE4/okular.app/Contents/MacOS
  /Applications/RStudio.app/Contents/MacOS
  
python modules should be installed automatically with python
os, sys, socket, Tkinter, tkFileDialog, tkFont, time, re, subprocess

R packages 
install.packages()
'knitr', 'ggplot2', 'reshape','e0171', 'devtools'
R bioconductor packages
source("http://bioconductor.org/biocLite.R")
biocLite()
'limma','preprocessCore', 'Biobase', 'org.Hs.eg.db', 'reactome.db', 'XML', 'genefilter', 'Hmisc'