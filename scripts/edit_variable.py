import os
import sys


def write_variable(identifier,entry):
	sys.path.insert(1, dirname+'/rscripts')
	import variable
	variable.variable_dic[identifier] = entry
	print '%s : %s\n' %(identifier,entry)
	if is_sequence(entry):
		identifier_line = "%s = c('%s')" %(identifier,"','".join(entry))
		identifier_line_int = "%s = c(%s)" %(identifier,",".join(entry))
		print '%s_line: %s '%(identifier,identifier_line) 
		print '%s_line_int: %s '%(identifier,identifier_line_int) 
		variable.variable_dic['%s_line' %(identifier)] = identifier_line+'\n'
		variable.variable_dic['%s_line_int' %(identifier)] = identifier_line_int+'\n'
		if len(entry) == 1:
			print '%s_entry: %s_entry = "%s"\n'%(identifier,identifier_line,entry[0])
			variable.variable_dic['%s_entry' %(identifier)] = '%s_entry = "%s"\n' %(identifier,entry[0])
	else:
		if "_line" not in identifier:
			print '%s_line : %s = %s\n' %(identifier,identifier,entry)
			variable.variable_dic[identifier+'_line'] = '%s = %s\n' %(identifier,entry)
			print '%s_line_str : %s = "%s"\n' %(identifier,identifier,entry)
			variable.variable_dic[identifier+'_line_str'] = '%s = "%s"\n' %(identifier,entry)
	#for key in variable.variable_dic.keys():
	#	print('%s : %s' %(key,variable.variable_dic[key]))
	#raw_input()
	variable_file = open('%s/rscripts/variable.py' %(dirname),'w')
	variable_file.write("variable_dic = %s" %(variable.variable_dic))
	variable_file.close()
	
def is_sequence(arg):
    return (not hasattr(arg, "strip") and
            hasattr(arg, "__getitem__") or
            hasattr(arg, "__iter__"))

global dirname
dirname = sys.argv[1]

sys.path.insert(1, dirname+'/rscripts')
import variable
key_list = variable.variable_dic.keys()
key_list.sort()
for key in key_list:
	print key
selected_key = raw_input('key : ')
test = ''
while selected_key != 'exit':
	try:
		test = variable.variable_dic[selected_key]
	except:
		selected_key = raw_input('key : ')
	print variable.variable_dic[selected_key]
	new_entry = raw_input('variable : ')
	if new_entry != '':
		if 'list' in selected_key:
			new_entry = new_entry.split(', ')
		write_variable(selected_key,new_entry)
		raw_input('write variable')
	selected_key = raw_input('key : ')
	
	