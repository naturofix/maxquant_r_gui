import os
import sys
import sqlite3 as lite
#import MySQLdb
module_path = '/home/sgarnett/Documents/programming/python_modules/'
sys.path.append(module_path)
import functions
import mysql_mod


file_path = sys.argv[1]
print file_path
file_name = os.path.basename(file_path)
print(file_name)
path_name = os.path.dirname(file_path)
print path_name
#raw_input()

read_file = open(file_path,'r')
read_list = read_file.readlines()
read_file.close()

heading_list = read_list[0].split('\t')
i = 0
for heading in heading_list:
	print '%s\t%s' %(i,heading)
	i+=1

list_number = int(0)

extract_list = []
for read_line in read_list[1:]:
	#print read_line
	entry_list = read_line.split('\t')
	#print(entry_list)
	extract_list.append(entry_list[list_number])
	#print(extract_list)
	#raw_input()
	
print(len(extract_list))
extract_list_set = set(extract_list)
print(len(extract_list_set))

database_name = 'SQLite'
con = lite.connect(database_name)
cur = con.cursor()
cur.execute("SELECT name FROM sqlite_master WHERE type='table'")

rows = cur.fetchall()
print '\nTable Names:'
for row in rows:
	print row[0]
print'\n'
raw_input('enter to continue')

path = path_name
table_name = mysql_mod.MySQLName(file_name)
print file_name
read_file = open(path+'/'+file_name,'r')
read_list = read_file.readlines()
read_file.close()
try:
	print read_list[0]
	hit = 1
except:
	hit = 0
	raw_input('error in file %s' %(file_name))
	for read_line in read_list:
		print read_line
if hit == 1:
	title_list = read_list[0].split('\t')
	print title_list
	header_list = []
	for title in title_list:
		edited_title = mysql_mod.MySQLName(title)
		attribute = 'TEXT'
		if edited_title == 'PEP':
			attribute = 'FLOAT'
			#raw_input('PEP')
		header = '%s %s' %(edited_title,attribute)
		print header
		header_list.append(header)
		#cur.execute(sql_header_line)
	try:
		cur.execute('DROP TABLE IF EXISTS %s' %(table_name))
		sql_header_line = 'Create Table if not exists %s (%s)' %(table_name,', '.join(header_list))
		print sql_header_line
		cur.execute(sql_header_line)
		hit = 1
	except lite.Error, e:
		print "Error %s:" % e.args[0]
		raw_input('error with create table %s' %(table_name))
		hit = 0
	#for read_line in read_list:
	#	print read_line
	#	raw_input()
	if hit == 1:
		for read_line in read_list[1:]:
			#print read_line
			entry_list = (read_line.replace('\n','')).split('\t')
			#print entry_list
			try:
				cmd = 'INSERT INTO %s VALUES("%s")' %(table_name,'","'.join(entry_list))
				print(cmd)
				cur.execute(cmd) 
			except lite.Error, e:
				print cmd
				print "Error %s:" % e.args[0]
				raw_input('error inserting data')
			#print '\n'+cmd+'\n'
			
			#raw_input()
		#cmd = "INSERT INTO %s VALUES(%s)" %(table_name,','.join((['?']*len(title_list)))),read_list[1:]
		#print cmd
		#cur.executemany("INSERT INTO
		
con.commit()