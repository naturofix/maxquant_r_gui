import os
import sys
import sqlite3 as lite
#import MySQLdb
module_path = '/home/sgarnett/Documents/programming/python_modules/'
sys.path.append(module_path)
import functions
import mysql_mod

def create_table(title_list,table_name):
	header_list = []
	for title in title_list:
		edited_title = mysql_mod.MySQLName(title)
		attribute = 'TEXT'
		if edited_title in float_list:
			attribute = 'FLOAT'
		header = '%s %s' %(edited_title,attribute)
		print header
		header_list.append(header)
	try:
		cur.execute('DROP TABLE IF EXISTS %s' %(table_name))
		sql_header_line = 'Create Table if not exists %s (%s)' %(table_name,', '.join(header_list))
		print sql_header_line
		cur.execute(sql_header_line)
		#raw_input()
		#hit = 1
	except lite.Error, e:
		print "Error %s:" % e.args[0]
		raw_input('error with create table %s' %(table_name))
		
def write_sorted_table(table_name,order_by,direction='ASC'):
	cmd = 'SELECT * from %s ORDER BY %s %s;' %(table_name,order_by,direction)
	#print cmd
	cur.execute(cmd)
	ordered_list = cur.fetchall()
	reduced_write_list = []
	for entry in ordered_list:
		str_list = [str(i) for i in entry]
		reduced_write_list.append('\t'.join(str_list)+'\n')
	
	write_file_name = '%s/%s.txt' %(path,table_name)
	write_file = open(write_file_name,'w')
	write_file.writelines(reduced_write_list)
	write_file.close()
	print write_file_name
	print len(reduced_write_list)
	return len(reduced_write_list)

#def split_write_table(table_name,order_by,direction='ASC'):
	#cmd = "SELECT m_z FROM %s" %(table_name)
	#print(cmd)
	#cur.execute(cmd)
	#m_z_tup = cur.fetchall()
	#m_z_list = []
	#for m_z in m_z_tup:
		#m_z_list.append(m_z[0])
	##print m_z_list
	#int_list = [float(i) for i in m_z_list]
	#median = numpy.median(int_list)
	#print median
	#cmd = 'SELECT * from %s ORDER BY %s %s;' %(table_name,order_by,direction)
	##print cmd
	#cur.execute(cmd)
	#ordered_list = cur.fetchall()
	#upper_write_list = []
	#lower_write_list = []
	#upper_mz = []
	#lower_mz = []
	#for entry in ordered_list:
		##raw_input(entry[0])
		#mz = float(entry[0])
		##print mz
		##print median
		#str_list = [str(i) for i in entry]
		#if mz <= median:
			#str_list = [str(i) for i in entry]
			#lower_write_list.append('\t'.join(str_list)+'\n')
			#lower_mz.append(mz)
		#else:
			#upper_write_list.append('\t'.join(str_list)+'\n')
			#upper_mz.append(mz)
		##print lower_mz
		##print upper_mz
		##raw_input()
	##print lower_mz
	##raw_input()
	##print upper_mz
	#low_write_file_name = '%s/%s_%s_%s.txt' %(path,table_name,min(lower_mz),max(lower_mz))
	#write_file = open(low_write_file_name,'w')
	#write_file.writelines(lower_write_list)
	#write_file.close()
	#upper_write_file_name = '%s/%s_%s_%s.txt' %(path,table_name,min(upper_mz),max(upper_mz))
	#write_file = open(upper_write_file_name,'w')
	#write_file.writelines(upper_write_list)
	#write_file.close()
	#print low_write_file_name
	#print len(lower_write_list)
	#print upper_write_file_name
	#print len(upper_write_list)

def split_write_table(table_name,order_by,direction='ASC'):
	cmd = "SELECT m_z FROM %s" %(table_name)
	print(cmd)
	cur.execute(cmd)
	m_z_tup = cur.fetchall()
	m_z_list = []
	for m_z in m_z_tup:
		m_z_list.append(m_z[0])
	#print m_z_list
	split_number = len(m_z_list)/5000 
	print split_number
	m_z_list.sort()
	int_list = [float(i) for i in m_z_list]
	int_list.sort()
	mz_split_lists = numpy.array_split(int_list,split_number+1)
	
	print mz_split_lists
	str_split_list = []
	for entry_list in mz_split_lists:
		str_split_list.append(str(i) for i in entry_list)
	print (len(str_split_list))
	#raw_input()
	#for mz_split in mz_split_list:
		
	#raw_input()
	#mz_split_lists = [range(t,t+split_number) for t in m_z_list]
	#raw_input(len(mz_split_lists))
	#print mz_split_lists
	#raw_input()
	
	median = numpy.median(int_list)
	print median
	cmd = 'SELECT * from %s ORDER BY %s %s;' %(table_name,order_by,direction)
	#print cmd
	cur.execute(cmd)
	ordered_list = cur.fetchall()
	upper_write_list = []
	lower_write_list = []
	upper_mz = []
	lower_mz = []
	write_list_lists = []
	mz_lists = []
	for i in range(len(mz_split_lists)):
		write_list_lists.append([])
		mz_lists.append([])
	for entry in ordered_list:
		print entry[0]
		for i in range(len(mz_split_lists)):
			if float(entry[0]) in mz_split_lists[i]:
				str_list = [str(j) for j in entry]
				write_list_lists[i].append('\t'.join(str_list)+'\n')
				mz_lists[i].append(float(entry[0]))
	for i in range(len(mz_split_lists)):
		write_list = write_list_lists[i]
		mz_list = mz_lists[i]
		print write_list[0]
		print write_list[len(write_list)-1]
		write_file_name = '%s/%s_%s_%s.txt' %(path,table_name,min(mz_list),max(mz_list))
		write_file = open(write_file_name,'w')
		write_file.writelines(write_list)
		write_file.close
		print write_file_name
		print(len(write_list))


		#hit = 0

float_list = 'PEP','start','stop'
pep_threshold = 0.01
extend_rt = 3
min_threshold = 3
max_threshold = 5
proteins_filename = 'proteins.txt'


database_name = 'MQ_sqlite.db'
path = '/home/sgarnett/Documents/RData/SH-SY5Y/SH-Ref/data/isolation_window'
path = sys.argv[1]
program_option = sys.argv[2]
select_file = sys.argv[3]
print 
file_list = os.listdir(path)
print file_list

os.chdir(path)
if database_name not in file_list:
	print """\n\nnew database %s create in %s
	type ".exit" to exit from sqlite3\n""" %(database_name,path)
	os.system('sqlite3 %s;' %(path+'/'+database_name))
	
#database_name = '/home/sgarnett/Documents/programming/python/MQ_sqlite/MQ_test.db'

con = lite.connect(database_name)
con.row_factory = lite.Row
cur = con.cursor()
cur.execute("SELECT name FROM sqlite_master WHERE type='table'")

rows = cur.fetchall()
print '\nTable Names:'
for row in rows:
	print row[0]
print'\n'
#raw_input('enter to continue')

option_number_list = []
program_option_list = ['LOAD','SELECT','TRIM','WRITE']
#for i in range(len(program_option_list)):
#	print '%s : %s' %(i,program_option_list[i])
#	option_number_list.append(i)
#selected_option = int(raw_input('Select option :'))

print(program_option)

if program_option == 'LOAD':
	for file_name in file_list:
		if file_name.find('.txt') != -1:
			table_name = mysql_mod.MySQLName(file_name)
			print file_name
			read_file = open(path+'/'+file_name,'r')
			read_list = read_file.readlines()
			read_file.close()
			try:
				print read_list[0]
				hit = 1
			except:
				hit = 0
				#raw_input('error in file %s' %(file_name))
				for read_line in read_list:
					print read_line
			if hit == 1:
				title_list = read_list[0].split('\t')
				print title_list
				header_list = []
				for title in title_list:
					edited_title = mysql_mod.MySQLName(title)
					attribute = 'TEXT'
					if edited_title == 'PEP':
						attribute = 'FLOAT'
						#raw_input('PEP')
					header = '%s %s' %(edited_title,attribute)
					print header
					header_list.append(header)
					#cur.execute(sql_header_line)
				try:
					cur.execute('DROP TABLE IF EXISTS %s' %(table_name))
					sql_header_line = 'Create Table if not exists %s (%s)' %(table_name,', '.join(header_list))
					print sql_header_line
					cur.execute(sql_header_line)
					hit = 1
				except lite.Error, e:
					print "Error %s:" % e.args[0]
					#raw_input('error with create table %s' %(table_name))
					hit = 0
				#for read_line in read_list:
				#	print read_line
				#	raw_input()
				if hit == 1:
					for read_line in read_list[1:]:
						#print read_line
						entry_list = (read_line.replace('\n','')).split('\t')
						#print entry_list
						try:
							cmd = 'INSERT INTO %s VALUES("%s")' %(table_name,'","'.join(entry_list))
							cur.execute(cmd) 
						except lite.Error, e:
							print cmd
							print "Error %s:" % e.args[0]
							#raw_input('error inserting data')
						#print '\n'+cmd+'\n'
						
						#raw_input()
					#cmd = "INSERT INTO %s VALUES(%s)" %(table_name,','.join((['?']*len(title_list)))),read_list[1:]
					#print cmd
					#cur.executemany("INSERT INTO %s VALUES(%s)" %(table_name,','.join((['?']*len(title_list)))),read_list[1:])

if program_option == "Peptides":
	cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
	table_list = cur.fetchall()
	print '\nTable Names:'
	for table_name in table_list:
		print table_name[0]
	print'\n'
	
	read_file = open(select_file,'r')
	read_list = read_file.readlines()
	read_file.close
	protein_list = []
	for read_line in read_list:
		print read_line
		protein_list.append(read_line.replace('\n',''))
	print protein_list
	
	cmd = "SELECT * FROM %s" %('peptides_txt')
	print cmd
	cur.execute(cmd)
	col_names = [cn[0] for cn in cur.description]
	create_table(col_names,'selected_peptides')
	print(col_names)
	
	for protein in protein_list:
		print protein
		protein_select = '%'+protein+'%'
		print protein_select
		cmd = "SELECT Proteins FROM peptides_txt WHERE Proteins LIKE '%s'" %(protein_select)
		print(cmd)
		cur.execute(cmd)
		selected_protein_list = cur.fetchall()
		for selected_protein in selected_protein_list:
			print selected_protein
	#cur.execture(cmd)
if program_option == "peptide_sequence":
	cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
	table_list = cur.fetchall()
	print '\nTable Names:'
	for table_name in table_list:
		print table_name[0]
	print'\n'
	
	read_file = open(select_file,'r')
	read_list = read_file.readlines()
	read_file.close
	protein_list = []
	for read_line in read_list:
		print read_line
		protein_list.append(read_line.replace('\n',''))
	print protein_list
	
	cmd = "SELECT * FROM %s" %('peptides_txt')
	print cmd
	cur.execute(cmd)
	col_names = [cn[0] for cn in cur.description]
	create_table(col_names,'selected_peptides')
	print(col_names)
	
	for protein in protein_list:
		print protein
		protein_select = '%'+protein+'%'
		print protein_select
		cmd = "SELECT Proteins FROM peptides_txt WHERE Sequence LIKE '%s'" %(protein_select)
		print(cmd)
		cur.execute(cmd)
		selected_protein_list = cur.fetchall()
		for selected_protein in selected_protein_list:
			print selected_protein

if program_option == "peptide_mass":
	cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
	table_list = cur.fetchall()
	print '\nTable Names:'
	for table_name in table_list:
		print table_name[0]
	print'\n'
	
	read_file = open(select_file,'r')
	read_list = read_file.readlines()
	read_file.close
	protein_list = []
	for read_line in read_list:
		print read_line
		protein_list.append(read_line.replace('\n',''))
	print protein_list
	
	cmd = "SELECT * FROM %s" %('peptides_txt')
	print cmd
	cur.execute(cmd)
	col_names = [cn[0] for cn in cur.description]
	create_table(col_names,'selected_peptides')
	print(col_names)
	
	for protein in protein_list:
		print protein
		protein_select = '%'+protein+'%'
		print protein_select
		cmd = "SELECT Sequence,Mass FROM peptides_txt WHERE Mass LIKE '%s'" %(protein_select)
		print(cmd)
		cur.execute(cmd)
		selected_protein_list = cur.fetchall()
		for selected_protein in selected_protein_list:
			print selected_protein
		#raw_input()

if program_option == "msmsScans_mass":
	cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
	table_list = cur.fetchall()
	print '\nTable Names:'
	for table_name in table_list:
		print table_name[0]
	print'\n'
	
	read_file = open(select_file,'r')
	read_list = read_file.readlines()
	read_file.close
	protein_list = []
	for read_line in read_list:
		print read_line
		protein_list.append(read_line.replace('\n',''))
	print protein_list
	
	cmd = "SELECT * FROM %s" %('msmsScans_txt')
	print cmd
	cur.execute(cmd)
	col_names = [cn[0] for cn in cur.description]
	create_table(col_names,'selected_peptides')
	print(col_names)
	#raw_input()
	for protein in protein_list:
		print protein
		protein_select = '%'+protein+'%'
		print protein_select
		cmd = "SELECT Raw_file,Retention_time,m_z FROM msmsScans_txt WHERE m_z LIKE '%s'" %(protein_select)
		print(cmd)
		cur.execute(cmd)
		selected_protein_list = cur.fetchall()
		for selected_protein in selected_protein_list:
			print selected_protein
		if len(selected_protein_list) > 0:
			raw_input()

if program_option == "inclusion_protein":
	cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
	table_list = cur.fetchall()
	print '\nTable Names:'
	for table_name in table_list:
		print table_name[0]
	print'\n'
	
	read_file = open(select_file,'r')
	read_list = read_file.readlines()
	read_file.close
	protein_list = []
	for read_line in read_list:
		print read_line
		protein_list.append(read_line.replace('\n',''))
	print protein_list
	
	cmd = "SELECT * FROM %s" %('peptides_txt')
	print cmd
	cur.execute(cmd)
	col_names = [cn[0] for cn in cur.description]
	create_table(col_names,'selected_peptides')
	print(col_names)
	raw_input()
	for protein in protein_list:
		print protein
		protein_select = '%'+protein+'%'
		print protein_select
		cmd = "SELECT Proteins FROM peptides_txt WHERE Proteins LIKE '%s'" %(protein_select)
		print(cmd)
		cur.execute(cmd)
		selected_protein_list = cur.fetchall()
		for selected_protein in selected_protein_list:
			print selected_protein

				
if program_option == 'SELECT':
	table_name = 'exclusion_list'
	header_list = 'm_z FLOAT',"start FLOAT","stop FLOAT","Charge INT","Description TEXT, PEP FLOAT"
	cur.execute('DROP TABLE IF EXISTS %s' %(table_name))
	sql_header_line = 'Create Table if not exists %s (%s)' %(table_name,', '.join(header_list))
	print sql_header_line
	cur.execute(sql_header_line)
	#raw_input()


	cmd = "SELECT * FROM %s" %('evidence_txt')
	print cmd
	cur.execute(cmd)
	col_names = [cn[0] for cn in cur.description]
	create_table(col_names,'top_evidence')

	cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
	table_list = cur.fetchall()
	print '\nTable Names:'
	for table_name in table_list:
		print table_name[0]
	print'\n'
	#raw_input('enter to continue')


	table_list = [['evidence_txt']]
	print table_list

	top_all = []
	top_good = []
	top_bad = []
	print 'please be patient processing data'
	#raw_input()
	number_inserted = 0
	proteingroups_list = []
	for table_name in table_list: ### BEWARE THIS LIMITS number entered into the database
		table_name = table_name[0]
		print table_name
		title_list = []
		cmd = "SELECT * FROM %s" %(table_name)
		print cmd
		cur.execute(cmd)
		table_list= cur.fetchall()
		for table_line in table_list:
			proteingroup = table_line['Protein_group_IDs']
			#print proteingroup
			if proteingroup not in proteingroups_list:
				proteingroups_list.append(proteingroup)
				seq_cmd = 'SELECT * FROM %s WHERE Protein_group_IDs = "%s"' %(table_name,proteingroup)
				cur.execute(seq_cmd)
				seq_list = cur.fetchall()
				#print(seq_list)
				#print(len(seq_list))
				sequence_list = []
				for seq_entry in seq_list:
					#print seq_entry["Sequence"],seq_entry["Modified_sequence"],seq_entry["Retention_time"],seq_entry["Calibrated_Retention_Time"],seq_entry["Calibrated_Retention_Time_Start"],seq_entry["Calibrated_Retention_Time_Finish"],seq_entry["Match_Time_Difference"],seq_entry['PEP'],seq_entry['Intensity'],seq_entry['Charge'],seq_entry['m_z']
					sequence = seq_entry["Sequence"]
					if sequence not in sequence_list:
						sequence_list.append(sequence)
				#print sequence_list
				#print '\n'
				top_all = []
				top_good = []
				top_bad = []
				#print sequence_list
				for unique_sequence in sequence_list:
					#print unique_sequence
					max_pep_cmd = 'SELECT MIN(PEP) FROM %s WHERE Protein_group_IDs = "%s" AND SEQUENCE = "%s"' %(table_name,proteingroup,unique_sequence)
					#raw_input(max_pep_cmd)
					cur.execute(max_pep_cmd)
					pep_list = cur.fetchone()
					#print(len(seq_list))
					max_pep =  pep_list[0]
					#print max_pep
					
					max_pep_cmd = 'SELECT * FROM %s WHERE Protein_group_IDs = "%s" AND SEQUENCE = "%s" AND PEP = "%s"' %(table_name,proteingroup,unique_sequence,max_pep)
					#raw_input(max_pep_cmd)
					cur.execute(max_pep_cmd)
					seq_list = cur.fetchone()
					#print seq_list
					cmd = 'INSERT INTO top_evidence SELECT * FROM %s WHERE Protein_group_IDs = "%s" AND SEQUENCE = "%s" AND PEP = "%s"' %(table_name,proteingroup,unique_sequence,max_pep)
					#print cmd
					#cmd = 'INSERT INTO top_evidence VALUES("%s")' %('","'.join(seq_list))
					#print cmd
					cur.execute(cmd)
					number_inserted += 1
					
					m_z = seq_list["m_z"]
					rt = seq_list["Retention_time"]
					charge = seq_list["Charge"]
					proteins = seq_list["Proteins"]
					sequence = seq_list["Sequence"]
					intensity = seq_list["Intensity"]
					exclusion_list = [m_z,'Positive',str(float(rt)-extend_rt),str(float(rt)+extend_rt),charge,proteins+':'+sequence+':'+intensity]
					exclusion_line = '\t'.join(exclusion_list)
					#print exclusion_line
					print number_inserted
	cmd = "SELECT * FROM top_evidence"
	print cmd
	cur.execute(cmd)
	table_list= cur.fetchall()
	print(len(table_list))

if program_option == 'TRIM':
	read_file = open(select_file,'r')
	read_list = read_file.readlines()
	read_file.close
	protein_list = []
	for read_line in read_list:
		print read_line
		protein_list.append(read_line.replace('\n',''))
	print protein_list
	
	#selection_file = open(select_file,'r')
	#selection_tup = selection_file.readlines()
	#selection_file.close()
	#protein_list = [i.replace('\n','') for i in selection_tup]
	#protein_list = []
	protein_hit_list = []

	with con:
		cur = con.cursor()
		table_name = 'exclusion_list'
		header_list = 'Mass','Formula','Species',"Charge",'Polarity',"Start","End",'NCE',"Description"
		create_table(header_list,'full_exclusion_list')
		create_table(header_list,'reduced_exclusion_list')
		create_table(header_list,'selected_exclusion_list')
		#raw_input()


		#cmd = "SELECT * FROM %s" %('evidence_txt'
		#print cmd
		#cur.execute(cmd)
		#col_names = [cn[0] for cn in cur.description]
		#create_table(col_names,'top_evidence')

		cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
		table_list = cur.fetchall()
		print '\nTable Names:'
		for table_name in table_list:
			print table_name[0]
		print'\n'
		#raw_input('enter to continue')


		table_list = [['top_evidence']]
		print table_list

		top_all = []
		top_good = []
		top_bad = []
		exclusion_number = 0
		#raw_input()
		number_inserted = 0
		proteingroups_list = []
		write_list = []

		for table_name in table_list:
			table_name = table_name[0]
			print table_name
			title_list = []
			cmd = "SELECT * FROM %s" %(table_name)
			print cmd
			cur.execute(cmd)
			table_list= cur.fetchall()
			print(len(table_list))
			for table_line in table_list:
				proteingroup = table_line['Protein_group_IDs']
				#print proteingroup
				if proteingroup not in proteingroups_list:
					proteingroups_list.append(proteingroup)
					seq_cmd = 'SELECT * FROM %s WHERE Protein_group_IDs = "%s"' %(table_name,proteingroup)
					cur.execute(seq_cmd)
					seq_list = cur.fetchall()
					for seq_entry in seq_list:
							m_z = seq_entry["m_z"]
							rt = seq_entry["Retention_time"]
							charge = seq_entry["Charge"]
							proteins = seq_entry["Proteins"]
							sequence = seq_entry["Sequence"]
							intensity = seq_entry["Intensity"]
							write_pep = seq_entry['PEP']
							exclusion_list = [m_z,'','',charge,'Positive',str(float(rt)-extend_rt),str(float(rt)+extend_rt),'25',proteins+':'+sequence+':'+intensity+':'+str(write_pep)]
							cmd = 'INSERT INTO %s VALUES("%s")' %('full_exclusion_list','","'.join(exclusion_list))
							#print(cmd)
							cur.execute(cmd)
						
					seq_cmd = 'SELECT * FROM %s WHERE Protein_group_IDs = "%s" ORDER BY PEP ASC LIMIT %s' %(table_name,proteingroup,max_threshold)
					cur.execute(seq_cmd)
					seq_list = cur.fetchall()
					#raw_input(len(seq_list))
					if len(seq_list) > min_threshold:
					#print(len(seq_list))
						sequence_list = []
						for seq_entry in seq_list:
							m_z = seq_entry["m_z"]
							rt = seq_entry["Retention_time"]
							charge = seq_entry["Charge"]
							proteins = seq_entry["Proteins"]
							sequence = seq_entry["Sequence"]
							intensity = seq_entry["Intensity"]
							write_pep = seq_entry['PEP']
							exclusion_list = [m_z,'','',charge,'Positive',str(float(rt)-extend_rt),str(float(rt)+extend_rt),'25',proteins+':'+sequence+':'+intensity+':'+str(write_pep)]
							#exclusion_list = [m_z,'Positive',str(float(rt)-extend_rt),str(float(rt)+extend_rt),charge,proteins+':'+sequence+':'+intensity+':'+str(write_pep)]
							cmd = 'INSERT INTO %s VALUES("%s")' %('reduced_exclusion_list','","'.join(exclusion_list))
							cur.execute(cmd)
							#print proteins
							for protein in protein_list:
								#print(protein)
								#raw_input()
								if protein in proteins:
									print 'HIT'
									print protein
									print proteins
									if protein == 'Rv':
										raw_input('Rv')
							#if proteins in protein_list:
								#raw_input('hit')
									cmd = 'INSERT INTO %s VALUES("%s")' %('selected_exclusion_list','","'.join(exclusion_list))
									protein_hit_list.append(proteins)
									print len(protein_hit_list)
							#raw_input(cmd)
									cur.execute(cmd)
							
		full_length = write_sorted_table('full_exclusion_list','End','ASC')
		reduced_length = write_sorted_table('reduced_exclusion_list','Start','ASC')
		selected_length = write_sorted_table('selected_exclusion_list','Start','ASC')
		#if full_length > 5000:
		#	split_write_table('full_exclusion_list','stop','ASC')
		#if reduced_length > 5000:
		#	split_write_table('reduced_exclusion_list','stop','ASC')
	gene_not_hit_list = []
	for protein in protein_list:
		if protein not in protein_hit_list:
			gene_not_hit_list.append(protein)
			
	print len(protein_list)
	print "number of genes found in RAW data = %s, number of PSM's = %s" %(len(set(protein_hit_list)),selected_length)
	print 'number of gens not found in RAW data = %s' %(len(gene_not_hit_list))

	write_file_name = 'genes_found.txt'
	write_list = [i+'\n' for i in set(protein_hit_list)]
	write_file = open(write_file_name,'w')
	write_file.writelines(write_list)
	write_file.close


	write_file_name = 'genes_not_found.txt'
	write_list = [i+'\n' for i in gene_not_hit_list]
	write_file = open(write_file_name,'w')
	write_file.writelines(write_list)
	write_file.close
							
							
							
if program_option == 'WRITE':
	with con:
		cur = con.cursor()
		table_name_list = ['full_exclusion_list','reduced_exclusion_list','selected_exclusion_list']
		for table_name in table_name_list:
			cmd = 'SELECT * from %s' %(table_name)
			table_length = cur.execute(cmd)
			raw_input(table_length)
			if table_length > 5000:
				split_write_table(table_name,'start','ASC')
		#full_length = write_sorted_table('full_exclusion_list','stop','ASC')
		#reduced_length = write_sorted_table('reduced_exclusion_list','start','ASC')
		#if full_length > 5000:
			#split_write_table('full_exclusion_list','stop','ASC')
		#if reduced_length > 5000:
			#split_write_table('reduced_exclusion_list','stop','ASC')
		#if selected_lenght > 5000:
			#split_write_table('selected_exclusion_list','stop','ASC')

con.commit()
if con:
        con.close()
print 'complete'