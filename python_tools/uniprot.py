import urllib
import urllib2

def uniprot_mapping(fromtype, totype, identifier):
    base = 'http://www.uniprot.org'
    tool = 'mapping'
    params = {'from':fromtype,
                'to':totype,
                'format':'tab',
                'query':identifier,
    }
    data = urllib.urlencode(params)
    url = base+'/'+tool+'?'+data
    response = urllib2.urlopen(url)
    return response.read()
