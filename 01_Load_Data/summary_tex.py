import os
import sys

python_path = sys.argv[1]
dirname = sys.argv[2]

sys.path.insert(1, python_path)
import MQ_functions

sys.path.insert(1, dirname)
import variable

#raw_input('summary')
file_name = 'summary'

header = ["\documentclass{article}\n","\usepackage[section] {placeins}\n","\\begin{document}\n","Load MaxQuant data into R\n","<<summary_FILE>>=\n","source('%s/rscripts/00_01.config.R')\n" %(dirname),'load(workspace_filename)\n',"setwd('%s/rscripts/')\n" %(dirname),'@\n']
summary_table = ["""
<<summary_table>>=
<<xtable1, results=tex>>=
require(xtable)
xtab<-xtable(df.summary_stats)
print(xtab, floating=FALSE,include.rownames=FALSE)
xtab<-xtable(df.calc)
print(xtab)
xtab <- xtable(df.summary.txt)
print(xtab)
xtab <- xtable(df.parameters.txt)
print(xtab)

xtab <- xtable(df.summary_stats['df.evidence.txt_Length',])
print(xtab)
@
"""]
evidence = ['\clearpage\n','\section{evidence}\n']
hist_column_list = ['Length','Charge','m.z','Resolution','Retention.time','B.mixture','Retention.Length','Retention.Time.Calibration','PIF','Fraction.of.total.spectrum','Base.peak.fraction','PEP','MS.MS.Count','Intensity']
for hist_column in hist_column_list:
	evidence.append("""\\begin{figure}[h]
<<evidence_%s, fig=TRUE>>=
par(mfrow=c(1,2))
data = df.evidence.txt$%s
try(hist(data, breaks=25, main=paste('Median =',signif(median(data),digits=3))))
try(boxplot(data))
title('evidence - %s', outer = true)
par(mfrow=c(1,1))
@

\\end{figure}\n

""" %(hist_column.replace('.','_'),hist_column,hist_column))
hist_column = 'Retention.Length'
evidence.append("""\\begin{figure}[h]
<<evidence_%s, fig=TRUE>>=
par(mfrow=c(1,2))
data = df.evidence.txt$%s
try(hist(data, breaks=200, xlim = c(0,2), main=NULL))
try(boxplot(data))
title('evidence - %s', outer = true)
par(mfrow=c(1,1))
@

\\end{figure}\n



""" %(hist_column.replace('.','_')+'_ZOOM',hist_column,hist_column))
print(evidence)
#evidence.append("""\\begin{figure}
#<<evidence_Retention_Length_2, fig=TRUE>>=
#hist(df.evidence.txt$Retention.Length, breaks=25)
#@
#\\end{figure}\n""")
#raw_input(evidence)
peptides= ['\section{peptides}\n']

hist_column_list = ['Missed.cleavages']
for hist_column in hist_column_list:
	peptides.append("""Peptides
\\begin{figure}[h]
<<peptides_%s, fig=TRUE>>=
try(hist(df.peptides.txt$%s, breaks=25))
@
\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))
	
allPeptides= ['\section{allPeptides}\n']

hist_column_list = ['Npoints','Retention.Length','Retention.Length..FWHM.']
for hist_column in hist_column_list:
	allPeptides.append("""Peptides
\\begin{figure}[h]
<<allPeptides_%s, fig=TRUE>>=
try(hist(df.allPeptides.txt$%s, breaks=25))
@
\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))

proteinGroups= ['\section{proteinGroups}\n']
hist_column_list = ['Proteins','Peptides','Unique.peptides','Sequence.coverage....','Unique.sequence.coverage....','Mol..weight..kDa','PE']
for hist_column in hist_column_list:
	proteinGroups.append("""\\begin{figure}
<<evidence_%s, fig=TRUE>>=
try(hist(df.proteinGroups.txt$%s, breaks=25))
@
\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))
footer = ["\\end{document}\n"]

msScans= ['\section{msScans}\n']

hist_column_list = ['Cycle.time','Dead.time','Ion.injection.time']
for hist_column in hist_column_list:
	msScans.append("""Peptides
\\begin{figure}[h]
<<msScans_%s, fig=TRUE>>=
try(hist(df.msScans.txt$%s, breaks=25))
@
\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))

read_file = open('%s/summary.txt' %(dirname),'r')
read_lines = read_file.readlines()
read_file.close()
read_length = len(read_lines)
#raw_input(read_length)
#write_lines = read_lines
summary = ['\section{Summary}\n']
for read_line_number in range(1,read_length):
	summary_line = """
\\begin{figure}
<<summary_barplot_%s, fig=TRUE,echo=TRUE>>=
data = df.summary.txt[%s,c('MS','MS.MS','MS.MS.Submitted','MS.MS.Identified','Peptide.Sequences.Identified')]
try(barplot(as.matrix(data),main=df.summary.txt[%s,1],xaxt='n'))
axis(1,at = c(1:dim(data)[2]),labels = colnames(data),las=3,cex.axis=0.5)
@
\\end{figure}\n""" %(read_line_number,read_line_number,read_line_number)
	summary.append(summary_line)


#write_lines = header+summary_table+evidence+peptides+allPeptides+proteinGroups+summary+footer
#write_file = open('%s/rscripts/%s.Rnw' %(dirname,file_name),'w')
#write_file.writelines(write_lines)
#write_file.close()






#header = ["\documentclass{article}\n","\usepackage[section] {placeins}\n","\\begin{document}\n","Load MaxQuant data into R\n","<<summary_FILE>>=\n",variable.variable_dic['load'],"setwd('%s/rscripts/')\n" %(dirname),'@\n']
#summary_table = ["""
#<<summary_table>>=
#<<xtable1, results=tex>>=
#require(xtable)
#xtab<-xtable(df.summary_stats)
#print(xtab, floating=FALSE,include.rownames=FALSE)
#xtab<-xtable(df.calc)
#print(xtab)
#xtab <- xtable(df.summary.txt)
#print(xtab)
#xtab <- xtable(df.parameters.txt)
#print(xtab)

#xtab <- xtable(df.summary_stats['df.evidence.txt_Length',])
#print(xtab)
#@
#"""]
#evidence = ['\clearpage\n','\section{evidence}\n']
#hist_column_list = ['Length','Charge','m.z','Resolution','Retention.time','B.mixture','Retention.Length','Retention.Time.Calibration','PIF','Fraction.of.total.spectrum','Base.peak.fraction','PEP','MS.MS.Count','Intensity']
#for hist_column in hist_column_list:
	#evidence.append("""\\begin{figure}[h]
#<<evidence_%s, fig=TRUE>>=
#par(mfrow=c(1,2))
#data = df.evidence.txt$%s
#try(hist(data, breaks=25, main=paste('Median =',signif(median(data),digits=3))))
#try(boxplot(data))
#title('evidence - %s', outer = true)
#par(mfrow=c(1,1))
#@

#\\end{figure}\n

#""" %(hist_column.replace('.','_'),hist_column,hist_column))
#hist_column = 'Retention.Length'
#evidence.append("""\\begin{figure}[h]
#<<evidence_%s, fig=TRUE>>=
#par(mfrow=c(1,2))
#data = df.evidence.txt$%s
#try(hist(data, breaks=200, xlim = c(0,2), main=NULL))
#try(boxplot(data))
#title('evidence - %s', outer = true)
#par(mfrow=c(1,1))
#@

#\\end{figure}\n



#""" %(hist_column.replace('.','_')+'_ZOOM',hist_column,hist_column))
#print(evidence)
##evidence.append("""\\begin{figure}
##<<evidence_Retention_Length_2, fig=TRUE>>=
##hist(df.evidence.txt$Retention.Length, breaks=25)
##@
##\\end{figure}\n""")
##raw_input(evidence)
#peptides= ['\section{peptides}\n']

#hist_column_list = ['Missed.cleavages']
#for hist_column in hist_column_list:
	#peptides.append("""Peptides
#\\begin{figure}[h]
#<<peptides_%s, fig=TRUE>>=
#try(hist(df.peptides.txt$%s, breaks=25))
#@
#\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))
	
#allPeptides= ['\section{allPeptides}\n']

#hist_column_list = ['Npoints','Retention.Length','Retention.Length..FWHM.']
#for hist_column in hist_column_list:
	#allPeptides.append("""Peptides
#\\begin{figure}[h]
#<<allPeptides_%s, fig=TRUE>>=
#try(hist(df.allPeptides.txt$%s, breaks=25))
#@
#\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))

#proteinGroups= ['\section{proteinGroups}\n']
#hist_column_list = ['Proteins','Peptides','Unique.peptides','Sequence.coverage....','Unique.sequence.coverage....','Mol..weight..kDa','PE']
#for hist_column in hist_column_list:
	#proteinGroups.append("""\\begin{figure}
#<<evidence_%s, fig=TRUE>>=
#try(hist(df.proteinGroups.txt$%s, breaks=25))
#@
#\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))
#footer = ["\\end{document}\n"]

#msScans= ['\section{msScans}\n']

#hist_column_list = ['Cycle.time','Dead.time','Ion.injection.time']
#for hist_column in hist_column_list:
	#msScans.append("""Peptides
#\\begin{figure}[h]
#<<msScans_%s, fig=TRUE>>=
#try(hist(df.msScans.txt$%s, breaks=25))
#@
#\\end{figure}\n"""  %(hist_column.replace('.','_'),hist_column))

#read_file = open('%s/summary.txt' %(dirname),'r')
#read_lines = read_file.readlines()
#read_file.close()
#read_length = len(read_lines)
##raw_input(read_length)
##write_lines = read_lines
#summary = ['\section{Summary}\n']
#for read_line_number in range(1,read_length):
	#summary_line = """
#\\begin{figure}
#<<summary_barplot_%s, fig=TRUE,echo=TRUE>>=
#data = df.summary.txt[%s,c('MS','MS.MS','MS.MS.Submitted','MS.MS.Identified','Peptide.Sequences.Identified')]
#try(barplot(as.matrix(data),main=df.summary.txt[%s,1],xaxt='n'))
#axis(1,at = c(1:dim(data)[2]),labels = colnames(data),las=3,cex.axis=0.5)
#@
#\\end{figure}\n""" %(read_line_number,read_line_number,read_line_number)
	#summary.append(summary_line)


write_lines = header+summary_table+evidence+peptides+allPeptides+proteinGroups+summary+footer
write_file = open('%s/rscripts/%s.Rnw' %(dirname,file_name),'w')
write_file.writelines(write_lines)
write_file.close()

os.system('cd %s/rscripts' %(dirname))

cmd = "rstudio %s/rscripts/%s.Rnw" %(dirname,file_name)
#pdflatex my_sweave_file.tex
print(cmd)
#os.system(cmd)

cmd = "R CMD Sweave %s/rscripts/%s.Rnw" %(dirname,file_name)
#pdflatex my_sweave_file.tex
print(cmd)
os.system(cmd)

cmd = 'pdflatex %s/rscripts/%s.tex' %(dirname,file_name)
print(cmd)
os.system(cmd)

cmd = 'okular %s/rscripts/summary.pdf' %(dirname)
print(cmd)
os.system(cmd)