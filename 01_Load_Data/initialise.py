import os
import sys

python_path = sys.argv[1]
dirname = sys.argv[2]

sys.path.insert(1, python_path)
import MQ_functions

print(dirname+'/rscripts')
sys.path.insert(1, dirname+'/rscripts')
#file_list = os.listdir(dirname+'/rscripts')
#raw_input(file_list)
import variable
reload(variable)


#python_path = variable.variable_dic['python_path']
#selected_file_list = ['proteinGroups.txt','peptides.txt','evidence.txt','parameters.txt','summary.txt']
selected_file_list = variable.variable_dic['file_list']
#file_list = variable.variable_dic['MQ_file_list']	
file_line = "','".join(selected_file_list)
print(file_line)
maxquant_files = "file_list = c('%s')\n" %(file_line)
working_directory_line = "working_directory = '%s/'\n" %(dirname)


project_name = variable.variable_dic['project_name']
project_name_line = variable.variable_dic['project_name_line_str']
load_line = variable.variable_dic['load']
load_list = [load_line]

source_list = variable.variable_dic['source_list']
workspace_filename = variable.variable_dic['workspace_filename']


file_info = """
print(working_directory)
#rscript_directory = '/home/sgarnett/Documents/programming/MaxQuant/R_iBAQ'
setwd(working_directory)
experiment_list = c('None')

print('############ CONFIG FILE #############')
print(paste('Project Name :',project_name))
print(paste('Working Directory : ',working_directory))


### Create working directory folder structure
      setwd(working_directory)
      working_directory_list = list.files(working_directory)
      #print(working_directory_list)
      ### Creates a Path List in the Working Directory ###
      directory_list = c('rscripts','workspace','output','images')
      for(directory_name in directory_list){
        #print(directory_name)
        hit = 0
        if(directory_name %in% working_directory_list){
          hit = 1
        }
        if(hit == 0){
          new_path = paste(working_directory,directory_name,sep='')
          #print(new_path)
          create_dir_function(new_path)
        }
      }



workspace_filename = %s
print(paste('Workspace Filename :',workspace_filename))\n""" %(workspace_filename)





cmd = working_directory_line+project_name_line,maxquant_files,file_info
write_file = open('%s/rscripts/00_02_data_location_design.R' %(dirname),'w')
write_file.writelines(cmd)
write_file.close()

#cmd = 'Rscript 01_Load_Data/01_Load.R'
#print(cmd)
#os.system(cmd)

source_file_line = """
%s
%s
print(working_directory)
print(workspace_filename)
save.image(workspace_filename)
""" %(load_line,''.join(source_list))

os.chdir('%s/rscripts/' %(dirname))

source_file = open('%s/rscripts/Reload_Source.R' %(dirname),'w')
source_file.write(source_file_line)
source_file.close()

#/home/sgarnett/Documents/programming/MaxQuant_Gui/00_config/00_03_Select_Data_All.R


config_file_list = ['00_01.config.R']
for config_file in config_file_list:
	cmd = 'cp %s/00_config/%s %s/rscripts/' %(python_path,config_file,dirname)
	print(cmd)
	os.system(cmd)
	
#functions_path = '%s/R_functions/' %(python_path)
#functions_list = os.listdir(functions_path)
#for functions_file in functions_list:
#	cmd = 'cp %s/%s %s/rscripts/' %(functions_path,functions_file,dirname)
#	print(cmd)
#os.system(cmd)

#R_file_list = ['01_Load','02_CLEAN_UP_DATA_step']


R_file_list = ['Load_functions.R']
for R_file_name in R_file_list:
	#raw_input(R_file_name)
	read_file = open('%s/00_config/%s' %(python_path,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	config_list = ["function_location = '%s/rscripts/00_functions/'\n" %(dirname)]
	write_list = config_list+read_list[2:]
	#raw_input(write_list)
	write_file_name = '%s/rscripts/%s' %(dirname,R_file_name)
	#raw_input(write_file_name)
	write_file = open(write_file_name,'w')
	write_file.writelines(write_list)
	write_file.close()


Rnw_file_list = ['00_01.config.R']
for R_file_name in Rnw_file_list:
	#raw_input(R_file_name)
	read_file = open('%s/00_config/%s' %(python_path,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	config_list = ["source('%s/rscripts/Load_functions.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname)]
	write_list = config_list+read_list[2:]
	#raw_input(write_list)
	write_file = open('%s/rscripts/%s' %(dirname,R_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()


R_file_list = ['00_00_create_workspace.R']
for R_file_name in R_file_list:
	#raw_input(R_file_name)
	read_file = open('%s/01_Load_Data/%s' %(python_path,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	#load_list = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname)]
	write_list = source_list+read_list[2:]
	write_file = open('%s/rscripts/%s' %(dirname,R_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()

R_file_list = ['summary_stat.R']
for R_file_name in R_file_list:
	#raw_input(R_file_name)
	read_file = open('%s/01_Load_Data/%s' %(python_path,R_file_name),'r')
	read_list = read_file.readlines()
	read_file.close()
	#load_list = ["source('%s/rscripts/00_01.config.R')\n" %(dirname),"source('%s/rscripts/00_02_data_location_design.R')\n" %(dirname),'load(workspace_filename)\n']
	write_list = load_list+read_list[2:]
	write_file = open('%s/rscripts/%s' %(dirname,R_file_name),'w')
	write_file.writelines(write_list)
	write_file.close()
	


