import os
import sys


file_name = sys.argv[1]


print file_name
read_file = open(file_name,'r')
read_list = read_file.readlines()
read_file.close
list_dic = {}
for read_entry in read_list:
	print(read_entry)
	read_entry = read_entry.replace('\r','').replace('\n','')
	entry_list = read_entry.split('\t')
	#print entry_list
	search_list = ['affects','increases','decreases']
	first_entry = entry_list[0].split(' ')[0]
	#print first_entry
	for search_entry in search_list:
		if first_entry == search_entry:
			#print search_entry
			list_dic[search_entry] = entry_list[1].split(', ')


dic_keys = list_dic.keys()


for key in dic_keys:
	print '\n\n'
	print key
	write_list = ['%s\n' %(str(i)) for i in list_dic[key]]
	print write_list
	write_file_name = '%s.%s' %(file_name,key)
	print write_file_name
	write_file = open(write_file_name,'w')
	write_file.writelines(write_list)
	write_file.close()
	