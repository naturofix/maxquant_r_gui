def Load():
	maxquant_file_list  = ['proteinGroups.txt','peptides.txt','summary.txt','parameters.txt']
	write_variable('file_list',maxquant_file_list)
	
	#experiment_name()
	
	
	R_dir = '01_Load_Data'
	R_file = '01_Load.R'
	add_list = [variable.variable_dic['file_list_line'],"file_path = '%s'\n" %(dirname)]
	tools(R_dir,R_file,add_list)
	
		
	R_dir = '01_Load_Data'
	R_file = '02_CLEAN_UP_DATA.R'
	tools(R_dir,R_file)
	status('Cleaned up data')
	Extract_Sample_Info()
	status('extract sample info')