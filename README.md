# README #

### What is this repository for? ###

* The MaxQuant Gui extract information from the text files of MaxQuant (proteinGroups, peptides, evidence, summary,  parameters)
This data can either be imported into SQLite to extract features for tragetted proteomics, or into R for statistical analysis

This is the first version

### How do I get set up? ###

This software was developed on Linux, it requires the following programs to be installed
python, R, rstudio, pandoc, markdown, latex, libcurl4-gnutls-dev, libxml2-dev

python packages : tkinter
R libraries : knitr, ggplot2, reshape, e1071, devtools
R Bioconductor packages : preprocessCore, limma, Mfuzz, Biobabase, org.Hs.eg.db, reactome.db, XML, genefilter, Hmisc

library(devtools)
install_github('ggbiplot','vqv')

The program is run through python from the maxquant_r_gui directory

python Run_Load.py

This will open a file browser, point the file browser into the MaxQaunt txt folder

click R Program : opens the R interface
Load Data and Clean Up : Loads data into R, removes contaminants and Reverse hits
                         Creates folders in the txt directory
                         create a R workspace
                         copies code as you use it


select the proteinGroups file clicking on the proteinGroups checkbox

Extract LFQ: Extracts Label Free Qaunt into additional tables

select iBAQ to use iBAQ

click Normalise

Now you can use the other buttons to view the data in different ways.
Select the type of normalisation you would like to use
na.z - NA and Zero's removed
qn - quantile normalised
z2na - zeros are converted to na
   combination of these 

click boxplot to get a boxplot of the data selected

knitr Report, will generate a summary pdf of the data. 

Observe when you change the checkbox's the table name field at the top of the window changes accordingly.

Open_Rstudio will open Rstudio and give you access to the data, custom code can be written here, save as a new file, completed files can be incorporated into the Gui at a later date.


### Contribution guidelines ###

Write new code from new.R opened by Open_Rstudio, save changes as a new file.
Upload the file to bitbucket
Request addition of a button to incorporate you code into the Gui

The header within the new.R file is created automatically by the GUI, this is essential to identify the data that is going to be used. This header can be customised if necessary, but this needs to be done from python.
Start your own code from line 20

If it is necessary to save changes to the workspace the last line of the document should be

save.image(workspace_filename)